[ ![Download](https://api.bintray.com/packages/hotelde/maven/hde-library/images/download.svg) ](https://bintray.com/hotelde/maven/hde-library/_latestVersion)

HOTEL DE Android Library
========================

This library provides access to HOTEL DE webservices for searching and booking hotels.

# Publish hde-library to maven
Invoke the gradle task *uploadArchives*

	$ ./gradlew uploadArchives

In Android Studio, the task can be found under *android-library:hde-library:Tasks:upload:uploadArchives*

# Usage
Checkout *HotelDe.java* to get started.

# Data Access Layer
This library is based on a JiBX binding for handling XML. You can find our JiBX project in [this project](https://bitbucket.org/hotelde/android-library-jibx).