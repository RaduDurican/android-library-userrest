package de.hotel.android.library.domain.model

import android.os.Parcel
import android.os.Parcelable

import java.util.ArrayList

/**
 * Information about one hotel and its offers (i.e. for every requested room).
 */
class HotelOffer() : Parcelable {

    var ratePlans: List<HdeRatePlan> = ArrayList()
    var cheapestRatePlan: HdeRatePlan? = null
    var cheapestCancelableRatePlan: HdeRatePlan? = null

    constructor(parcel: Parcel) : this() {
        parcel.readTypedList(ratePlans, HdeRatePlan.CREATOR)
        cheapestRatePlan = parcel.readParcelable<HdeRatePlan>(HdeRatePlan::class.java.classLoader)
        cheapestCancelableRatePlan = parcel.readParcelable<HdeRatePlan>(HdeRatePlan::class.java.classLoader)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeTypedList(ratePlans)
        parcel.writeParcelable(cheapestRatePlan, i)
        parcel.writeParcelable(cheapestCancelableRatePlan, i)
    }

    fun getRatePlan(index: Int): HdeRatePlan {
        return ratePlans[index]
    }

    fun getRatePlan(bookingCode: String): HdeRatePlan {
        for (i in ratePlans.indices) {
            if (ratePlans[i].bookingCode == bookingCode) {
                return ratePlans[i]
            }
        }

        throw RuntimeException("Rateplan with bookingCode not found: " + bookingCode)
    }

    companion object {

        @JvmField val CREATOR: Parcelable.Creator<HotelOffer> = object : Parcelable.Creator<HotelOffer> {
            override fun createFromParcel(parcel: Parcel): HotelOffer {
                return HotelOffer(parcel)
            }

            override fun newArray(i: Int): Array<HotelOffer?> {
                return arrayOfNulls(i)
            }
        }
    }
}
