package de.hotel.android.library.util;

public class Constants {
    public static final String SCHEMA_HTNG_CHECK_AVAILABILITY = "http://htng.org/2010A/HTNG_SeamlessShopAndBookService#CheckAvailability";
    public static final String SCHEMA_HTNG_RESERVATION_REQUEST = "http://htng.org/2010A/HTNG_SeamlessShopAndBookService#ProcessReservationRequest";
    public static final String SCHEMA_GET_PROPERTY_DESCRIPTION_REQUEST = "http://webservices.hotel.de/V2_8/IHotelSearchWebService/GetPropertyDescription";
    public static final String SCHEMA_GET_PROPERTY_REVIEWS_REQUEST = "http://webservices.hotel.de/V2_8/IHotelSearchWebService/GetPropertyReviews";
    public static final String SCHEMA_GET_GET_LOCATIONS_REQUEST = "http://webservices.hotel.de/V2_8/IHotelSearchWebService/GetLocations";
    public static final String SCHEMA_DETERMINE_LOCATION_NUMBER_REQUEST = "http://webservices.hotel.de/V2_8/IHotelSearchWebService/DetermineLocationNumber";
    public static final String SCHEMA_GET_CUSTOMER_DATA_REQUEST = "http://webservices.hotel.de/V2_8/ICustomerManagementWebService/GetCustomerData";
    public static final String SCHEMA_CHECK_RESERVATION_STATUS_REQUEST = "http://webservices.hotel.de/V2_8/IHotelBookWebService/CheckReservationStatus";
    public static final String SCHEMA_CANCEL_RESERVATION_REQUEST = "http://webservices.hotel.de/V2_8/IHotelBookWebService/CancelReservation";

    public static final String HSBW_BETA_ENDPOINT = "https://betaappservices.hotel.de";
    public static final String HSBW_PROD_ENDPOINT = "https://appservices.hotel.de";

    public static final String HSBW_2_8_HOTEL_SEARCH_SERVICE = "/V2_8/HotelSearchWebService.svc";
    public static final String HSBW_2_8_CUSTOMER_MANAGEMENT_SERVICE = "/V2_8/CustomerManagementWebService.svc";
    public static final String HSBW_2_8_BOOK_SERVICE = "/V2_8/HotelBookWebService.svc";

    public static final String HSBW_3_0_SEAMLESS_SHOP_AND_BOOK_SERVICE = "/V3_0/OTA2012A/HTNG_SeamlessShopAndBookService.svc";

    public static final String LAC_PROD_ENDPOINT = "https://www.hotel.de/lac";

    public static final String COLA_BETA_ENDPOINT = "https://staging.hotel.de";
    public static final String COLA_PROD_ENDPOINT = "https://www.hotel.de";

    public static final String COLA_SIMPLE_REGISTRATION = "/Customer/SimpleRegistration/SimpleRegistration";
}
