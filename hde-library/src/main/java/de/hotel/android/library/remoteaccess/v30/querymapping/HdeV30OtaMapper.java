package de.hotel.android.library.remoteaccess.v30.querymapping;

import de.hotel.android.library.domain.model.data.Language;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DateTimeSpanType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAPayloadStdAttributes;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.POSType;

public interface HdeV30OtaMapper {
    String HSBW_CONTEXT = "hotel.de"; // TODO Please discuss whether this is a suitable location for this constant

    DateTimeSpanType createDateTimeSpanType(Long from, Long to);
    POSType createPosType();
    OTAPayloadStdAttributes createOtaPayloadStdAttributes(Language language);
}
