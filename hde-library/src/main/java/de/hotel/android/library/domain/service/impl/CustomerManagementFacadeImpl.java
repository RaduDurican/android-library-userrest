package de.hotel.android.library.domain.service.impl;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.List;

import de.hotel.android.library.domain.model.Reservation;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.enums.CancelResultCode;
import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.query.CancelReservationQuery;
import de.hotel.android.library.domain.model.query.CheckReservationStatusQuery;
import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;
import de.hotel.android.library.domain.model.response.RegistrationResponse;
import de.hotel.android.library.domain.service.CustomerManagementFacade;
import de.hotel.android.library.remoteaccess.adapter.HdeCustomerManagementAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeCustomerRegistrationAdapter;
import de.hotel.android.library.remoteaccess.adapter.HdeReservationManagementAdapterImpl;
import de.hotel.android.library.remoteaccess.v28.querymapping.CustomerDataQuery;

public class CustomerManagementFacadeImpl implements CustomerManagementFacade {
    private final HdeCustomerManagementAdapterImpl customerManagementAdapter;
    private final HdeReservationManagementAdapterImpl reservationManagementAdapter;
    private final HdeCustomerRegistrationAdapter customerRegistrationAdapter;

    public CustomerManagementFacadeImpl(@NonNull HdeCustomerManagementAdapterImpl customerManagementAdapter,
                                        @NonNull HdeReservationManagementAdapterImpl reservationManagementAdapter,
                                        @NonNull HdeCustomerRegistrationAdapter customerRegistrationAdapter) {
        this.customerManagementAdapter = customerManagementAdapter;
        this.reservationManagementAdapter = reservationManagementAdapter;
        this.customerRegistrationAdapter = customerRegistrationAdapter;
    }

    public Customer getCustomerData(@NonNull CustomerDataQuery query) throws IOException {
        return customerManagementAdapter.getCustomerData(query);
    }

    public List<Reservation> getReservationList(@NonNull CheckReservationStatusQuery query) {
        return reservationManagementAdapter.getReservationList(query);
    }

    public @CancelResultCode int cancelReservation(@NonNull CancelReservationQuery query) {
        return reservationManagementAdapter.cancelReservation(query);
    }

    public @RegistrationResultCode int registerCustomer(@NonNull CustomerRegistrationQuery query) {
        return customerRegistrationAdapter.registerCustomer(query);
    }
}
