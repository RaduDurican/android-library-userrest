package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import de.hotel.android.library.domain.model.data.GeoPosition;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.util.ObjectDumper;
import de.hotel.remoteaccess.v28.model.DetermineLocationNumberResponse1;
import de.hotel.remoteaccess.v28.model.ErrorCodeType;
import de.hotel.remoteaccess.v28.model.GetLocationsResponse1;


public class HdeV28LocationsResultMapper {
    public List<Location> mapLocations(@NonNull GetLocationsResponse1 response, @Nullable Language requestedLanguage) {
        if (response == null) {
            throw new RuntimeException("GetLocationsResponse1 is null");
        }

        if (response.getError().getErrorCode() != ErrorCodeType.NO_ERROR) {
            throw new RuntimeException(ObjectDumper.dump(response.getError().getErrorCode()));
        }

        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < response.getLocations().getLocationList().size(); i++) {
            locations.add(mapLocation(response.getLocations().getLocationList().get(i), requestedLanguage));
        }

        return locations;
    }

    public List<Location> mapLocations(@NonNull DetermineLocationNumberResponse1 response, @Nullable Language requestedLanguage) {
        if (response == null) {
            throw new RuntimeException("DetermineLocationNumberResponse1 is null");
        }

        if (response.getError().getErrorCode() != ErrorCodeType.NO_ERROR) {
            throw new RuntimeException(ObjectDumper.dump(response.getError().getErrorCode()));
        }

        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < response.getLocationList().getLocationList().size(); i++) {
            locations.add(mapLocation(response.getLocationList().getLocationList().get(i), requestedLanguage));
        }

        return locations;
    }

    private Location mapLocation(de.hotel.remoteaccess.v28.model.Location v28Location, Language requestedLanguage) {
        Location location = new Location();

        location.setLocationId(v28Location.getLocationNr());
        location.setLocationName(v28Location.getLocationName());
        location.setLocationType(mapLocationType(v28Location.getType()));
        location.setIsoA3Country(v28Location.getCountryISOa3());
        location.setRegionName(v28Location.getMainRegion());
        location.setGeoPosition(mapGeoPosition(v28Location));

        if (requestedLanguage != null) {
            try {
                location.setLocationLanguage((Language) requestedLanguage.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        return location;
    }

    private @LocationType int mapLocationType(de.hotel.remoteaccess.v28.model.LocationType v28LocationType) {
        switch (v28LocationType) {
            case CITY:
                return LocationType.CITY;
            case REGION:
                return LocationType.REGION;
            case AIRPORT:
                return LocationType.AIRPORT;
            case RAILWAY_STATION:
                return LocationType.TRAIN_STATION;
            case FAIR:
                return LocationType.FAIR;
            case SIGHT:
                return LocationType.POI;
            case DISTRICT:
                return LocationType.DISTRICT;
            case ISLAND:
            case PROVINCE:
            case DOWN_TOWN:
            case NOT_SET:
            case OTHER:
            default:
                return LocationType.UNKNOWN;
        }
    }

    private GeoPosition mapGeoPosition(de.hotel.remoteaccess.v28.model.Location v28Location) {
        GeoPosition geoPosition = new GeoPosition();

        geoPosition.setLatitude(v28Location.getLatitude());
        geoPosition.setLongitude(v28Location.getLongitude());

        return geoPosition;
    }
}
