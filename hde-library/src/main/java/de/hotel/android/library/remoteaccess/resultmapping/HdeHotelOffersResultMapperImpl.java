package de.hotel.android.library.remoteaccess.resultmapping;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.AdditionalDetailType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.AdditionalDetailsType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.BasicPropertyInfoType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.CancelPenaltiesType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.CancelPenaltyType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.CurrencyAmountGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DeadlineGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.EffectiveExpireOptionalDateGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.FormattedTextSubSectionType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.FormattedTextTextType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.GuaranteeType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.MealsIncludedGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS.RoomStays.RoomStay;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ParagraphType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentCardType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.PaymentFormType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RateIndicatorType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RatePlanGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RatePlanType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RateType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStayType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomTypeType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.TotalType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.TransportationType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.VendorMessageType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hotel.android.library.domain.model.BookingGuarantee;
import de.hotel.android.library.domain.model.HdeAdditionalDetails;
import de.hotel.android.library.domain.model.HdeCancelPenalty;
import de.hotel.android.library.domain.model.HdePaymentInfo;
import de.hotel.android.library.domain.model.HdeRate;
import de.hotel.android.library.domain.model.HdeRatePlan;
import de.hotel.android.library.domain.model.HdeRoomRate;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.HotelOffer;
import de.hotel.android.library.domain.model.RoomDescription;
import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.Distance;
import de.hotel.android.library.domain.model.data.GeoPosition;
import de.hotel.android.library.domain.model.enums.HdeGuaranteeType;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapper;
import de.hotel.android.library.util.Booleans;
import de.hotel.android.library.util.Dates;
import timber.log.Timber;

/**
 * This Mapper delivers {@link HotelAvailResult}s from
 * Hotel.de (HDE) webservice objects.
 */
public class HdeHotelOffersResultMapperImpl implements HdeHotelOffersResultMapper {

    private static final java.lang.String KM = "km";
    private static final String TAG = HdeHotelOffersResultMapperImpl.class.getSimpleName();

    private static final String HSBW_ADDITIONAL_DESCRIPTION_BREAKFAST = "Breakfast";
    private static final String TRANSPORTATION_HIGHWAY = "5";
    private static final String TRANSPORTATION_AIRPORT = "14";
    private static final String TRANSPORTATION_TRAIN_STATION = "21";
    private static final String INFO_TYPE_PICTURES = "23";
    private static final String AWARD_PROVIDER_HOTEL_DE = "hotel.de";
    private static final String AWARD_PROVIDER_HOTEL_DE_UGCR = "hotel.de UGCR";

    private static final String DETAILS_BREAKFAST = "43";
    private static final String DETAILS_AMENITIES = "17";
    private static final String DETAILS_RECEPTION = "18";

    private final HdeCurrencyConversionResultMapper hdeCurrencyConversionResultMapper;
    private final HdeV30CreditCardMapper creditCardMapper;

    public HdeHotelOffersResultMapperImpl(@NonNull HdeCurrencyConversionResultMapper hdeCurrencyConversionResultMapper, @NonNull HdeV30CreditCardMapper hdeV30CreditCardMapper) {
        this.hdeCurrencyConversionResultMapper = hdeCurrencyConversionResultMapper;
        this.creditCardMapper = hdeV30CreditCardMapper;
    }

    @Override
    public List<Hotel> createHotels(
            @NonNull List<? extends RoomStayType> roomStays,
            @Nullable OTAHotelAvailRS.CurrencyConversions currencyConversions,
            @NonNull String iso3CurrencyCustomer) {
        List<Hotel> hotelList = new ArrayList<>();

        for (RoomStayType roomStay : roomStays) {

            // Filter out hotels that are not available (can happen for corporates)
            if (roomStay instanceof RoomStay) {
                RoomStay innerRoomStay = (RoomStay) roomStay;
                if (innerRoomStay.getAvailabilityStatus() != RateIndicatorType.AVAILABLE_FOR_SALE) {
                    continue;
                }
            }

            // Map: RoomTypeCode -> RoomType
            final Map<String, RoomTypeType> roomTypeCode2RoomTypeMap = new HashMap<>();
            if (roomStay.getRoomTypes() != null) {
                for (RoomTypeType roomType : roomStay.getRoomTypes().getRoomTypeList()) {
                    roomTypeCode2RoomTypeMap.put(roomType.getRoomGroup().getRoomTypeCode(), roomType);
                }
            }

            // Map: RatePlanCode -> RatePlan
            final Map<String, RatePlanType> ratePlanCode2RatePlanMap = new HashMap<>();
            final RoomStayType.RatePlans ratePlans = roomStay.getRatePlans();
            if (ratePlans != null) {
                final List<RatePlanType> ratePlanList = ratePlans.getRatePlanList();
                if (ratePlanList != null && !ratePlanList.isEmpty()) {
                    for (RatePlanType ratePlan : ratePlanList) {
                        ratePlanCode2RatePlanMap.put(ratePlan.getRatePlanCode(), ratePlan);
                    }
                }
            }


            // Hotel
            Hotel hotel = new Hotel();
            hotel.setName(roomStay.getBasicPropertyInfo().getHotelReferenceGroup().getHotelName());
            hotel.setHotelId(roomStay.getBasicPropertyInfo().getHotelReferenceGroup().getHotelCode());
            //hotel.setPicture("http://www.hotel.de/media/marketing/deals/deals_370x130_de.jpg");

            String logo = getLogo(roomStay);
            if (!TextUtils.isEmpty(logo)) {
                hotel.setPicture(Uri.parse(getLogo(roomStay)));
            }

            // Address
            setAddress(roomStay, hotel);

            // Rating
            setRatings(roomStay, hotel);

            // Distances
            setDistances(roomStay, hotel);

            // TODO Have a look at the currency handling overall
            String iso3CurrencyHotel = roomStay.getRoomRates().getRoomRateList().get(0).getRates().getRateList().get(0).getBase().getCurrencyCodeGroup().getCurrencyCode();
            BigDecimal customerCurrencyFactor = hdeCurrencyConversionResultMapper.getCustomerCurrencyFactor(currencyConversions, iso3CurrencyHotel, iso3CurrencyCustomer);


			/*
             * HDE v3.0, as of today, allows only one room type to be requested
			 * at once. Therefore, all offers (List of {@link
			 * RoomStayType.RoomRates.RoomRate}) must belong to the same room.
			 */
            List<HdeRatePlan> hdeRatePlanList = new ArrayList<>();
            HotelOffer hotelOffer = new HotelOffer();

            for (RoomStayType.RoomRates.RoomRate roomRate : roomStay.getRoomRates().getRoomRateList()) {
                if (roomRate == null) {
                    Timber.d("roomRate Loop(): roomRate is null");
                    continue;
                }

                RatePlanGroup ratePlanGroup = roomRate.getRatePlanGroup();
                if (ratePlanGroup == null) {
                    Timber.d("roomRate Loop(): group is null");
                    continue;
                }

                final String ratePlanCode = ratePlanGroup.getRatePlanCode();
                final RatePlanType ratePlanType = ratePlanCode2RatePlanMap.get(ratePlanCode);

                //Sometimes (for Booking) the ratePlanCode provided by the roomStay - ratePlanGroup
                // is not the same as that provided by the roomStay - ratePlan.
                // So the mapper will get a null object for the RatePlanType
                // 06.08.2015 checked with Vivi this will be fixed in next Release
                if (ratePlanType == null) {
                    Timber.d("roomRate Loop(): ratePlanType is null");
                    continue;
                }

                final RatePlanType.Sequence ratePlanSequence = ratePlanType.getSequence();

                HdeRoomRate hdeRoomRate = new HdeRoomRate();
                hdeRoomRate.setIso3CurrencyCustomer(iso3CurrencyCustomer);
                hdeRoomRate.setIso3CurrencyHotel(iso3CurrencyHotel);

                RateType rateTypeRates = roomRate.getRates();
                addDateOffer(rateTypeRates, hdeRoomRate);

                final String roomTypeCode = roomRate.getRoomTypeCode();
                final RoomTypeType roomType = roomTypeCode2RoomTypeMap.get(roomTypeCode);
                hdeRoomRate.setRoomDescription(mapRoomDescription(roomType));

                HdeRatePlan hdeRatePlan = new HdeRatePlan();
                if (!TextUtils.isEmpty(ratePlanType.getRatePlanType())) {
                    //noinspection ResourceType Reason: This is correct
                    hdeRatePlan.setRatePlanType(Integer.valueOf(ratePlanType.getRatePlanType()));
                }
                addRoomRates(hdeRatePlan, roomRate, ratePlanSequence);
                setAveragePriceToOffer(hdeRoomRate, hdeRatePlan, customerCurrencyFactor);

                HdePaymentInfo paymentInfo = createPaymentInfo(hotel, ratePlanType, hdeRatePlan);
                hdeRatePlan.setPaymentInfo(paymentInfo);
                hdeRatePlan.setRoomRate(hdeRoomRate);

                hdeRatePlan.setBookingGuarantee(mapBookingGuarantee(paymentInfo));

                HdeRatePlan cheapest = hotelOffer.getCheapestRatePlan();
                if (cheapest == null || hdeRatePlan.getTotalAmountAfterTax().compareTo(cheapest.getTotalAmountAfterTax()) < 0) {
                    hotelOffer.setCheapestRatePlan(hdeRatePlan);
                }

                HdeRatePlan cheapestCancelable = hotelOffer.getCheapestCancelableRatePlan();
                if (paymentInfo.isCancelable() && (cheapestCancelable == null || hdeRatePlan.getTotalAmountAfterTax().compareTo(cheapestCancelable.getTotalAmountAfterTax()) < 0)) {
                    hotelOffer.setCheapestCancelableRatePlan(hdeRatePlan);
                }

                hdeRatePlanList.add(hdeRatePlan);
            }

            sortRoomRates(hdeRatePlanList);

            hotelOffer.setRatePlans(hdeRatePlanList);
            hotel.setHotelOffer(hotelOffer);
            hotelList.add(hotel);
        }

        return hotelList;
    }

    private void sortRoomRates(List<HdeRatePlan> ratePlans) {
        Collections.sort(ratePlans, new Comparator<HdeRatePlan>() {
            @Override
            public int compare(HdeRatePlan lhs, HdeRatePlan rhs) {
                return lhs.getRoomRate().getTotalRoomPriceGrossHotel().compareTo(rhs.getRoomRate().getTotalRoomPriceGrossHotel());
            }
        });

        if (ratePlans.size() < 3) {
            return;
        }

        if (ratePlans.get(0).getPaymentInfo().isCancelable() || ratePlans.get(1).getPaymentInfo().isCancelable()) {
            return;
        }

        // The second rate should be a cancelable rate if the first one isn't cancelable already
        for (int i = 2; i < ratePlans.size(); i++) {
            if (ratePlans.get(i).getPaymentInfo().isCancelable()) {
                Collections.swap(ratePlans, 1, i);
                break;
            }
        }
    }

    private String getLogo(RoomStayType roomStay) {
        RoomStayType.BasicPropertyInfo info = roomStay.getBasicPropertyInfo();
        if (info == null
                || info.getVendorMessages() == null
                || info.getVendorMessages().getVendorMessageList() == null) {
            return null;
        }

        VendorMessageType imageVendorMessageType = null;
        for (VendorMessageType type : info.getVendorMessages().getVendorMessageList()) {
            if (type.getInfoType().equals(INFO_TYPE_PICTURES)) {
                imageVendorMessageType = type;
                break;
            }
        }
        if (imageVendorMessageType == null) {
            return null;
        }
        List<FormattedTextSubSectionType> subSectionList = imageVendorMessageType.getSubSectionList();
        if (subSectionList == null || subSectionList.isEmpty()) {
            return null;
        }
        List<ParagraphType> paragraphList = subSectionList.get(0).getParagraphList();
        if (paragraphList == null || paragraphList.isEmpty()) {
            return null;
        }
        List<ParagraphType.Sequence> textList = paragraphList.get(0).getTextList();
        if (textList == null || textList.isEmpty()) {
            return null;
        }
        return textList.get(0).getURL();
    }


    private void setAddress(RoomStayType roomStay, Hotel hotel) {
        Address address = new Address();
        address.setStreet(roomStay.getBasicPropertyInfo().getAddress().getAddressLineList().get(0)); // TODO check if 0 is suitable
        address.setCity(roomStay.getBasicPropertyInfo().getAddress().getCityName());
        address.setCountryIsoA3(roomStay.getBasicPropertyInfo().getAddress().getCountryName().getCode());
        address.setPostalCode(roomStay.getBasicPropertyInfo().getAddress().getPostalCode());

        if (roomStay.getBasicPropertyInfo().getPosition() != null
                && roomStay.getBasicPropertyInfo().getPosition().getPositionGroup() != null
                && roomStay.getBasicPropertyInfo().getPosition().getPositionGroup().getLatitude() != null) {

            GeoPosition geoPosition = new GeoPosition();
            geoPosition.setLatitude(Double.valueOf(roomStay.getBasicPropertyInfo().getPosition().getPositionGroup().getLatitude()));
            geoPosition.setLongitude(Double.valueOf(roomStay.getBasicPropertyInfo().getPosition().getPositionGroup().getLongitude()));
            address.setGeoPosition(geoPosition);
        }

        hotel.setAddress(address);
    }

    private void setDistances(RoomStayType roomStay, Hotel hotel) {
        if (roomStay.getBasicPropertyInfo().getRelativePosition() == null) {
            return;
        }

        List<Distance> distances = new ArrayList<>();

        setDestinationDistance(roomStay, distances);
        setTransportationDistances(roomStay, distances);

        hotel.setDistances(distances);
    }

    private void setTransportationDistances(RoomStayType roomStay, List<Distance> distances) {
        if (roomStay.getBasicPropertyInfo().getRelativePosition().getTransportations() == null) {
            return;
        }

        for (TransportationType.Transportation transportation : roomStay.getBasicPropertyInfo().getRelativePosition().getTransportations().getTransportationList()) {
            Distance distance = new Distance();

            try {
                distance.setDistanceInKm(parseTransportationText(transportation.getDescriptiveText()));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            switch (transportation.getTransportationCode()) {
                case TRANSPORTATION_HIGHWAY:
                    distance.setLocationType(LocationType.HIGHWAY);
                    break;
                case TRANSPORTATION_AIRPORT:
                    distance.setLocationType(LocationType.AIRPORT);
                    break;
                case TRANSPORTATION_TRAIN_STATION:
                    distance.setLocationType(LocationType.TRAIN_STATION);
                    break;
                default:
                    continue;
            }

            distances.add(distance);
        }
    }

    private void setDestinationDistance(RoomStayType roomStay, List<Distance> distances) {
        Distance destinationDistance = new Distance();
        // TODO: Setting the location type city is not always correct.
        // HSBW does not tell us what the attribute "distance" in the node "RelativePosition" actually means.
        // When searching in a city, it means the distance to the city center. When searching at a POI,
        // it means the distance to the POI.
        // At this point we do not know what type was searched.
        destinationDistance.setLocationType(LocationType.CITY);
        destinationDistance.setDistanceInKm(Float.valueOf(roomStay.getBasicPropertyInfo().getRelativePosition().getRelativePositionGroup().getDistance()));

        distances.add(destinationDistance);
    }

    private float parseTransportationText(String transportationText) throws ParseException {
        String km = transportationText.replace(KM, "").trim();
        NumberFormat numberFormat = NumberFormat.getInstance(Locale.GERMAN);

        return numberFormat.parse(km).floatValue();
    }

    private void setRatings(RoomStayType roomStay, Hotel hotel) {
        for (BasicPropertyInfoType.Award award : roomStay.getBasicPropertyInfo().getAwardList()) {

            Float rating = Float.valueOf(award.getRating());
            if (Float.compare(rating, 0) < 0) {
                continue;
            }

            switch (award.getProvider()) {
                case AWARD_PROVIDER_HOTEL_DE:
                    hotel.setStarsRating(rating);
                    break;
                case AWARD_PROVIDER_HOTEL_DE_UGCR:
                    hotel.setUserRating(rating);
                    break;
            }
        }
    }

    private void addDateOffer(RateType rateType, HdeRoomRate hdeRoomRate) {
        if (rateType == null) {
            Timber.d("addDateOffer(): rateTypeRates is null");
            return;
        }

        List<RateType.Rate> rates = rateType.getRateList();
        if (rates != null && !rates.isEmpty()) {

            for (RateType.Rate rate : rates) {
                HdeRate dateOffer = new HdeRate();
                TotalType base = rate.getBase();
                if (base != null) {
                    dateOffer.setAmountAfterTax(base.getAmountAfterTax());
                    dateOffer.setAmountBeforeTax(base.getAmountAfterTax());
                    dateOffer.setCurrencyCode(base.getCurrencyCodeGroup().getCurrencyCode());
                }

                EffectiveExpireOptionalDateGroup dateGroup = rate.getEffectiveExpireOptionalDateGroup();
                if (dateGroup != null) {
                    Date effDate = dateGroup.getEffectiveDate();
                    if (effDate != null) {
                        dateOffer.setEffectiveDate(effDate.toString());
                    }

                    Date expDate = dateGroup.getExpireDate();
                    if (expDate != null) {
                        dateOffer.setExpireDate(expDate.toString());
                    }
                }
                hdeRoomRate.getHdeRates().add(dateOffer);
            }
        }
    }

    private HdePaymentInfo createPaymentInfo(Hotel hotel, RatePlanType ratePlan, HdeRatePlan hdeRatePlan) {
        HdePaymentInfo paymentInfo = new HdePaymentInfo();

        if (ratePlan.getPrepaidIndicator() != null) {
            paymentInfo.setPrepaidIndicator(ratePlan.getPrepaidIndicator());
        }

        RatePlanType.Sequence sequence = ratePlan.getSequence();
        if (sequence == null) {
            return paymentInfo;
        }

        ParagraphType ratePlanDescription = sequence.getRatePlanDescription();
        if (ratePlanDescription != null) {
            List<ParagraphType.Sequence> textList = ratePlanDescription.getTextList();
            if (textList != null && textList.size() > 0) {
                paymentInfo.setRateDescription(textList.get(0).getText().getString());
            }
        }

        /*
        * extract cancelPenalty information
        */
        List<HdeCancelPenalty> hdeCancelPenaltyList = new ArrayList<>();
        CancelPenaltiesType cancelPenaltiesType = sequence.getCancelPenalties();
        if (cancelPenaltiesType != null) {
            List<CancelPenaltyType> cancelPenaltyList = cancelPenaltiesType.getCancelPenaltyList();
            if (cancelPenaltyList != null && cancelPenaltyList.size() > 0) {
                CancelPenaltyType cancelPenalty = cancelPenaltyList.get(0);

                boolean noCancellationPossible = Booleans.booleanValue(cancelPenalty.getNoCancelInd());
                paymentInfo.setCancelable(!noCancellationPossible);
                DeadlineGroup deadline = cancelPenalty.getDeadline();
                if (deadline != null) {
                    try {
                        paymentInfo.setCancellationDeadline(Dates.UTC_COMBINED_DATE_TIME_FORMAT.parse(deadline.getAbsoluteDeadline()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                        paymentInfo.setCancellationDeadline(null);
                    }
                } else {
                    paymentInfo.setCancellationDeadline(null);
                }

                List<ParagraphType> descriptions = cancelPenalty.getPenaltyDescriptionList();
                if (descriptions != null) {
                    for (ParagraphType description : descriptions) {
                        List<ParagraphType.Sequence> textSequenceList = description.getTextList();
                        for (ParagraphType.Sequence textSequence : textSequenceList) {
                            HdeCancelPenalty hdeCancelPenalty = new HdeCancelPenalty();
                            hdeCancelPenalty.setText(textSequence.getText().getString());
                            hdeCancelPenalty.setLanguage(textSequence.getText().getLanguage());
                            hdeCancelPenaltyList.add(hdeCancelPenalty);
                        }
                    }
                    paymentInfo.setCancelPenaltyList(hdeCancelPenaltyList);
                }
            }
        }


        /*
         * extract meals included information
         */
        addMealInformation(paymentInfo, sequence);

        /*
         * extract the additional details
         */

        getAdditionalDetails(hotel, paymentInfo, sequence);

        /*
         + extract the guarantee information
         */
        List<RatePlanType.Sequence.Guarantee> guaranteeList = sequence.getGuaranteeList();
        if (guaranteeList != null && guaranteeList.size() > 0) {
            RatePlanType.Sequence.Guarantee guarantee = guaranteeList.get(0);
            paymentInfo.setHoldTime(guarantee.getHoldTime());
            final GuaranteeType.GuaranteeTypeInner guaranteeTypeValue = guarantee.getGuaranteeType();

            RatePlanType.Sequence.Guarantee.AmountPercent amountPercent = guarantee.getAmountPercent();
            if (amountPercent != null) {
                CurrencyAmountGroup currencyAmountGroup = amountPercent.getCurrencyAmountGroup();
                if (currencyAmountGroup != null) {
                    paymentInfo.setDepositAmount(currencyAmountGroup.getAmount());
                    paymentInfo.setDepositCurrencyCode(currencyAmountGroup.getCurrencyCode());
                } else if (amountPercent.getPercent() != null && hdeRatePlan.getTotalAmountAfterTax() != null) {
                    paymentInfo.setDepositAmount(amountPercent.getPercent().multiply(hdeRatePlan.getTotalAmountAfterTax()).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP));
                    paymentInfo.setDepositCurrencyCode(hdeRatePlan.getCurrencyCode());
                }
            }

            @HdeGuaranteeType int hdeGuaranteeType = mapGuaranteeType(guaranteeTypeValue);
            paymentInfo.setGuaranteeType(hdeGuaranteeType);

            GuaranteeType.GuaranteesAccepted guaranteeAcceptees = guarantee.getGuaranteesAccepted();
            if (guaranteeAcceptees != null) {
                List<GuaranteeType.GuaranteesAccepted.GuaranteeAccepted> guaranteeAcceptedList = guaranteeAcceptees.getGuaranteeAcceptedList();
                List<Integer> cardTypes = new ArrayList<>();

                for (GuaranteeType.GuaranteesAccepted.GuaranteeAccepted guaranteeAccepted : guaranteeAcceptedList) {
                    PaymentFormType.Sequence acceptedSequence = guaranteeAccepted.getSequence();
                    PaymentCardType paymentCardType = acceptedSequence.getPaymentCard();
                    String cardCode = paymentCardType.getCardCode();
                    cardTypes.add(creditCardMapper.mapFromCreditCardCode(cardCode));
                }
                paymentInfo.setCreditCardTypes(cardTypes);
            }
        }

        return paymentInfo;
    }


    private BookingGuarantee mapBookingGuarantee(@NonNull HdePaymentInfo paymentInfo) {

        BookingGuarantee bookingGuarantee = new BookingGuarantee();

        switch (paymentInfo.getGuaranteeType()) {
            case HdeGuaranteeType.NONE:
                bookingGuarantee.setCreditCardNeeded(false);
                break;
            case HdeGuaranteeType.DEPOSIT:
                bookingGuarantee.setCreditCardNeeded(true);
                bookingGuarantee.setPrepaidNeeded(true);
                break;
            case HdeGuaranteeType.GUARANTEE_REQUIRED:
                Date holdTime = paymentInfo.getHoldTime();
                if (holdTime != null) {
                    bookingGuarantee.setCreditCardNeeded(false);
                } else {
                    bookingGuarantee.setCreditCardNeeded(true);
                }
                break;

            //TODO: Ignored for the moment check if these values
            // are really needed
            case HdeGuaranteeType.DEPOSIT_REQUIRED:
            case HdeGuaranteeType.PROFILE:
            case HdeGuaranteeType.CCDC_VOUCHER:
            case HdeGuaranteeType.PRE_PAY:
                bookingGuarantee.setCreditCardNeeded(false);
                break;
        }
        return bookingGuarantee;
    }

    private
    @HdeGuaranteeType int mapGuaranteeType(GuaranteeType.GuaranteeTypeInner innerType) {
        String guaranteeType = innerType.name();

        if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.GUARANTEE_REQUIRED.name())) {
            return HdeGuaranteeType.GUARANTEE_REQUIRED;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.DEPOSIT.name())) {
            return HdeGuaranteeType.DEPOSIT;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.DEPOSIT_REQUIRED.name())) {
            return HdeGuaranteeType.DEPOSIT_REQUIRED;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.CCDC_VOUCHER.name())) {
            return HdeGuaranteeType.CCDC_VOUCHER;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.PRE_PAY.name())) {
            return HdeGuaranteeType.PRE_PAY;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.PROFILE.name())) {
            return HdeGuaranteeType.PROFILE;
        } else if (guaranteeType.equals(GuaranteeType.GuaranteeTypeInner.NONE.name())) {
            return HdeGuaranteeType.NONE;
        }
        return HdeGuaranteeType.NONE;
    }

    private void addMealInformation(HdePaymentInfo paymentInfo, RatePlanType.Sequence sequence) {
        paymentInfo.setBreakFastIncluded(false);
        paymentInfo.setLunchIncluded(false);
        paymentInfo.setDinnerIncluded(false);

        MealsIncludedGroup mealsIncluded = sequence.getMealsIncluded();
        if (mealsIncluded == null) {
            return;
        }

        Boolean breakfastIncluded = mealsIncluded.getBreakfast();
        paymentInfo.setBreakFastIncluded(breakfastIncluded != null && breakfastIncluded);

        Boolean lunchIncluded = mealsIncluded.getLunch();
        paymentInfo.setLunchIncluded(lunchIncluded != null && lunchIncluded);

        Boolean dinnerIncluded = mealsIncluded.getDinner();
        paymentInfo.setDinnerIncluded(dinnerIncluded != null && dinnerIncluded);
    }


    private void getAdditionalDetails(Hotel hotel, HdePaymentInfo paymentInfo, RatePlanType.Sequence sequence) {
        List<HdeAdditionalDetails> hdeAdditionalDetailsList = new ArrayList<>();
        paymentInfo.setAdditionalDetailList(hdeAdditionalDetailsList);

        AdditionalDetailsType additionalDetails = sequence.getAdditionalDetails();
        if (additionalDetails == null) {
            return;
        }

        for (AdditionalDetailType additionalDetailType : additionalDetails.getAdditionalDetailList()) {
            // Reception times belong to the hotel. They are in the additional details because of the OTA standard.
            if (mapAdditionalDetailsType(additionalDetailType.getType()) == de.hotel.android.library.domain.model.enums.AdditionalDetailsType.RECEPTION) {
                if (hotel.getReceptionTimes() == null || hotel.getReceptionTimes().isEmpty()) {
                    hotel.setReceptionTimes(additionalDetailType.getDetailDescription().getTextList().get(0).getText().getString());
                }
                continue;
            }

            hdeAdditionalDetailsList.add(getAdditionalDetails(additionalDetailType));
        }
    }


    private HdeAdditionalDetails getAdditionalDetails(@NonNull AdditionalDetailType additionalDetailType) {
        HdeAdditionalDetails hdeAdditionalDetails = new HdeAdditionalDetails();

        String detailsTypeStr = additionalDetailType.getType();
        hdeAdditionalDetails.setDetailsType(mapAdditionalDetailsType(detailsTypeStr));

        CurrencyAmountGroup currencyAmountGroup = additionalDetailType.getCurrencyAmountGroup();
        if (currencyAmountGroup != null) {
            hdeAdditionalDetails.setAmount(currencyAmountGroup.getAmount());
            hdeAdditionalDetails.setCurrencyCode(currencyAmountGroup.getCurrencyCode());
        }

        ParagraphType detailDescription = additionalDetailType.getDetailDescription();
        if (detailDescription != null) {

            List<String> descriptions = new ArrayList<>();

            String name = detailDescription.getName();
            if (!TextUtils.isEmpty(name)) {
                descriptions.add(name);
            }

            List<ParagraphType.Sequence> textList = detailDescription.getTextList();
            if (textList != null && textList.size() != 0) {

                for (ParagraphType.Sequence textSequence : textList) {
                    descriptions.add(textSequence.getText().getString());
                }
            }

            hdeAdditionalDetails.setDescriptions(descriptions);
        }

        return hdeAdditionalDetails;
    }

    @de.hotel.android.library.domain.model.enums.AdditionalDetailsType
    private int mapAdditionalDetailsType(@NonNull String detailsType) {

        switch (detailsType) {
            case DETAILS_BREAKFAST:
                return de.hotel.android.library.domain.model.enums.AdditionalDetailsType.BREAKFAST;
            case DETAILS_AMENITIES:
                return de.hotel.android.library.domain.model.enums.AdditionalDetailsType.AMENITIES;
            case DETAILS_RECEPTION:
                return de.hotel.android.library.domain.model.enums.AdditionalDetailsType.RECEPTION;
            default:
                return de.hotel.android.library.domain.model.enums.AdditionalDetailsType.NONE;
        }
    }

    private RoomDescription mapRoomDescription(RoomTypeType roomType) {
        if (roomType == null || roomType.getRoomDescription() == null) {
            return null;
        }

        List<ParagraphType.Sequence> roomDescriptionTextList = roomType.getRoomDescription().getTextList();
        RoomDescription roomDescription = new RoomDescription();

        List<String> roomDescriptionList = new ArrayList<>();
        List<String> roomImageList = new ArrayList<>();

        for (ParagraphType.Sequence roomDescriptionText : roomDescriptionTextList) {

            FormattedTextTextType text = roomDescriptionText.getText();
            if (text != null) {
                roomDescriptionList.add(text.getString());
            }
            String image = roomDescriptionText.getImage();
            if (!TextUtils.isEmpty(image)) {
                roomImageList.add(image);
            }

        }
        roomDescription.setDescriptionLong(roomDescriptionList);
        roomDescription.setImageList(roomImageList);

        return roomDescription;

    }

    private void addRoomRates(HdeRatePlan hdeRatePlan,
                              @NonNull RoomStayType.RoomRates.RoomRate roomRate,
                              @Nullable RatePlanType.Sequence ratePlanSequence) {
        List<String> ratePlanDescriptions = getRateDescriptions(ratePlanSequence);
        hdeRatePlan.setRatePlanDescriptions(ratePlanDescriptions);

        hdeRatePlan.setBookingCode(roomRate.getBookingCode());
        hdeRatePlan.setInvBlockCode(roomRate.getInvBlockCode());
        hdeRatePlan.setRoomTypeCode(roomRate.getRoomTypeCode());
        hdeRatePlan.setRatePlanCode(roomRate.getRatePlanGroup().getRatePlanCode());

        int numberOfRequestedRooms = roomRate.getNumberOfUnits().intValue();
        hdeRatePlan.setNumberOfRequestedRooms(numberOfRequestedRooms);

        if (roomRate.getTotal() != null) {

            BigDecimal amountAfterTax = roomRate.getTotal().getAmountAfterTax();
            BigDecimal amountBeforeTax = roomRate.getTotal().getAmountBeforeTax();

            hdeRatePlan.setTotalAmountBeforeTax(amountBeforeTax);
            hdeRatePlan.setTotalAmountAfterTax(amountAfterTax);
            hdeRatePlan.setCurrencyCode(roomRate.getTotal().getCurrencyCodeGroup().getCurrencyCode());
        }
    }

    private void setAveragePriceToOffer(HdeRoomRate hdeRoomRate, HdeRatePlan hdeRatePlan, @Nullable BigDecimal currencyFactor) {

        BigDecimal amountBeforeTax = hdeRatePlan.getTotalAmountBeforeTax();
        BigDecimal amountAfterTax = hdeRatePlan.getTotalAmountAfterTax();

        int numberOfRooms = hdeRatePlan.getNumberOfRequestedRooms();
        if (amountAfterTax != null) {
            final BigDecimal totalRoomPriceGrossHotel = amountAfterTax.divide(BigDecimal.valueOf(numberOfRooms), 2, RoundingMode.HALF_UP);
            hdeRoomRate.setTotalRoomPriceGrossHotel(totalRoomPriceGrossHotel);
            if (currencyFactor != null) {
                hdeRoomRate.setTotalRoomPriceGrossCustomer(totalRoomPriceGrossHotel.multiply(currencyFactor));
            }
        }

        if (amountBeforeTax != null) {
            final BigDecimal totalRoomPriceNetHotel = amountBeforeTax.divide(BigDecimal.valueOf(numberOfRooms), 2, RoundingMode.HALF_UP);
            hdeRoomRate.setTotalRoomPriceNetHotel(totalRoomPriceNetHotel);
            if (currencyFactor != null) {
                hdeRoomRate.setTotalRoomPriceNetCustomer(totalRoomPriceNetHotel.multiply(currencyFactor));
            }
        }

    }

    private List<String> getRateDescriptions(RatePlanType.Sequence ratePlanSequence) {
        List<String> ratePlanDescriptions = new ArrayList<>();
        if (ratePlanSequence == null) {
            return ratePlanDescriptions;
        }

        ParagraphType ratePlanDescription = ratePlanSequence.getRatePlanDescription();
        if (ratePlanDescription != null) {
            List<ParagraphType.Sequence> ratePlanTextList = ratePlanDescription.getTextList();
            if (ratePlanTextList != null) {
                for (ParagraphType.Sequence sequence : ratePlanTextList) {
                    String ratePlanString = sequence.getText().getString();
                    ratePlanDescriptions.add(ratePlanString);
                }
            }
        }

        return ratePlanDescriptions;
    }
}
