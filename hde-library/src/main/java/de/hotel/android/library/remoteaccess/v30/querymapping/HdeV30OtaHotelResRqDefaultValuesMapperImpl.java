package de.hotel.android.library.remoteaccess.v30.querymapping;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelResRQ;

public class HdeV30OtaHotelResRqDefaultValuesMapperImpl implements HdeV30OtaHotelResRqMapper {
    private static final String MOBILER_BOOKER = "mobile-booker";
    private static final String MOBILER_BOOKER_COUNTRY = "DEU";

    private final HdeV30OtaHotelResRqMapper hdeV30OtaHotelResRqMapper;

    public HdeV30OtaHotelResRqDefaultValuesMapperImpl(@NonNull HdeV30OtaHotelResRqMapper hdeV30OtaHotelResRqMapper) {
        this.hdeV30OtaHotelResRqMapper = hdeV30OtaHotelResRqMapper;
    }

    @Override
    public OTAHotelResRQ createOTAHotelResRequest(@NonNull HotelReservationQuery hotelReservationQuery, @RemoteAccessTargetEnvironmentType int remoteAccessTargetEnvironmentType) {
        setAddressDefaultsIfNotSet(hotelReservationQuery);

        return hdeV30OtaHotelResRqMapper.createOTAHotelResRequest(hotelReservationQuery, remoteAccessTargetEnvironmentType);
    }

    private void setAddressDefaultsIfNotSet(HotelReservationQuery hotelReservationQuery) {
        setAddressDefaultsIfNotSet(hotelReservationQuery.getBookingPerson());
    }

    private void setAddressDefaultsIfNotSet(Customer customer) {
        Address address = customer.getAddress();

        if (address == null) {
            address = new Address();
        }

        if (TextUtils.isEmpty(address.getStreet())) {
            address.setStreet(MOBILER_BOOKER);
        }

        if (TextUtils.isEmpty(address.getCity())) {
            address.setCity(MOBILER_BOOKER);
        }

        if (TextUtils.isEmpty(address.getPostalCode())) {
            address.setPostalCode(MOBILER_BOOKER);
        }

        if (TextUtils.isEmpty(address.getCountryIsoA3())) {
            address.setCountryIsoA3(MOBILER_BOOKER_COUNTRY);
        }

        customer.setAddress(address);
    }
}
