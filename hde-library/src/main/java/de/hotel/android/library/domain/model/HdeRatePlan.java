package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import de.hotel.android.library.domain.model.enums.RatePlanType;

public class HdeRatePlan implements Parcelable {

    public static final Parcelable.Creator<HdeRatePlan> CREATOR = new Parcelable.Creator<HdeRatePlan>() {
        @Override
        public HdeRatePlan createFromParcel(Parcel parcel) {
            return new HdeRatePlan(parcel);
        }

        @Override
        public HdeRatePlan[] newArray(int i) {
            return new HdeRatePlan[i];
        }
    };
    private List<String> ratePlanDescriptions = new ArrayList<>();
    private String bookingCode;
    private BigDecimal totalAmountBeforeTax;
    private BigDecimal totalAmountAfterTax;
    private String currencyCode;
    private int numberOfRequestedRooms;
    private int numberOfDays;
    private HdeRoomRate roomRate;
    private String invBlockCode;
    private String roomTypeCode;
    private String ratePlanCode;
    private @RatePlanType int ratePlanType;
    private HdePaymentInfo paymentInfo;

    private BookingGuarantee bookingGuarantee;

    public HdeRatePlan() {
    }

    public HdeRatePlan(Parcel parcel) {
        parcel.readStringList(ratePlanDescriptions);

        bookingCode = parcel.readString();
        currencyCode = parcel.readString();

        totalAmountBeforeTax = (BigDecimal) parcel.readSerializable();
        totalAmountAfterTax = (BigDecimal) parcel.readSerializable();

        numberOfRequestedRooms = parcel.readInt();
        numberOfDays = parcel.readInt();

        roomRate = parcel.readParcelable(HdeRoomRate.class.getClassLoader());

        invBlockCode = parcel.readString();
        roomTypeCode = parcel.readString();
        ratePlanCode = parcel.readString();
        ratePlanType = parcel.readInt();

        paymentInfo = parcel.readParcelable(HdePaymentInfo.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(ratePlanDescriptions);

        parcel.writeString(bookingCode);
        parcel.writeString(currencyCode);

        parcel.writeSerializable(totalAmountBeforeTax);
        parcel.writeSerializable(totalAmountAfterTax);

        parcel.writeInt(numberOfRequestedRooms);
        parcel.writeInt(numberOfDays);

        parcel.writeParcelable(roomRate, i);

        parcel.writeString(invBlockCode);
        parcel.writeString(roomTypeCode);
        parcel.writeString(ratePlanCode);
        parcel.writeInt(ratePlanType);

        parcel.writeParcelable(paymentInfo, i);

    }

    public HdeRoomRate getRoomRate() {
        return roomRate;
    }

    public void setRoomRate(HdeRoomRate roomRate) {
        this.roomRate = roomRate;
    }

    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("\nbooking Code=").append(bookingCode);
        builder.append("\nnum Days=").append(String.valueOf(numberOfDays));
        builder.append("\nnum Rooms=").append(String.valueOf(numberOfRequestedRooms));
        builder.append("\nrateName=").append(getRatePlanDescription());
        if (totalAmountAfterTax != null) {
            builder.append("\ntotalAmmountAfertTax=").append(String.valueOf(totalAmountAfterTax.floatValue()));
        } else {
            builder.append("\ntotalAmmountAfertTax=").append("Undefined");
        }
        builder.append("\ncurrency").append(currencyCode);

        return builder.toString();
    }

    public List<String> getRatePlanDescriptions() {
        return ratePlanDescriptions;
    }

    public void setRatePlanDescriptions(List<String> ratePlanDescriptions) {
        this.ratePlanDescriptions = ratePlanDescriptions;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getRatePlanDescription() {
        return ratePlanDescriptions.size() > 0 ? ratePlanDescriptions.get(0) : "";
    }

    public BigDecimal getTotalAmountBeforeTax() {
        return totalAmountBeforeTax;
    }

    public void setTotalAmountBeforeTax(BigDecimal totalAmountBeforeTax) {
        this.totalAmountBeforeTax = totalAmountBeforeTax;
    }

    public BigDecimal getTotalAmountAfterTax() {
        return totalAmountAfterTax;
    }

    public void setTotalAmountAfterTax(BigDecimal totalAmountAfterTax) {
        this.totalAmountAfterTax = totalAmountAfterTax;
    }

    public int getNumberOfRequestedRooms() {
        return numberOfRequestedRooms;
    }

    public void setNumberOfRequestedRooms(int numberOfRequestedRooms) {
        this.numberOfRequestedRooms = numberOfRequestedRooms;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public String getInvBlockCode() {
        return invBlockCode;
    }

    public void setInvBlockCode(String invBlockCode) {
        this.invBlockCode = invBlockCode;
    }

    public String getRoomTypeCode() {
        return roomTypeCode;
    }

    public void setRoomTypeCode(String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }

    public String getRatePlanCode() {
        return ratePlanCode;
    }

    public void setRatePlanCode(String ratePlanCode) {
        this.ratePlanCode = ratePlanCode;
    }

    public @RatePlanType int getRatePlanType() {
        return ratePlanType;
    }

    public void setRatePlanType(@RatePlanType int ratePlanType) {
        this.ratePlanType = ratePlanType;
    }

    public HdePaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(HdePaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BookingGuarantee getBookingGuarantee() {
        return bookingGuarantee;
    }

    public void setBookingGuarantee(BookingGuarantee bookingGuarantee) {
        this.bookingGuarantee = bookingGuarantee;
    }
}
