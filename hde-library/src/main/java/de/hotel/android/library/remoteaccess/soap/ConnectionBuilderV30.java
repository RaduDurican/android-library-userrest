package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.net.URLConnection;
import java.text.MessageFormat;

public class ConnectionBuilderV30 implements ConnectionBuilder {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_APPLICATION_SOAP_XML = "application/soap+xml;";
    private static final String SOAP_ACTION_PATTERN = "action=\"{0}\"";

    private final ConnectionBuilder connectionBuilder;

    public ConnectionBuilderV30(@NonNull ConnectionBuilder connectionBuilder) {
        this.connectionBuilder = connectionBuilder;
    }

    @Override
    public URLConnection createUrlConnection(int requestBodyByteCount, String soapAction) throws IOException {
        URLConnection urlConnection = connectionBuilder.createUrlConnection(requestBodyByteCount, soapAction);
        urlConnection.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_APPLICATION_SOAP_XML + MessageFormat.format(SOAP_ACTION_PATTERN, soapAction));

        return urlConnection;
    }
}
