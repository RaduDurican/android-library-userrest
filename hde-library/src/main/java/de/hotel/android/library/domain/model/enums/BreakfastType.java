package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

/**
 * @author Johannes Mueller
 */
@IntDef({BreakfastType.UNKNOWN, BreakfastType.INCLUSIVE, BreakfastType.EXCLUSIVE, BreakfastType.NOT_AVAILABLE, BreakfastType.VARIOUS, BreakfastType.INCLUSIVE_HALF_BOARD,
        BreakfastType.INCLUSIVE_FULL_BOARD, BreakfastType.ALL_INCLUSIVE})
public @interface BreakfastType {

    int UNKNOWN = 0;
    int INCLUSIVE = 1;
    int EXCLUSIVE = 2;
    int NOT_AVAILABLE = 3;
    int VARIOUS = 4;
    int INCLUSIVE_HALF_BOARD = 5;
    int INCLUSIVE_FULL_BOARD = 6;
    int ALL_INCLUSIVE = 7;
}
