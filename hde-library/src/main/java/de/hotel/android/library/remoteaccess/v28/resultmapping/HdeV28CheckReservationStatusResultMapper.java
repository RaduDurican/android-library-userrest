package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import de.hotel.android.library.domain.model.Reservation;
import de.hotel.android.library.domain.model.ReservationRate;
import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.HdeGuest;
import de.hotel.android.library.domain.model.enums.CancellationType;
import de.hotel.android.library.domain.model.enums.ReservationStatus;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.util.Dates;
import de.hotel.remoteaccess.v28.model.ArrayOfGuest;
import de.hotel.remoteaccess.v28.model.CheckReservationStatusResponse1;
import de.hotel.remoteaccess.v28.model.Guest;
import de.hotel.remoteaccess.v28.model.ReservationInfo;
import de.hotel.remoteaccess.v28.model.ReservationSingleRate;

public class HdeV28CheckReservationStatusResultMapper {

    public List<Reservation> mapCheckReservationStatusResponse(@NonNull CheckReservationStatusResponse1 response) {

        if (response.getReservations() == null || response.getReservations().getReservationInfoList() == null || response.getReservations().getReservationInfoList().isEmpty()) {
            return Collections.emptyList();
        }

        List<Reservation> reservations = new ArrayList<>();
        for (int i = 0; i < response.getReservations().getReservationInfoList().size(); i++) {
            reservations.add(mapReservation(response.getReservations().getReservationInfoList().get(i)));
        }

        return reservations;
    }

    private Reservation mapReservation(ReservationInfo reservationInfo) {
        Reservation reservation = new Reservation();

        reservation.setGuests(createGuestList(reservationInfo));

        // Basic hotel info
        reservation.setHotelName(reservationInfo.getHotelInfo().getName());
        reservation.setHotelPhoneNumber(reservationInfo.getHotelInfo().getPhoneNumber());
        if (!TextUtils.isEmpty(reservationInfo.getHotelInfo().getRatingHotelDe())) {
            reservation.setStarsRating(Float.valueOf(reservationInfo.getHotelInfo().getRatingHotelDe()));
        }

        Address hotelAddress = new Address();
        de.hotel.remoteaccess.v28.model.Address hotelAddressV28 = reservationInfo.getHotelInfo().getHotelAddress();
        hotelAddress.setStreet(hotelAddressV28.getStreet());
        hotelAddress.setPostalCode(hotelAddressV28.getPostalCode());
        hotelAddress.setCity(hotelAddressV28.getCity());
        hotelAddress.setCountryIsoA3(hotelAddressV28.getCountryISOa3());
        reservation.setHotelAddress(hotelAddress);

        Calendar arrival = Calendar.getInstance();
        Dates.setMidnight(arrival);
        arrival.set(reservationInfo.getArrival().getYear(), reservationInfo.getArrival().getMonth() - 1, reservationInfo.getArrival().getDay());
        reservation.setArrival(arrival.getTime());

        Calendar departure = Calendar.getInstance();
        Dates.setMidnight(departure);
        departure.set(reservationInfo.getDeparture().getYear(), reservationInfo.getDeparture().getMonth() - 1, reservationInfo.getDeparture().getDay());
        reservation.setDeparture(departure.getTime());

        reservation.setReservationNr(reservationInfo.getReservationNr());
        reservation.setReservationId(reservationInfo.getReservationID());
        reservation.setMayTechnicallyBeCancelled(reservationInfo.isMayTechnicallyBeCancelled());

        switch (reservationInfo.getReservationStatus()) {
            case CANCEL_OK:
                reservation.setReservationStatus(ReservationStatus.CANCELLED);
                break;
            case RESERVATION_OK:
                reservation.setReservationStatus(ReservationStatus.RESERVED);
                break;
            default:
                reservation.setReservationStatus(ReservationStatus.OTHER);
                break;
        }

        // The cancellation code is just made of the first 8 chars of the reservation ID
        reservation.setCancellationCode(reservationInfo.getReservationID().substring(0, 8));


        List<ReservationRate> reservationRates = new ArrayList<>();

        for (int i = 0; i < reservationInfo.getRateDetailList().getReservationSingleRateList().size(); i++) {
            ReservationSingleRate singleRate = reservationInfo.getRateDetailList().getReservationSingleRateList().get(i);
            ReservationRate rate = new ReservationRate();

            if (singleRate.getRoomDescriptionList() != null
                    && singleRate.getRoomDescriptionList().getDescriptionList() != null
                    && !singleRate.getRoomDescriptionList().getDescriptionList().isEmpty()) {

                rate.setRoomDescription(singleRate.getRoomDescriptionList().getDescriptionList().get(0).getString());
            }

            rate.setRoomCount(singleRate.getNumberOfRooms());

            switch (singleRate.getMaxNumberOfPersonsPerRoom()) {
                case 1:
                    rate.setRoomtype(RoomType.SINGLE);
                    break;
                case 2:
                    rate.setRoomtype(RoomType.DOUBLE);
                    break;
                case 3:
                    rate.setRoomtype(RoomType.THREE_BED);
                    break;
                case 4:
                    rate.setRoomtype(RoomType.FOUR_BED);
                    break;
                case 5:
                    rate.setRoomtype(RoomType.FAMILY);
                    break;
                default:
                    rate.setRoomtype(RoomType.UNKNOWN);
                    break;
            }

            reservationRates.add(rate);
        }

        reservation.setRates(reservationRates);

        ReservationSingleRate singleRate = reservationInfo.getRateDetailList().getReservationSingleRateList().get(0);

        if (reservationInfo.getTotalPrice() != null) {
            reservation.setTotalPrice(reservationInfo.getTotalPrice());
        } else {
            reservation.setTotalPrice(singleRate.getPriceTotal().getAmountAfterTax());
        }

        reservation.setCurrency(singleRate.getPriceTotal().getCurrency());


        switch (singleRate.getRateCancellable()) {
            case WITHOUT_CHARGE_UNTIL_DATE:
                reservation.setCancellationType(CancellationType.WITHOUT_CHARGE_UNTIL_DATE);
                break;
            case WITH_CHARGE:
                reservation.setCancellationType(CancellationType.WITHOUT_CHARGE_UNTIL_DATE);
                break;
            case NON_CANCELLABLE_OR_WITH_CHARGE:
                reservation.setCancellationType(CancellationType.NON_CANCELLABLE_OR_WITH_CHARGE);
                break;
            case NON_CANCELLABLE:
                reservation.setCancellationType(CancellationType.NON_CANCELLABLE);
                break;
            case UNKNOWN:
                reservation.setCancellationType(CancellationType.UNKNOWN);
                break;
        }

        if (reservationInfo.getOriginalCancellationPolicy() != null
                && reservationInfo.getOriginalCancellationPolicy().getDescriptions() != null
                && !reservationInfo.getOriginalCancellationPolicy().getDescriptions().getDescriptionList().isEmpty()) {
            reservation.setCancellationPolicy(
                    reservationInfo
                            .getOriginalCancellationPolicy()
                            .getDescriptions()
                            .getDescriptionList()
                            .get(0)
                            .getString()
                            .replaceAll("\\s+", " ")
                            .trim());
        }

        return reservation;
    }

    private List<HdeGuest> createGuestList(ReservationInfo reservationInfo) {
        List<HdeGuest> hdeGuests = new ArrayList<>();

        ArrayOfGuest guestArray = reservationInfo.getGuests();
        if (guestArray != null) {
            List<Guest> guestList = guestArray.getGuestList();
            if (guestList != null) {
                for (Guest guest : guestList) {
                    hdeGuests.add(new HdeGuest(guest.getFirstName(), guest.getLastName(), guest.getEMail()));
                }
            }
        }
        return hdeGuests;
    }
}
