package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

/**
 * ValuationDetailsCategor< specifies the Guest Types which
 * did the rating
 */

@IntDef({HdeGuestType.UNKNOWWN,
        HdeGuestType.ALL_GUESTS,
        HdeGuestType.BUISNESS_GUESTS,
        HdeGuestType.GUESTS_TRAVELLING_ALONE,
        HdeGuestType.GUESTS_TRAVELLING_IN_GROUPS,
        HdeGuestType.GUESTS_TRAVELLING_WITH_CHILDREN,
        HdeGuestType.GUESTS_AGE_BETWEEN_31_50,
        HdeGuestType.GUESTS_AGE_OVER_50,
        HdeGuestType.GUESTS_AGE_UNDER_31})
public @interface HdeGuestType {
    int UNKNOWWN = -1;
    int ALL_GUESTS = 0;
    int BUISNESS_GUESTS = 1;
    int GUESTS_TRAVELLING_ALONE = 2;
    int GUESTS_TRAVELLING_IN_GROUPS = 3;
    int GUESTS_TRAVELLING_WITH_CHILDREN = 4;
    int GUESTS_AGE_BETWEEN_31_50 = 5;
    int GUESTS_AGE_OVER_50 = 6;
    int GUESTS_AGE_UNDER_31 = 7;
}
