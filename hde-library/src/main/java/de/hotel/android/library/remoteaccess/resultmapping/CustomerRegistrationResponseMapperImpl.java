package de.hotel.android.library.remoteaccess.resultmapping;

import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.response.RegistrationResponse;

public class CustomerRegistrationResponseMapperImpl {

    public @RegistrationResultCode int mapCustomerRegistrationResponse(RegistrationResponse response) {
        if (response == null || response.getResult() == null) {
            return RegistrationResultCode.UNKNOWN;
        }

        if (response.getResult().getErrorCodes() != null && !response.getResult().getErrorCodes().isEmpty()) {
            switch (response.getResult().getErrorCodes().get(0)) {
                case RegistrationResponse.ERROR_CODE_CUSTOMER_ALREADY_EXISTS:
                    return RegistrationResultCode.CUSTOMER_ERROR_EMAIL_EXISTS;
                default:
                    return RegistrationResultCode.UNKNOWN;
            }
        }

        // It is possible that all properties in response.getResult() are null, even the error codes.
        if (response.getResult().getCustomerNumber() == null) {
            return RegistrationResultCode.UNKNOWN;
        }

        return RegistrationResultCode.SUCCESS;
    }
}
