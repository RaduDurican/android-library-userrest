package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import de.hotel.remoteaccess.v28.model.CustomerDataRequest;
import de.hotel.remoteaccess.v28.model.GetCustomerData;

public class HdeV28GetCustomerDataRequestMapper {

    private HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28GetCustomerDataRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public GetCustomerData createGetCustomerDataRequest(@NonNull CustomerDataQuery query) {

        GetCustomerData getCustomerData = new GetCustomerData();
        CustomerDataRequest customerDataRequest = new CustomerDataRequest();
        customerDataRequest.setEmail(query.getEmailAddress().trim());
        customerDataRequest.setPassword(query.getPassword().trim());

        baseRequestMapper.setStandardProperties(customerDataRequest, query.getLanguage().getIso2Language());

        getCustomerData.setObjRequest(customerDataRequest);

        return getCustomerData;
    }
}
