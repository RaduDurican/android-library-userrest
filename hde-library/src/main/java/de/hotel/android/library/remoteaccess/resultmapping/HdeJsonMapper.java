package de.hotel.android.library.remoteaccess.resultmapping;

import android.support.annotation.NonNull;

import java.io.InputStream;

public interface HdeJsonMapper {
    <T> T mapFromJson(@NonNull InputStream inputStream, Class<T> responseType);
}
