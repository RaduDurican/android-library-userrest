package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;
import de.hotel.android.library.domain.model.HotelPropertyReviews;

public interface PropertyReviewsAdapter {
    HotelPropertyReviews fetchPropertyReviews(PropertyReviewsQuery query);
}
