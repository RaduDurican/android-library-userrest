package de.hotel.android.library.domain.service;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;

/**
 * Domain Service handling requests for making, modifying and cancelling
 * reservations.
 *
 * @author Johannes Mueller
 */
public interface ReservationFacade {
    HotelReservationResponse bookHotel(@NonNull HotelReservationQuery hotelReservationRequest);
}
