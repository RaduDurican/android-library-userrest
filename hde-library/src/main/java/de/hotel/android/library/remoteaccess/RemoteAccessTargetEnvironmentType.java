package de.hotel.android.library.remoteaccess;

import android.support.annotation.IntDef;

/**
 * Generic target environment type. Used to determine which backend should be used for requests.
 *
 * Note that different services (such as LAC, HSWB 2.8 or HSBW 3.0) provide different options.
 * The libraries AccessFactory class will do the right thing (tm) in each case.
 *
 * @see AccessFactory
 */
@IntDef({RemoteAccessTargetEnvironmentType.BETA, RemoteAccessTargetEnvironmentType.PRODUCTION, RemoteAccessTargetEnvironmentType.CUSTOM})
public @interface RemoteAccessTargetEnvironmentType {
    int BETA = 1;
    int PRODUCTION = 2;
    int CUSTOM = 3;
}
