package de.hotel.android.library.remoteaccess.cola;


import java.io.IOException;
import java.net.URLConnection;

import de.hotel.android.library.remoteaccess.soap.ConnectionBuilder;

public class WebFormRequestBuilderImpl implements WebFormRequestBuilder {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_APPLICATION_WWW_FORM = "application/x-www-form-urlencoded";

    private ConnectionBuilder connectionBuilder;

    public WebFormRequestBuilderImpl(ConnectionBuilder connectionBuilder) {
        this.connectionBuilder = connectionBuilder;
    }

    @Override
    public URLConnection createWebFormUrlConnection(int requestBodyByteCount) throws IOException {
        URLConnection urlConnection = connectionBuilder.createUrlConnection(requestBodyByteCount, null);
        urlConnection.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_APPLICATION_WWW_FORM);

        return urlConnection;
    }
}
