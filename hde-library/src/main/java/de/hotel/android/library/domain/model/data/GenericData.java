package de.hotel.android.library.domain.model.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

/**
 * @author Johannes Mueller
 */
public class GenericData {

    private String key;
    private List<String> valueList;

    @NonNull
    public String getKey() {
        return key;
    }

    public void setKey(@NonNull String key) {
        this.key = key;
    }

    @Nullable
    public List<String> getValueList() {
        return valueList;
    }

    public void setValueList(@Nullable List<String> valueList) {
        this.valueList = valueList;
    }
}
