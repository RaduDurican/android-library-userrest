package de.hotel.android.library.remoteaccess.resultmapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ErrorType;
import de.hotel.android.library.util.ObjectDumper;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class HdeHotelAvailErrorMapperImpl implements HdeHotelAvailErrorMapper {
    private static final String ERROR_CODE_NO_AVAILABILITY = "322";

    @Override
    public HotelAvailResult handleHotelAvailErrors(@NonNull HotelAvailQuery query, @NonNull List<ErrorType> errors) {
        if (errors.isEmpty()) {
            throw new RuntimeException("handleHotelAvailErrors called without errors");
        }

        for (ErrorType error : errors) {
            if (error.getErrorWarningAttributeGroup().getCode().equals(ERROR_CODE_NO_AVAILABILITY)) {
                return createHotelAvailResultNoAvailability(query);
            }
        }

        // TODO throw specific exceptions for the various possible errors
        String errorDump = ObjectDumper.dump(errors);
        throw new RuntimeException(errorDump);
    }

    private HotelAvailResult createHotelAvailResultNoAvailability(HotelAvailQuery query) {
        HotelAvailResult hotelAvailResult = new HotelAvailResult();
        hotelAvailResult.setFromDate(query.getHotelAvailCriterion().getFrom());
        hotelAvailResult.setToDate(query.getHotelAvailCriterion().getTo());
        hotelAvailResult.setTimestamp(new Date(System.currentTimeMillis()));
        return hotelAvailResult;
    }
}
