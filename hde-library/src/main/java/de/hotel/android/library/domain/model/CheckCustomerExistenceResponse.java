package de.hotel.android.library.domain.model;

public class CheckCustomerExistenceResponse {
    private boolean customerExists;
    private Integer customerNumber;

    public boolean customerExists() {
        return customerExists;
    }

    public void setCustomerExists(boolean customerExists) {
        this.customerExists = customerExists;
    }

    public Integer getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }
}
