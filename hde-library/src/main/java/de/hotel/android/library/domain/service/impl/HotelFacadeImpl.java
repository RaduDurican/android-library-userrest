package de.hotel.android.library.domain.service.impl;

import android.support.annotation.NonNull;

import java.util.Date;

import de.hotel.android.library.domain.adapter.HotelAdapter;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;
import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;
import de.hotel.android.library.domain.service.HotelFacade;
import de.hotel.android.library.remoteaccess.adapter.HdePropertyDescriptionAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.PropertyDescriptionAdapter;
import de.hotel.android.library.remoteaccess.adapter.PropertyReviewsAdapter;
import de.hotel.android.library.util.Lists;
import timber.log.Timber;

public class HotelFacadeImpl implements HotelFacade {

    public static String TAG = HotelFacadeImpl.class.getName();
    
    private final HotelAdapter hotelAdapter;
    private final PropertyDescriptionAdapter hdePropertyDescriptionAdapter;
    private final PropertyReviewsAdapter hdePropertyReviewsAdapter;

    private static final CatchAllExceptionHandler catchAllExceptionHandler = new CatchAllExceptionHandler();

    public HotelFacadeImpl(@NonNull HotelAdapter hotelAdapter,
                           @NonNull PropertyDescriptionAdapter hdePropertyDescriptionAdapter,
                           @NonNull PropertyReviewsAdapter hdePropertyReviewsAdapter) {
        this.hotelAdapter = hotelAdapter;
        this.hdePropertyDescriptionAdapter = hdePropertyDescriptionAdapter;
        this.hdePropertyReviewsAdapter = hdePropertyReviewsAdapter;
    }

    @Override
    public HotelAvailResult searchAvailableHotels(@NonNull HotelAvailQuery hotelAvailQuery) {
        return hotelAdapter.searchAvailableHotels(hotelAvailQuery);
    }

    @Override
    public Hotel fetchHotel(@NonNull PropertyDescriptionQuery query) {
        return hdePropertyDescriptionAdapter.fetchHotelDescription(query);
    }

    @Override
    public HotelPropertyReviews fetchHotelReviews(@NonNull PropertyReviewsQuery query) {
        return hdePropertyReviewsAdapter.fetchPropertyReviews(query);
    }

    @Override
    public HotelAvailResult fetchHotelDetailInformation(@NonNull final HotelAvailQuery hotelAvailQuery) {
        final HotelAvailResult[] hotelAvailResult = new HotelAvailResult[1];
        Thread availThread = new Thread(new Runnable() {
            @Override
            public void run() {
                hotelAvailResult[0] = searchAvailableHotels(hotelAvailQuery);
            }
        });

        availThread.setUncaughtExceptionHandler(catchAllExceptionHandler);
        availThread.start();

        final PropertyDescriptionQuery propertyDescriptionQuery = new PropertyDescriptionQuery();
        propertyDescriptionQuery.setHotelId(hotelAvailQuery.getHotelRateCriterion().getHotelId());
        propertyDescriptionQuery.setLanguage(hotelAvailQuery.getLocale().getLanguage());
        if (hotelAvailQuery.getCompanyNumber() != null) {
            propertyDescriptionQuery.setCompanyNumber(hotelAvailQuery.getCompanyNumber());
        }

        final Hotel[] hotels = new Hotel[1];

        Thread descriptionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                hotels[0] = fetchHotel(propertyDescriptionQuery);
            }
        });

        descriptionThread.setUncaughtExceptionHandler(catchAllExceptionHandler);
        descriptionThread.start();

        final PropertyReviewsQuery propertyReviewsQuery = new PropertyReviewsQuery();
        propertyReviewsQuery.setHotelId(hotelAvailQuery.getHotelRateCriterion().getHotelId());
        propertyReviewsQuery.setLanguage(hotelAvailQuery.getLocale().getLanguage());

        final HotelPropertyReviews[] hotelPropertyReviews = new HotelPropertyReviews[1];
        Thread reviewsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                hotelPropertyReviews[0] = fetchHotelReviews(propertyReviewsQuery);
            }
        });

        reviewsThread.setUncaughtExceptionHandler(catchAllExceptionHandler);
        reviewsThread.start();

        try {
            availThread.join();
            descriptionThread.join();
            reviewsThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        HotelAvailResult availResult;
        if (hotelAvailResult[0] == null) {
            availResult = new HotelAvailResult();
            availResult.setFromDate(hotelAvailQuery.getHotelAvailCriterion().getFrom());
            availResult.setToDate(hotelAvailQuery.getHotelAvailCriterion().getTo());
            availResult.setTimestamp(new Date(System.currentTimeMillis()));
        } else {
            availResult = hotelAvailResult[0];
        }

        Hotel hotel;
        // Get the hotel from the SingleAvail
        if (!Lists.isNullOrEmpty(availResult.getHotelList())) {
            hotel = availResult.getHotelList().get(0);
            // When the hotel comes from the SingleAvail, we need the properties from the v28 PropertyDescription
            if (hotels[0] != null) {
                hotel.setHotelProperties(hotels[0].getHotelProperties());
            }
        } else {
            // When the SingleAvail fails (e.g. no availability), we take the hotel from the v28 PropertyDescription
            // Thus we don't need to set the properties explicitly.
            hotel = hotels[0];
            availResult.getHotelList().add(hotel);
            availResult.setTotalAvailableHotels(1);
        }

        hotel.setHotelPropertyReviews(hotelPropertyReviews[0]);

        return availResult;
    }

    private static class CatchAllExceptionHandler implements Thread.UncaughtExceptionHandler {

        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            Timber.e(throwable, "Exception in %s", TAG);
            // Do nothing
            // Background: When we don't set this as UncaughtExceptionHandlers the whole app crashes
            // despite this method is being called in an AsyncTask.
        }
    }
}
