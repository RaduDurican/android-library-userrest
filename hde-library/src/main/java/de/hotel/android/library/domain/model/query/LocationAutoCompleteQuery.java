package de.hotel.android.library.domain.model.query;

import android.support.annotation.NonNull;

public class LocationAutoCompleteQuery {
    private final String searchPhrase;
    private final String isoA2LanguageCode;

    public LocationAutoCompleteQuery(@NonNull String searchPhrase, @NonNull String isoA2LanguageCode) {
        this.searchPhrase = searchPhrase;
        this.isoA2LanguageCode = isoA2LanguageCode;
    }

    public String getSearchPhrase() {
        return searchPhrase;
    }

    public String getIsoA2LanguageCode() {
        return isoA2LanguageCode;
    }
}
