package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({SortOrder.RECOMMENDED, SortOrder.PRICE_ASCENDING, SortOrder.DISTANCE_DESTINATION, SortOrder.STAR_RATING_DESCENDING, SortOrder.USER_RATING_DESCENDING, SortOrder.PRICE_DESCENDING})
public @interface SortOrder {
    int RECOMMENDED = 0;
    int PRICE_ASCENDING = 1;
    int DISTANCE_DESTINATION = 2;
    int STAR_RATING_DESCENDING = 3;
    int USER_RATING_DESCENDING = 4;
    int PRICE_DESCENDING = 5;
}
