package de.hotel.android.library.domain.model.query;

import de.hotel.android.library.domain.model.data.Language;

public class PropertyReviewsQuery {
    private Language language;
    private String hotelId;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }
}
