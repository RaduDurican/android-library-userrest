package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({LocationType.UNKNOWN, LocationType.CITY, LocationType.DISTRICT, LocationType.REGION, LocationType.POI, LocationType.FAIR, LocationType.COMPANY_LOCATION, LocationType.AIRPORT,
        LocationType.HIGHWAY, LocationType.TRAIN_STATION, LocationType.PUBLIC_TRANSPORT, LocationType.IATA, LocationType.HOTEL})
public @interface LocationType {

    int UNKNOWN = 0;
    int CITY = 1;
    int DISTRICT = 2;
    int REGION = 3;
    int POI = 4;
    int FAIR = 5;
    int COMPANY_LOCATION = 6;
    int AIRPORT = 7;
    int HIGHWAY = 8;
    int TRAIN_STATION = 9;
    int PUBLIC_TRANSPORT = 10;
    int IATA = 11;
    int HOTEL = 12;
}
