package de.hotel.android.library.remoteaccess.http;

import java.io.IOException;
import java.io.InputStream;

public interface HdeLocationAutoCompleteHttpClient {
    InputStream performRequest(String urlParameter) throws IOException;
}
