package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.net.URLConnection;

public class ConnectionBuilderV28 implements ConnectionBuilder {

    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_APPLICATION_SOAP_XML = "text/xml;charset=UTF-8;";
    private static final String SOAP_ACTION = "SOAPAction";

    private final ConnectionBuilder connectionBuilder;

    public ConnectionBuilderV28(@NonNull ConnectionBuilder connectionBuilder) {
        this.connectionBuilder = connectionBuilder;
    }

    @Override
    public URLConnection createUrlConnection(int requestBodyByteCount, String soapAction) throws IOException {
        URLConnection urlConnection = connectionBuilder.createUrlConnection(requestBodyByteCount, soapAction);
        urlConnection.setRequestProperty(CONTENT_TYPE, CONTENT_TYPE_APPLICATION_SOAP_XML);
        urlConnection.setRequestProperty(SOAP_ACTION, soapAction);

        return urlConnection;
    }
}
