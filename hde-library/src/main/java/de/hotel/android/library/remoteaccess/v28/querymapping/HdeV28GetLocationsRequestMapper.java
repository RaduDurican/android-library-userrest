package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import de.hotel.android.library.domain.model.data.Language;
import de.hotel.remoteaccess.v28.model.ArrayOfInt;
import de.hotel.remoteaccess.v28.model.GetLocations;
import de.hotel.remoteaccess.v28.model.GetLocationsRequest;

public class HdeV28GetLocationsRequestMapper {
    private HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28GetLocationsRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public GetLocations createGetLocationsRequest(int locationId, Language language) {
        GetLocationsRequest getLocationsRequest = new GetLocationsRequest();
        ArrayOfInt locationNumbers = new ArrayOfInt();
        ArrayList<Integer> intList = new ArrayList<>();
        intList.add(locationId);
        locationNumbers.setIntList(intList);
        getLocationsRequest.setLocationNumbers(locationNumbers);

        baseRequestMapper.setStandardProperties(getLocationsRequest, language.getIso2Language());

        GetLocations getLocations = new GetLocations();
        getLocations.setObjParamRequest(getLocationsRequest);

        return getLocations;
    }
}
