package de.hotel.android.library.remoteaccess.soap;

/**
 * Service for performing XML requests against HSBW
 */
public interface SoapService {
    <T, S> S performRequest(T request, Class<S> type, String soapAction, boolean retryOnFailure);
}
