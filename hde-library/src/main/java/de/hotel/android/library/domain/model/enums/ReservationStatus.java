package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({ReservationStatus.OTHER, ReservationStatus.RESERVED, ReservationStatus.CANCELLED})
public @interface ReservationStatus {
    int OTHER = 0;
    int RESERVED = 1;
    int CANCELLED = 2;
}
