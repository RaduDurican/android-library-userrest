package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({AdditionalDetailsType.NONE, AdditionalDetailsType.BREAKFAST,
        AdditionalDetailsType.AMENITIES, AdditionalDetailsType.RECEPTION})

public @interface AdditionalDetailsType {
    int NONE = -1;
    int BREAKFAST = 0;
    int AMENITIES = 1;
    int RECEPTION = 2;
}
