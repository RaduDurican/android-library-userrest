package de.hotel.android.library.remoteaccess.v30;

/**
 * @author Johannes Mueller
 */
public final class OtaConstants {

    private OtaConstants() {
        // Don't instantiate me.
    }

    public static final String OTA_AGE_QUALIFYING_CODE_ADULT = "10";
    public static final String OTA_REQUESTOR_TYPE_INTERNET_BROKER = "13";
    public static final String OTA_UNIT_OF_MEASURE_CODE_KM = "2";

    // OTA Code Type 'FIT'
    public static final String OTA_FIT_CITY_TAX = "3";
    public static final String OTA_FIT_SERVICE_CHARGE = "14";
    public static final String OTA_FIT_MISCELLANEOUS = "27";
    public static final String OTA_FIT_VAT = "36";
    public static final String OTA_FIT_UNKNOWN = "41";

    // Unique Id Type
    public static final String OTA_UNIQUE_ID_TYPE_HOTEL = "10";

    // Phone Technology Type
    public static final String PHONE_TECH_TYPE_VOICE = "1";
    public static final String PHONE_TECH_TYPE_MOBILE = "5";

    // Profile Type
    public static final String PROFILE_TYPE_CUSTOMER = "1";
}
