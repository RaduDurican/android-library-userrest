package de.hotel.android.library.remoteaccess.resultmapping;


import android.support.annotation.NonNull;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;


public class HdeJsonMapperImpl implements HdeJsonMapper {

    private static final String UTF_8 = "UTF-8";

    @Override
    public <T> T mapFromJson(@NonNull InputStream inputStream, Class<T> responseType) {
        return new Gson().fromJson(new InputStreamReader(inputStream, Charset.forName(UTF_8)), responseType);
    }
}