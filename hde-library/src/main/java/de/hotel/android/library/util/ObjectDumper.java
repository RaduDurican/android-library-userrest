package de.hotel.android.library.util;

import android.util.Log;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Utility class for dumping objects.
 *
 * @author mlehmann@edv-medien.de
 */
@SuppressWarnings({"SameParameterValue", "RedundantThrows"})
public final class ObjectDumper {
    private static final String TAG = ObjectDumper.class.getSimpleName();
    private static final Map<Class<?>, ObjectDumpFormatter> CLASS_OBJECT_DUMP_FORMATTER_MAP = new ConcurrentHashMap<>();

    private ObjectDumper() { /* by design */
    }

    // CS.SUPPRESS JavadocType, Reason: Nomen est omen.
    private static class BigIntegerDumpFormatter implements ObjectDumpFormatter {
        public String objectDumpFormat(Object object) {
            if (object instanceof BigInteger) {
                return Integer.toString(((BigInteger) object).intValue());
            }

            return "null";
        }
    }

    // CS.SUPPRESS JavadocType, Reason: Nomen est omen.
    private static class BigDecimalDumpFormatter implements ObjectDumpFormatter {
        public String objectDumpFormat(Object object) {
            if (object instanceof BigDecimal) {
                return Double.toString(((BigDecimal) object).doubleValue());
            }

            return "null";
        }
    }

    // CS.SUPPRESS JavadocType, Reason: Nomen est omen.
    private static class DateDumpFormatter implements ObjectDumpFormatter {
        public String objectDumpFormat(Object object) {
            if (object instanceof Date) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);

                return simpleDateFormat.format((Date) object);
            }

            return "null";
        }
    }

    // CS.SUPPRESS JavadocType, Reason: Nomen est omen.
    private static class CalendarDumpFormatter implements ObjectDumpFormatter {
        public String objectDumpFormat(Object object) {
            if (object instanceof XMLGregorianCalendar) {
                object = ((XMLGregorianCalendar) object).toGregorianCalendar();
            }

            if (object instanceof Calendar) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);

                return simpleDateFormat.format(((Calendar) object).getTime());
            }

            return "null";
        }
    }

    // CS.SUPPRESS JavadocType, Reason: Nomen est omen.
    private static class UUIDDumpFormatter implements ObjectDumpFormatter {
        public String objectDumpFormat(Object object) {
            if (object instanceof UUID) {

                return ((UUID) object).toString();
            }

            return "null";
        }
    }

    static {
        registerObjectDumpFormatter(BigInteger.class, new BigIntegerDumpFormatter());
        registerObjectDumpFormatter(BigDecimal.class, new BigDecimalDumpFormatter());
        registerObjectDumpFormatter(Date.class, new DateDumpFormatter());
        registerObjectDumpFormatter(Calendar.class, new CalendarDumpFormatter());
        registerObjectDumpFormatter(UUID.class, new UUIDDumpFormatter());
        registerObjectDumpFormatter(XMLGregorianCalendar.class, new CalendarDumpFormatter());
    }

    public static void registerObjectDumpFormatter(Class<?> objectClass, ObjectDumpFormatter objectDumpFormatter) {
        CLASS_OBJECT_DUMP_FORMATTER_MAP.put(objectClass, objectDumpFormatter);
    }

    public static void unregisterObjectDumpFormatter(Class<?> objectClass) {
        CLASS_OBJECT_DUMP_FORMATTER_MAP.remove(objectClass);
    }

    public static String dump(Object object) {
        return dump(object, true, true, true, false, false);
    }

    public static String dump(Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields, boolean quoteStrings) {
        StringBuilder stringBuilder = new StringBuilder();

        try {
            Set<Object> instanceCircleDetectionSet = new HashSet<>();
            List<DumpLine> dumpLineList = dumpRecursive("DUMP", object, dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet, 0);

            int maxFieldNameLength = 0;
            for (DumpLine dumpLine : dumpLineList) {
                maxFieldNameLength = dumpLine.fieldName.length() > maxFieldNameLength && dumpLine.hasFieldValue ? dumpLine.fieldName.length() : maxFieldNameLength;
            }

            for (DumpLine dumpLine : dumpLineList) {
                if (dumpLine.hasFieldValue) {
                    stringBuilder.append(dumpLine.fieldName + indentationString(maxFieldNameLength - dumpLine.fieldName.length()) + ": " + dumpLine.fieldValue + "\n");
                } else {
                    stringBuilder.append(dumpLine.fieldName + "\n");
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }

        return stringBuilder.toString();
    }

    /**
     * This class holds information for a dump line.
     */
    private static class DumpLine {
        private String fieldName = null;
        private String fieldValue = null;
        private boolean hasFieldValue = false;

        public DumpLine(String fieldName) {
            this.fieldName = fieldName;
        }

        public DumpLine(String fieldName, String fieldValue) {
            this.fieldName = fieldName;
            this.fieldValue = fieldValue;
            this.hasFieldValue = true;
        }
    }

    // CS.SUPPRESS CyclomaticComplexity, Reason: Clearly structured.
    private static boolean isSimpleType(Object object) {
        // CS.SUPPRESS BooleanExpressionComplexity, Reason: Long, but simple.
        if (object == null || object.getClass().isPrimitive() || object.getClass().isEnum() || object instanceof Boolean || object instanceof Character || object instanceof Byte
                || object instanceof Short || object instanceof Integer || object instanceof Long || object instanceof Float || object instanceof Double || object instanceof String) {
            return true;
        }

        for (Class<?> objectClass : CLASS_OBJECT_DUMP_FORMATTER_MAP.keySet()) {
            if (objectClass.isInstance(object)) {
                return true;
            }
        }

        return false;
    }

    private static boolean isArray(Object object) {
        if (object == null) {
            return false;
        }

        return object.getClass().isArray();
    }

    private static boolean isMap(Object object) {
        return object instanceof Map<?, ?>;
    }

    private static boolean isIterable(Object object) {
        return object instanceof Iterable<?>;
    }

    private static String indentationString(int indentationSpaces) {
        StringBuilder stringBuilder = new StringBuilder(indentationSpaces);

        for (int i = 0; i < indentationSpaces; i++) {
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }

    private static List<Field> getFields(Class<?> objectClass) {
        List<Field> fieldList = new ArrayList<>();

        if (objectClass.getSuperclass() != Object.class && objectClass.getSuperclass() != null) {
            fieldList.addAll(getFields(objectClass.getSuperclass()));
        }

        if (objectClass != Object.class) {
            fieldList.addAll(Arrays.asList(objectClass.getDeclaredFields()));
        }

        return fieldList;
    }

    private static String format(Object object, boolean quoteStrings) {
        if (object == null) {
            return "null";
        }

        for (Class<?> objectClass : CLASS_OBJECT_DUMP_FORMATTER_MAP.keySet()) {
            if (objectClass.isInstance(object)) {
                return CLASS_OBJECT_DUMP_FORMATTER_MAP.get(objectClass).objectDumpFormat(object);
            }
        }

        if (quoteStrings && (object instanceof String)) {
            return "'" + object + "'";
        }

        return object.toString();
    }

    private static List<DumpLine> dumpSimpleType(String fieldName, Object object, boolean quoteStrings, int indentationLevel) {
        List<DumpLine> dumpLineList = new LinkedList<>();

        dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName, format(object, quoteStrings)));

        return dumpLineList;
    }

    private static List<DumpLine> dumpArray(String fieldName, Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields, boolean quoteStrings,
                                            Set<Object> instanceCircleDetectionSet, int indentationLevel) {
        List<DumpLine> dumpLineList = new LinkedList<>();

        dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName + " [" + object.getClass().getCanonicalName() + "]"));

        for (int i = 0; i < Array.getLength(object); i++) {
            try {
                dumpLineList.addAll(dumpRecursive(fieldName + "[" + (i) + "]", Array.get(object, i), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                        instanceCircleDetectionSet, indentationLevel + 1));
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage(), e);
            }
        }

        return dumpLineList;
    }

    private static List<DumpLine> dumpMap(String fieldName, Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields, boolean quoteStrings,
                                          Set<Object> instanceCircleDetectionSet, int indentationLevel) {
        List<DumpLine> dumpLineList = new LinkedList<>();

        dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName + " [" + object.getClass().getCanonicalName() + "]"));

        Map<?, ?> map = (Map<?, ?>) object;
        int i = 0;
        for (Map.Entry<?, ?> mapEntry : map.entrySet()) {
            try {
                dumpLineList.addAll(dumpRecursive(fieldName + ".key  [" + i + "]", mapEntry.getKey(), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                        instanceCircleDetectionSet, indentationLevel + 1));
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage(), e);
            }
            try {
                dumpLineList.addAll(dumpRecursive(fieldName + ".value[" + i + "]", mapEntry.getValue(), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                        instanceCircleDetectionSet, indentationLevel + 1));
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage(), e);
            }
            i++;
        }

        return dumpLineList;
    }

    private static List<DumpLine> dumpIterable(String fieldName, Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields, boolean quoteStrings,
                                               Set<Object> instanceCircleDetectionSet, int indentationLevel) {
        List<DumpLine> dumpLineList = new LinkedList<>();

        dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName + " [" + object.getClass().getCanonicalName() + "]"));

        Iterator<?> iterator = ((Iterable<?>) object).iterator();
        int i = 0;
        while (iterator.hasNext()) {
            try {
                dumpLineList.addAll(dumpRecursive(fieldName + "[" + (i++) + "]", iterator.next(), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                        instanceCircleDetectionSet, indentationLevel + 1));
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage(), e);
            }
        }

        return dumpLineList;
    }

    private static List<DumpLine> dumpRecursive(String fieldName, Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields,
                                                boolean quoteStrings, Set<Object> instanceCircleDetectionSet, int indentationLevel) throws Exception {
        List<DumpLine> dumpLineList = new LinkedList<>();

        try {
            if (isSimpleType(object)) {
                return dumpSimpleType(fieldName, object, quoteStrings, indentationLevel);
            } else if (isArray(object)) {
                return dumpArray(fieldName, object, dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet, indentationLevel);
            } else if (isMap(object)) {
                return dumpMap(fieldName, object, dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet, indentationLevel);
            } else if (isIterable(object)) {
                return dumpIterable(fieldName, object, dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet, indentationLevel);
            }

            if (instanceCircleDetectionSet.contains(object)) {
                dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName + " " + "[" + object.getClass().getCanonicalName() + "] " + "<CIRCULAR/RECURSIVE INSTANCE>"));
                return dumpLineList;
            } else {
                dumpLineList.add(new DumpLine(indentationString(indentationLevel * 2) + fieldName + " " + "[" + object.getClass().getCanonicalName() + "]"));
                instanceCircleDetectionSet.add(object);
            }

            indentationLevel++;

            List<Field> fieldList = getFields(object.getClass());

            dumpFields(object, dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet, indentationLevel, dumpLineList, fieldList);

        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage(), e);
        }

        return dumpLineList;
    }

    // CS.SUPPRESS CyclomaticComplexity, Reason: Clearly structured.
    private static void dumpFields(Object object, boolean dumpRecursive, boolean dumpArrays, boolean dumpNonPublicFields, boolean dumpStaticFields, boolean quoteStrings,
                                   Set<Object> instanceCircleDetectionSet, int indentationLevel, List<DumpLine> dumpLineList, List<Field> fieldList) throws Exception {
        for (Field field : fieldList) {
            if ((dumpNonPublicFields || Modifier.isPublic(field.getModifiers())) && (dumpStaticFields || !Modifier.isStatic(field.getModifiers()))) {
                field.setAccessible(true);

                if (isSimpleType(field.get(object))) {
                    dumpLineList.addAll(dumpSimpleType(field.getName(), field.get(object), quoteStrings, indentationLevel));
                } else if (isArray(field.get(object))) {
                    if (dumpArrays) {
                        dumpLineList.addAll(dumpArray(field.getName(), field.get(object), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet,
                                indentationLevel));
                    }
                } else if (isMap(field.get(object))) {
                    if (dumpArrays) {
                        dumpLineList.addAll(dumpMap(field.getName(), field.get(object), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings, instanceCircleDetectionSet,
                                indentationLevel));
                    }
                } else if (isIterable(field.get(object))) {
                    if (dumpArrays) {
                        dumpLineList.addAll(dumpIterable(field.getName(), field.get(object), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                                instanceCircleDetectionSet, indentationLevel));
                    }
                } else {
                    if (dumpRecursive) {
                        dumpLineList.addAll(dumpRecursive(field.getName(), field.get(object), dumpRecursive, dumpArrays, dumpNonPublicFields, dumpStaticFields, quoteStrings,
                                instanceCircleDetectionSet, indentationLevel));
                    }
                }
            }
        }
    }
}
