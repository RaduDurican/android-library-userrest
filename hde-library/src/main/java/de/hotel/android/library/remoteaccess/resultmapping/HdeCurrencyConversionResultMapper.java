package de.hotel.android.library.remoteaccess.resultmapping;

import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;

import java.math.BigDecimal;

public interface HdeCurrencyConversionResultMapper {
    BigDecimal getCustomerCurrencyFactor(OTAHotelAvailRS.CurrencyConversions currencyConversions, String iso3CurrencyHotel, String iso3CurrencyCustomer);
}
