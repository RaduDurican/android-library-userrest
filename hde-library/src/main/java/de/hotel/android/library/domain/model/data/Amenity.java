package de.hotel.android.library.domain.model.data;

import de.hotel.android.library.domain.model.enums.AmenityType;

public class Amenity {
    private @AmenityType
    int amenityType;
    private String Description;

    public int getAmenityType() {
        return amenityType;
    }

    public void setAmenityType(int amenityType) {
        this.amenityType = amenityType;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
}
