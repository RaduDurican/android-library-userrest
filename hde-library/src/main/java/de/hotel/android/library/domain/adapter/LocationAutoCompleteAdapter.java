package de.hotel.android.library.domain.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;

import java.io.IOException;
import java.util.List;

public interface LocationAutoCompleteAdapter {
    List<Location> searchLocations(@NonNull LocationAutoCompleteQuery request) throws IOException;
}
