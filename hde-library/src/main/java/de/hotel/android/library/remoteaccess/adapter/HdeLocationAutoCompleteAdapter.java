package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.adapter.LocationAutoCompleteAdapter;
import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;
import de.hotel.android.library.remoteaccess.autocomplete.HdeLocationAutoCompleteMapper;
import de.hotel.android.library.remoteaccess.http.HdeLocationAutoCompleteHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class HdeLocationAutoCompleteAdapter implements LocationAutoCompleteAdapter {
    private final HdeLocationAutoCompleteHttpClient hdeLocationAutoCompleteHttpClient;
    private final HdeLocationAutoCompleteMapper hdeLocationAutoCompleteMapper;

    public HdeLocationAutoCompleteAdapter(@NonNull HdeLocationAutoCompleteHttpClient hdeLocationAutoCompleteHttpClient, @NonNull HdeLocationAutoCompleteMapper hdeLocationAutoCompleteMapper) {
        this.hdeLocationAutoCompleteHttpClient = hdeLocationAutoCompleteHttpClient;
        this.hdeLocationAutoCompleteMapper = hdeLocationAutoCompleteMapper;
    }

    @Override
    public List<Location> searchLocations(@NonNull LocationAutoCompleteQuery request) throws IOException {
        InputStream inputStream = hdeLocationAutoCompleteHttpClient.performRequest(hdeLocationAutoCompleteMapper.mapLocationAutoCompleteRequest(request));

        return hdeLocationAutoCompleteMapper.mapLocationAutoCompleteResponse(inputStream);
    }
}
