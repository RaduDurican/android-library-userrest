package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef(flag = true, value = {AmenityType.OTHER,
        AmenityType.EXECUTIVE_FLOOR,
        AmenityType.EXERCISE_GYM,
        AmenityType.FREE_AIRPORT_SHUTTLE,
        AmenityType.INDOOR_PARKING,
        AmenityType.INDOOR_POOL,
        AmenityType.OUTDOOR_POOL,
        AmenityType.POOL,
        AmenityType.RESTAURANT,
        AmenityType.SAUNA,
        AmenityType.PARKING_LOT,
        AmenityType.PETS_ALLOWED,
        AmenityType.AIR_CONDITIONING,
        AmenityType.BALCONY,
        AmenityType.CRIBS,
        AmenityType.INTERNET_ACCESS,
        AmenityType.NON_SMOKING,
        AmenityType.SAFE,
        AmenityType.WIFI,
        AmenityType.HANDICAP_ROOM})
public @interface AmenityType {
    int OTHER = 0;
    int EXECUTIVE_FLOOR = 1;
    int EXERCISE_GYM = 1 << 1;
    int FREE_AIRPORT_SHUTTLE = 1 << 2;
    int INDOOR_PARKING = 1 << 3;
    int INDOOR_POOL = 1 << 4;
    int OUTDOOR_POOL = 1 << 5;
    int POOL = 1 << 6;
    int RESTAURANT = 1 << 7;
    int SAUNA = 1 << 8;
    int PARKING_LOT = 1 << 9;
    int PETS_ALLOWED = 1 << 10;
    int AIR_CONDITIONING = 1 << 11;
    int BALCONY = 1 << 12;
    int CRIBS = 1 << 13;
    int INTERNET_ACCESS = 1 << 14;
    int NON_SMOKING = 1 << 15;
    int SAFE = 1 << 16;
    int WIFI = 1 << 17;
    int HANDICAP_ROOM = 1 << 18;
}
