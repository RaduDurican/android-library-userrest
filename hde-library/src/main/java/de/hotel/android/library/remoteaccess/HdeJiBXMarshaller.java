package de.hotel.android.library.remoteaccess;

import org.jibx.runtime.JiBXException;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public interface HdeJiBXMarshaller {
    <T> OutputStream marshalRequest(T request) throws JiBXException, UnsupportedEncodingException;
    <T> T unmarshalResponse(InputStream content, Class<T> type) throws JiBXException;
}
