package de.hotel.android.library.remoteaccess.v30.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.v30.OtaConstants;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DateTimeSpanGroup;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.DateTimeSpanType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAPayloadStdAttributes;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.POSType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.SourceType;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.UniqueIDGroup;
import de.hotel.android.library.util.Dates;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class HdeV30OtaMapperImpl implements HdeV30OtaMapper {

    private static final int HSBW_VERSION = 3;
    private final String username;
    private final int remoteAccessTargetEnvironmentType;

    public HdeV30OtaMapperImpl(@NonNull String username, @RemoteAccessTargetEnvironmentType int remoteAccessTargetEnvironmentType) {
        this.username = username;
        this.remoteAccessTargetEnvironmentType = remoteAccessTargetEnvironmentType;
    }

    @Override
    public DateTimeSpanType createDateTimeSpanType(Long from, Long to) {
        DateTimeSpanType stayDateRange = new DateTimeSpanType();
        DateTimeSpanGroup stayDateRangeTimeSpanGroup = new DateTimeSpanGroup();
        stayDateRangeTimeSpanGroup.setStart(Dates.yyyyMmDdFor(from));
        stayDateRangeTimeSpanGroup.setEnd(Dates.yyyyMmDdFor(to));
        stayDateRange.setDateTimeSpanGroup(stayDateRangeTimeSpanGroup);
        return stayDateRange;
    }

    @Override
    public POSType createPosType() {
        POSType posType = new POSType();

        ArrayList<SourceType> sourceTypes = new ArrayList<>();
        SourceType sourceType = new SourceType();
        SourceType.RequestorID requestorID = new SourceType.RequestorID();
        UniqueIDGroup uniqueIDGroup = new UniqueIDGroup();
        uniqueIDGroup.setID(username);
        uniqueIDGroup.setIDContext(HSBW_CONTEXT);
        uniqueIDGroup.setType(OtaConstants.OTA_REQUESTOR_TYPE_INTERNET_BROKER);

        requestorID.setUniqueIDGroup(uniqueIDGroup);
        sourceType.setRequestorID(requestorID);
        sourceTypes.add(sourceType);

        posType.setSourceList(sourceTypes);
        return posType;
    }

    @Override
    public OTAPayloadStdAttributes createOtaPayloadStdAttributes(Language language) {
        OTAPayloadStdAttributes otaPayloadStdAttributes = new OTAPayloadStdAttributes();
        otaPayloadStdAttributes.setVersion(new BigDecimal(HSBW_VERSION));
        otaPayloadStdAttributes.setEchoToken(UUID.randomUUID().toString());
        otaPayloadStdAttributes.setTimeStamp(new Date());
        otaPayloadStdAttributes.setTarget(
                remoteAccessTargetEnvironmentType == RemoteAccessTargetEnvironmentType.PRODUCTION
                        ? OTAPayloadStdAttributes.Target.PRODUCTION
                        : OTAPayloadStdAttributes.Target.TEST);
        otaPayloadStdAttributes.setPrimaryLangID(language.getIso2Language());
        return otaPayloadStdAttributes;
    }
}
