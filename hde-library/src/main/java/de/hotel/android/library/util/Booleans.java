package de.hotel.android.library.util;

/**
 * Utility class for {@link java.lang.Boolean}.
 *
 * @author Johannes Mueller
 */
public final class Booleans {

    private Booleans() {
        // Don't instantiate me.
    }

    public static boolean booleanValue(Boolean booleanObject) {
        if (booleanObject != null) {
            return booleanObject;
        }

        return false;
    }

}
