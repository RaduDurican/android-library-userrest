package de.hotel.android.library.remoteaccess;

import org.jibx.runtime.BindingDirectory;
import org.jibx.runtime.IBindingFactory;
import org.jibx.runtime.IMarshallingContext;
import org.jibx.runtime.JiBXException;
import org.jibx.runtime.impl.UnmarshallingContext;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;

public class HdeJiBXMarshallerImpl implements HdeJiBXMarshaller {
    private static final String UTF_8 = "UTF-8";

    @Override
    public <T> OutputStream marshalRequest(T request) throws JiBXException, UnsupportedEncodingException {
        IBindingFactory bindingFactory = BindingDirectory.getFactory(request.getClass());
        IMarshallingContext marshallingContext = bindingFactory.createMarshallingContext();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream, UTF_8);
        marshallingContext.setOutput(streamWriter);
        marshallingContext.marshalDocument(request);

        return outputStream;
    }

    @Override
    public <T> T unmarshalResponse(InputStream content, Class<T> type) throws JiBXException {
        IBindingFactory envelopeBindingFactory = BindingDirectory.getFactory(type);
        UnmarshallingContext envelopeUnmarshallingContext = (UnmarshallingContext) envelopeBindingFactory.createUnmarshallingContext();

        //noinspection unchecked
        return (T) envelopeUnmarshallingContext.unmarshalDocument(content, UTF_8);
    }
}
