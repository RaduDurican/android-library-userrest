package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;

import java.util.UUID;

import de.hotel.remoteaccess.v28.model.GetPropertyDescription;
import de.hotel.remoteaccess.v28.model.PropertyDescriptionRequest;
import de.hotel.remoteaccess.v28.model.WebServiceConsumerInformation;

public class HdeV28PropertyDescriptionRequestMapper {

    private final int id;
    private final String password;

    public HdeV28PropertyDescriptionRequestMapper(int id, @NonNull String password) {
        this.id = id;
        this.password = password;
    }

    public GetPropertyDescription createPropertyDescriptionRequest(@NonNull PropertyDescriptionQuery query) {
        PropertyDescriptionRequest propertyDescriptionRequest = new PropertyDescriptionRequest();
        propertyDescriptionRequest.setPropertyNumber(Integer.valueOf(query.getHotelId()));
        propertyDescriptionRequest.setCompanyNumber(query.getCompanyNumber());

        setStandardProperties(query.getLanguage().getIso2Language(), propertyDescriptionRequest);

        GetPropertyDescription getPropertyDescription = new GetPropertyDescription();
        getPropertyDescription.setObjRequest(propertyDescriptionRequest);

        return getPropertyDescription;
    }

    private void setStandardProperties(String isoA2Language, PropertyDescriptionRequest propertyDescriptionRequest) {
        WebServiceConsumerInformation webServiceConsumerInformation = new WebServiceConsumerInformation();
        webServiceConsumerInformation.setID(id);
        webServiceConsumerInformation.setPassword(password);
        propertyDescriptionRequest.setWebServiceConsumerInformation(webServiceConsumerInformation);

        propertyDescriptionRequest.setSecureContext(true);
        propertyDescriptionRequest.setToken(UUID.randomUUID().toString());
        propertyDescriptionRequest.setLanguage(isoA2Language);
    }
}
