package de.hotel.android.library.domain.model.query;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.criterion.HotelBookCriterion;
import de.hotel.android.library.domain.model.criterion.HotelRateCriterion;
import de.hotel.android.library.domain.model.criterion.HotelSearchCriterion;
import de.hotel.android.library.domain.model.enums.SortOrder;

public class HotelAvailQuery extends Query {
    private Integer pageSize;
    private Integer pageNumber;
    private @SortOrder int sortOrder = SortOrder.RECOMMENDED;
    private Integer companyNumber;

    private HotelSearchCriterion hotelSearchCriterion;
    private HotelAvailCriterion hotelAvailCriterion;
    private HotelRateCriterion hotelRateCriterion;
    private HotelBookCriterion hotelBookCriterion;


    @Nullable
    public HotelSearchCriterion getHotelSearchCriterion() {
        return hotelSearchCriterion;
    }

    public void setHotelSearchCriterion(@NonNull HotelSearchCriterion hotelSearchCriterion) {
        this.hotelSearchCriterion = hotelSearchCriterion;
    }

    @NonNull
    public HotelAvailCriterion getHotelAvailCriterion() {
        return hotelAvailCriterion;
    }

    public void setHotelAvailCriterion(@NonNull HotelAvailCriterion hotelAvailCriterion) {
        this.hotelAvailCriterion = hotelAvailCriterion;
    }

    @Nullable
    public HotelRateCriterion getHotelRateCriterion() {
        return hotelRateCriterion;
    }

    public void setHotelRateCriterion(@NonNull HotelRateCriterion hotelRateCtriterion) {
        this.hotelRateCriterion = hotelRateCtriterion;
    }

    public HotelBookCriterion getHotelBookCriterion() {
        return hotelBookCriterion;
    }

    public void setHotelBookCriterion(HotelBookCriterion hotelBookCriterion) {
        this.hotelBookCriterion = hotelBookCriterion;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public @Nullable Integer getCompanyNumber() {
        return companyNumber;
    }

    public void setCompanyNumber(@Nullable Integer companyNumber) {
        this.companyNumber = companyNumber;
    }
}
