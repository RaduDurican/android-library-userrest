package de.hotel.android.library.domain.model.criterion;

import de.hotel.android.library.domain.model.enums.RoomType;

/**
 * Every {@link RoomCriterion} defines one room.
 *
 * @author Johannes Mueller
 */
public class RoomCriterion implements Cloneable {

    private int roomType;
    private int adultCount;
    private int quantity;

    public RoomCriterion clone() {
        try {
            RoomCriterion roomCriterionClone = (RoomCriterion) super.clone();
            roomCriterionClone.setRoomType(this.getRoomType());
            roomCriterionClone.setAdultCount(this.getAdultCount());
            roomCriterionClone.setQuantity(this.getQuantity());
            return roomCriterionClone;
        } catch (CloneNotSupportedException e) {
            // RoomCriterion is cloneable.
        }
        throw new IllegalStateException();
    }

    @RoomType
    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(@RoomType int roomType) {
        this.roomType = roomType;
    }

    public int getAdultCount() {
        return adultCount;
    }

    public void setAdultCount(int adultCount) {
        this.adultCount = adultCount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
