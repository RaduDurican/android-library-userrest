package de.hotel.android.library.domain.adapter;

import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;

/**
 * Adapter handling requests for hotel searches.
 * <p/>
 * This class maps access layer response/request objects to the hotel
 * service domain model and vice versa.
 */
public interface HotelAdapter {
    HotelAvailResult searchAvailableHotels(HotelAvailQuery hotelAvailQuery);
}
