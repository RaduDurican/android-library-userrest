package de.hotel.android.library.domain.model.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * @author Johannes Mueller
 */
public class GeoPosition implements Cloneable, Parcelable {

    public static final Parcelable.Creator<GeoPosition> CREATOR = new Parcelable.Creator<GeoPosition>() {
        @Override
        public GeoPosition createFromParcel(Parcel parcel) {
            return new GeoPosition(parcel);
        }

        @Override
        public GeoPosition[] newArray(int i) {
            return new GeoPosition[i];
        }
    };
    private Double latitude;
    private Double longitude;

    public GeoPosition() {
    }

    public GeoPosition(Parcel parcel) {
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof GeoPosition)) {
            return false;
        }

        GeoPosition otherGeoPosition = (GeoPosition) other;

        return latitude.compareTo(otherGeoPosition.getLatitude()) == 0 && longitude.compareTo(otherGeoPosition.getLongitude()) == 0;

    }


    @NonNull
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(@NonNull Double latitude) {
        this.latitude = latitude;
    }

    @NonNull
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(@NonNull Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
