package de.hotel.android.library.domain.service.impl;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.adapter.LocationAutoCompleteAdapter;
import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;
import de.hotel.android.library.domain.service.LocationAutoCompleteFacade;

import java.io.IOException;
import java.util.List;

public class LocationAutoCompleteFacadeImpl implements LocationAutoCompleteFacade {
    private final LocationAutoCompleteAdapter locationAutoCompleteAdapter;

    public LocationAutoCompleteFacadeImpl(@NonNull LocationAutoCompleteAdapter locationAutoCompleteAdapter) {
        this.locationAutoCompleteAdapter = locationAutoCompleteAdapter;
    }

    @Override
    public List<Location> searchLocations(@NonNull LocationAutoCompleteQuery request) throws IOException {
        return locationAutoCompleteAdapter.searchLocations(request);
    }
}
