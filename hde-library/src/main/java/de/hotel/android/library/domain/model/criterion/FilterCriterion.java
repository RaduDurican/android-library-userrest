package de.hotel.android.library.domain.model.criterion;

import de.hotel.android.library.domain.model.enums.AmenityType;
import de.hotel.android.library.domain.model.enums.BreakfastType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FilterCriterion {
    private Integer starRatingMinimum;
    private Integer userRatingMinimum;
    private BigDecimal pricePerNightMaximum;
    private List<Integer> amenities = new ArrayList<>();
    private @BreakfastType int breakfastType = BreakfastType.UNKNOWN;

    public Integer getStarRatingMinimum() {
        return starRatingMinimum;
    }

    public void setStarRatingMinimum(Integer starRatingMinimum) {
        this.starRatingMinimum = starRatingMinimum;
    }

    public Integer getUserRatingMinimum() {
        return userRatingMinimum;
    }

    public void setUserRatingMinimum(Integer userRatingMinimum) {
        this.userRatingMinimum = userRatingMinimum;
    }

    public BigDecimal getPricePerNightMaximum() {
        return pricePerNightMaximum;
    }

    public void setPricePerNightMaximum(BigDecimal pricePerNightMaximum) {
        this.pricePerNightMaximum = pricePerNightMaximum;
    }

    public List<Integer> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Integer> amenities) {
        this.amenities = amenities;
    }

    public int getBreakfastType() {
        return breakfastType;
    }

    public void setBreakfastType(int breakfastType) {
        this.breakfastType = breakfastType;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof FilterCriterion)) {
            return false;
        }

        FilterCriterion otherFilterCriterion = (FilterCriterion) other;

        if (!starRatingEquals(otherFilterCriterion)) {
            return false;
        }
        if (!userRatingEquals(otherFilterCriterion)) {
            return false;
        }
        if (!pricePerNightEquals(otherFilterCriterion)) {
            return false;
        }
        if (breakfastType != otherFilterCriterion.breakfastType) {
            return false;
        }
        if (amenities.size() != otherFilterCriterion.amenities.size()) {
            return false;
        }
        for (@AmenityType int amenity : amenities) {
            if (!otherFilterCriterion.amenities.contains(amenity)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        FilterCriterion filterCriterion = new FilterCriterion();
        filterCriterion.setUserRatingMinimum(getUserRatingMinimum());
        filterCriterion.setStarRatingMinimum(getStarRatingMinimum());
        filterCriterion.setPricePerNightMaximum(getPricePerNightMaximum());
        filterCriterion.setBreakfastType(getBreakfastType());
        ArrayList<Integer> amenities = new ArrayList<>();
        amenities.addAll(getAmenities());
        filterCriterion.setAmenities(amenities);

        return filterCriterion;
    }

    private boolean starRatingEquals(FilterCriterion otherFilterCriterion) {
        if (starRatingMinimum == null && otherFilterCriterion.starRatingMinimum != null) {
            return false;
        }

        if (starRatingMinimum != null && otherFilterCriterion.starRatingMinimum == null) {
            return false;
        }

        if (starRatingMinimum != null && otherFilterCriterion.starRatingMinimum != null
                && starRatingMinimum.compareTo(otherFilterCriterion.getStarRatingMinimum()) != 0) {
            return false;
        }

        return true;
    }

    private boolean userRatingEquals(FilterCriterion otherFilterCriterion) {
        if (userRatingMinimum == null && otherFilterCriterion.userRatingMinimum != null) {
            return false;
        }

        if (userRatingMinimum != null && otherFilterCriterion.userRatingMinimum == null) {
            return false;
        }

        if (userRatingMinimum != null && otherFilterCriterion.userRatingMinimum != null
                && userRatingMinimum.compareTo(otherFilterCriterion.getUserRatingMinimum()) != 0) {
            return false;
        }

        return true;
    }

    private boolean pricePerNightEquals(FilterCriterion otherFilterCriterion) {
        if (pricePerNightMaximum == null && otherFilterCriterion.pricePerNightMaximum != null) {
            return false;
        }

        if (pricePerNightMaximum != null && otherFilterCriterion.pricePerNightMaximum == null) {
            return false;
        }

        if (pricePerNightMaximum != null && otherFilterCriterion.pricePerNightMaximum != null
                && pricePerNightMaximum.compareTo(otherFilterCriterion.getPricePerNightMaximum()) != 0) {
            return false;
        }

        return true;
    }
}
