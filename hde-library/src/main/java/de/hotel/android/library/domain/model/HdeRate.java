package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;

/**
 * Represents a Rate for a specific day
 */
public class HdeRate implements Parcelable {
    public static final Parcelable.Creator<HdeRate> CREATOR = new Parcelable.Creator<HdeRate>() {
        @Override
        public HdeRate createFromParcel(Parcel parcel) {
            return new HdeRate(parcel);
        }

        @Override
        public HdeRate[] newArray(int i) {
            return new HdeRate[i];
        }
    };
    private String effectiveDate;
    private String expireDate;
    private BigDecimal amountAfterTax;
    private BigDecimal amountBeforeTax;
    private String currencyCode;

    public HdeRate() {
    }

    public HdeRate(Parcel parcel) {
        effectiveDate = parcel.readString();
        expireDate = parcel.readString();
        amountAfterTax = (BigDecimal) parcel.readSerializable();
        amountBeforeTax = (BigDecimal) parcel.readSerializable();
        currencyCode = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(effectiveDate);
        parcel.writeString(expireDate);
        parcel.writeSerializable(amountAfterTax);
        parcel.writeSerializable(amountBeforeTax);
        parcel.writeString(currencyCode);
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public BigDecimal getAmountAfterTax() {
        return amountAfterTax;
    }

    public void setAmountAfterTax(BigDecimal amountAfterTax) {
        this.amountAfterTax = amountAfterTax;
    }

    public BigDecimal getAmountBeforeTax() {
        return amountBeforeTax;
    }

    public void setAmountBeforeTax(BigDecimal amountBeforeTax) {
        this.amountBeforeTax = amountBeforeTax;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
