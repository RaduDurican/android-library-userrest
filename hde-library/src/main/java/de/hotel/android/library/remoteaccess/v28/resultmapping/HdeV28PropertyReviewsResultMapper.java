package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hotel.android.library.domain.model.HotelCustomerReview;
import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.data.HdeValuationDetail;
import de.hotel.android.library.domain.model.enums.HdeGuestType;
import de.hotel.android.library.domain.model.review.ReviewCategoryPerson;
import de.hotel.remoteaccess.v28.model.ArrayOfValuationDetail;
import de.hotel.remoteaccess.v28.model.CustomerPropertyReviews;
import de.hotel.remoteaccess.v28.model.CustomerPropertyReviewsDetail;
import de.hotel.remoteaccess.v28.model.PropertyReviewsResponse;
import de.hotel.remoteaccess.v28.model.PropertyValuation;
import de.hotel.remoteaccess.v28.model.SpecificGuestType;
//import de.hotel.remoteaccess.v28.model.TravelPerson;
import de.hotel.remoteaccess.v28.model.ValuationDetail;

public class HdeV28PropertyReviewsResultMapper {

    public HotelPropertyReviews mapPropertyReviewsResponse(@NonNull PropertyReviewsResponse response) {
        HotelPropertyReviews hotelReviews = new HotelPropertyReviews();

        PropertyValuation propertyValuation = response.getPropertyValuation();

        ArrayOfValuationDetail valuationDetailArray = propertyValuation.getPropertyValuationDetails();

        List<ValuationDetail> valuationDetails = valuationDetailArray.getValuationDetailList();
        for (ValuationDetail valuationDetail : valuationDetails) {

            HdeValuationDetail hdeDetail = new HdeValuationDetail();
            createValuationDetail(hdeDetail, valuationDetail);
            @HdeGuestType int guestType = getGuestType(valuationDetail);
            hotelReviews.addValuationDetail(guestType, hdeDetail);
        }

        hotelReviews.setCustomerReviews(mapCustomerReviews(response.getCustomerPropertyReviews()));
        return hotelReviews;
    }

    private List<HotelCustomerReview> mapCustomerReviews(CustomerPropertyReviews reviews) {
        if (reviews == null || reviews.getCustomerPropertyValuationDetails() == null || reviews.getCustomerPropertyValuationDetails().getCustomerPropertyReviewsDetailList().isEmpty()) {
            return Collections.emptyList();
        }

        ArrayList<HotelCustomerReview> hotelCustomerReviews = new ArrayList<>();
        List<CustomerPropertyReviewsDetail> reviewsDetails = reviews.getCustomerPropertyValuationDetails().getCustomerPropertyReviewsDetailList();

        for (int i = 0; i < reviewsDetails.size(); i++) {
            CustomerPropertyReviewsDetail reviewsDetail = reviewsDetails.get(i);

            HotelCustomerReview customerReview = new HotelCustomerReview();
            customerReview.setComment(reviewsDetail.getComment());
            customerReview.setCommentPositive(reviewsDetail.getCommentPositiv());
            customerReview.setCommentNegative(reviewsDetail.getCommentNegativ());
            customerReview.setTravelDate(reviewsDetail.getDateTravel());
            customerReview.setReviewCategoryPerson(mapCategoryPerson(reviewsDetail.getValuationDetailCategoryPerson()));
            customerReview.setRecommendValue(reviewsDetail.getRecommendValue());
            hotelCustomerReviews.add(customerReview);
        }

        return hotelCustomerReviews;
    }

    private @ReviewCategoryPerson int mapCategoryPerson(String travelPerson) {
        switch (travelPerson) {
            case "GuestsTravellingAlone":
                return ReviewCategoryPerson.GUESTS_TRAVELLING_ALONE;
            case "GuestsTravellingAsACouple":
                return ReviewCategoryPerson.GUESTS_TRAVELLING_AS_A_COUPLE;
            case "GuestsTravellingInGroups":
                return ReviewCategoryPerson.GUESTS_TRAVELLING_IN_GROUPS;
            case "GuestsTravellingWithChildren":
                return ReviewCategoryPerson.GUESTS_TRAVELLING_WITH_CHILDREN;
            case "UndefinedGuestTypes":
                return ReviewCategoryPerson.UNDEFINED_GUEST_TYPES;
            case "Unknown":
            default:
                return ReviewCategoryPerson.UNKNOWN;
        }
    }

    private void createValuationDetail(HdeValuationDetail hdeDetail, ValuationDetail valuationDetail) {

        hdeDetail.setNumberOfReviews(valuationDetail.getNumberOfReviews());
        hdeDetail.setOverallEvaluation(asFloat(valuationDetail.getOverallEvaluation()));

        hdeDetail.setCatering(asFloat(valuationDetail.getCatering()));
        hdeDetail.setCleanliness(asFloat(valuationDetail.getCleanliness()));
        hdeDetail.setFriendliness(asFloat(valuationDetail.getFriendlinessAndCapabilityOfStaff()));
        hdeDetail.setRatioOfPriceAndPerformance(asFloat(valuationDetail.getRatioOfPriceAndPerformance()));
        hdeDetail.setRecommendationToOtherGuests(asFloat(valuationDetail.getRecommendationToOtherGuests()));
        hdeDetail.setRoomNoise(asFloat(valuationDetail.getRoomNoise()));
        hdeDetail.setRoomQuality(asFloat(valuationDetail.getRoomQuality()));
    }

    private int getGuestType(ValuationDetail valuationDetail) {
        SpecificGuestType guestType = valuationDetail.getValuationDetailCategory();

        switch (guestType) {
            case ALL_GUESTS:
                return HdeGuestType.ALL_GUESTS;

            case BUSINESS_GUESTS:
                return HdeGuestType.BUISNESS_GUESTS;

            case GUESTS_TRAVELLING_ALONE:
                return HdeGuestType.GUESTS_TRAVELLING_ALONE;

            case GUESTS_TRAVELLING_IN_GROUPS:
                return HdeGuestType.GUESTS_TRAVELLING_IN_GROUPS;

            case GUESTS_TRAVELLING_WITH_CHILDREN:
                return HdeGuestType.GUESTS_TRAVELLING_WITH_CHILDREN;

            case GUESTS_WITH_AGE_UNDER31_YEARS:
                return HdeGuestType.GUESTS_AGE_UNDER_31;

            case GUESTS_WITH_AGE_BETWEEN31_AND50_YEARS:
                return HdeGuestType.GUESTS_AGE_BETWEEN_31_50;

            case GUESTS_WITH_AGE_OVER50_YEARS:
                return HdeGuestType.GUESTS_AGE_OVER_50;

            default:
            case PRIVATE_GUESTS:
            case CORPORATE_CUSTOMER_GUESTS:
            case GUESTS_TRAVELLING_AS_A_COUPLE:
                return HdeGuestType.UNKNOWWN;
        }
    }

    private Float asFloat(String valueStr) {
        return (!TextUtils.isEmpty(valueStr)) ? Float.parseFloat(valueStr) : null;
    }
}
