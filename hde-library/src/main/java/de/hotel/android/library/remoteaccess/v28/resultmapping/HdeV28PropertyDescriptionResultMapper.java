package de.hotel.android.library.remoteaccess.v28.resultmapping;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelDescription;
import de.hotel.android.library.domain.model.HotelPicture;
import de.hotel.android.library.domain.model.HotelProperties;
import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.Amenity;
import de.hotel.android.library.domain.model.data.GeoPosition;
import de.hotel.android.library.domain.model.enums.AmenityType;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.enums.PictureCategory;
import de.hotel.android.library.domain.model.enums.PictureType;
import de.hotel.android.library.domain.model.query.PropertyDescription;
import de.hotel.android.library.util.Lists;
import de.hotel.remoteaccess.v28.model.AmenityDescription;
import de.hotel.remoteaccess.v28.model.ArrayOfPictureReference;
import de.hotel.remoteaccess.v28.model.CreditCard;
import de.hotel.remoteaccess.v28.model.Description;
import de.hotel.remoteaccess.v28.model.PictureReference;
import de.hotel.remoteaccess.v28.model.PropertyDescriptionExtendedResponse;
import timber.log.Timber;

public class HdeV28PropertyDescriptionResultMapper {

    public Hotel mapPropertyDescriptionResponseToHotel(@NonNull PropertyDescriptionExtendedResponse response) {
        Hotel hotel = new Hotel();
        fillHotel(hotel, response);

        hotel.setHotelProperties(mapPropertyDescriptionResponse(hotel, response));


        return hotel;
    }

    private HotelProperties mapPropertyDescriptionResponse(@NonNull Hotel hotel, @NonNull PropertyDescriptionExtendedResponse response) {
        PropertyDescription propertyDescription = new PropertyDescription();
        propertyDescription.setPropertyNumber(response.getBasicHotelParameters().getPropertyNumber());

        HotelProperties hotelProperties = new HotelProperties();
        hotelProperties.setPropertyDescription(propertyDescription);

        mapHotelDescription(hotelProperties, response);
        mapAmenities(hotelProperties, response);
        mapAcceptedCreditCards(hotelProperties, response);
        mapPictures(hotel, hotelProperties, response);

        return hotelProperties;
    }


    private void fillHotel(Hotel hotel, PropertyDescriptionExtendedResponse response) {
        de.hotel.remoteaccess.v28.model.Hotel basicHotelParameters = response.getBasicHotelParameters();
        hotel.setName(basicHotelParameters.getName());
        hotel.setAddress(mapAddress(response));
        hotel.setPicture(mapPictureUri(response));
        hotel.setHotelId(String.valueOf(basicHotelParameters.getPropertyNumber()));
        if (!TextUtils.isEmpty(basicHotelParameters.getRatingHotelDe())) {
            hotel.setStarsRating(Float.valueOf(basicHotelParameters.getRatingHotelDe()));
        }
        if (!TextUtils.isEmpty(basicHotelParameters.getOverallEvaluation())) {
            hotel.setUserRating(Float.valueOf(basicHotelParameters.getOverallEvaluation()));
        }
    }

    private Uri mapPictureUri(PropertyDescriptionExtendedResponse response) {
        if (response.getBasicHotelParameters().getMedia() == null || response.getBasicHotelParameters().getMedia().getPictureReferenceList() == null || response.getBasicHotelParameters().getMedia().getPictureReferenceList().isEmpty()) {
            return null;
        }

        Uri uri = null;
        try {
            uri = Uri.parse(response.getBasicHotelParameters().getMedia().getPictureReferenceList().get(0).getLink());
        } catch (Exception e) {
            Timber.e(e, "Could not parse the picture Uri");
        }

        return uri;
    }

    private Address mapAddress(PropertyDescriptionExtendedResponse response) {
        de.hotel.remoteaccess.v28.model.Hotel basicHotelParameters = response.getBasicHotelParameters();
        Address address = new Address();
        address.setCity(basicHotelParameters.getHotelAddress().getCity());
        address.setPostalCode(basicHotelParameters.getHotelAddress().getPostalCode());
        address.setCountryIsoA3(basicHotelParameters.getHotelAddress().getCountryISOa3());
        address.setStreet(basicHotelParameters.getHotelAddress().getStreet());
        GeoPosition geoPosition = new GeoPosition();
        geoPosition.setLatitude(basicHotelParameters.getHotelAddress().getGeographicCoordinates().getLatitude());
        geoPosition.setLongitude(basicHotelParameters.getHotelAddress().getGeographicCoordinates().getLongitude());
        address.setGeoPosition(geoPosition);

        return address;
    }

    private void mapAcceptedCreditCards(HotelProperties hotelProperties, PropertyDescriptionExtendedResponse response) {
        if (response.getAcceptedCreditCards() == null
                || Lists.isNullOrEmpty(response.getAcceptedCreditCards().getCreditCardList())) {
            return;
        }

        for (CreditCard creditCard : response.getAcceptedCreditCards().getCreditCardList()) {
            hotelProperties.getAcceptedCreditCards().add(mapCreditCard(creditCard));
        }
    }

    private
    @CreditCardType
    int mapCreditCard(CreditCard creditCard) {
        switch (creditCard.getCreditCardType().xmlValue()) {
            case "AmericanExpress":
                return CreditCardType.AMERICAN_EXPRESS;
            case "Visa":
                return CreditCardType.VISA;
            case "DinersClub":
                return CreditCardType.DINERS_CLUB;
            case "MasterCard_EuroCard":
                return CreditCardType.MASTER_CARD;
            case "JapanCreditBureau":
                return CreditCardType.JCB;
            // These are possible values in HSBW 2.8, but the app does not show them
//            case "Discover"
//            case "CarteBlanche"
//            case "EnRoute"
//            case "Optima"
//            case "AirCanada"
//            case "BankCardJapan"
        }

        // TODO Change this to a more robust error handling, e.g. an "unknown" type
        // It's better to ignore unknown credit cards in the UI than crashing the app
        throw new RuntimeException("Cannot map this credit card type: " + creditCard.getCreditCardType());
    }

    private void mapHotelDescription(HotelProperties hotelProperties, PropertyDescriptionExtendedResponse response) {
        if (response.getDescriptions() == null
                || response.getDescriptions().getHotelDescription() == null
                || Lists.isNullOrEmpty(response.getDescriptions().getHotelDescription().getDescriptionList())) {
            return;
        }

        HotelDescription hotelDescription = new HotelDescription();
        for (Description description : response.getDescriptions().getHotelDescription().getDescriptionList()) {
            if (description.getContentType() == null) {
                continue;
            }
            switch (description.getContentType()) {
                case HTML:
                    hotelDescription.setHtmlDescription(description.getString().trim());
                    continue;
                case PLAIN_TEXT:
                    hotelDescription.setPlainTextDescription(description.getString().trim());
                    continue;
            }
        }

        hotelProperties.setHotelDescription(hotelDescription);
    }

    private void mapAmenities(HotelProperties hotelProperties, PropertyDescriptionExtendedResponse response) {
        if (response.getAmenitiesHotel() != null) {
            mapHotelAmenities(hotelProperties.getHotelAmenities(), response.getAmenitiesHotel().getAmenityDescriptionList());
        }
        if (response.getAmenitiesLeisure() != null) {
            mapAmenites(hotelProperties.getLeisureAmenities(), response.getAmenitiesLeisure().getAmenityDescriptionList());
        }
        if (response.getAmenitiesGastronomy() != null) {
            mapAmenites(hotelProperties.getGastronomyAmenities(), response.getAmenitiesGastronomy().getAmenityDescriptionList());
        }
        if (response.getAmenitiesRoom() != null) {
            mapAmenites(hotelProperties.getRoomAmenities(), response.getAmenitiesRoom().getAmenityDescriptionList());
        }
        if (response.getComplimentaryServices() != null) {
            mapAmenites(hotelProperties.getComplimentaryServices(), response.getComplimentaryServices().getAmenityDescriptionList());
        }
    }

    private void mapHotelAmenities(List<Amenity> hotelAmenities, List<AmenityDescription> amenityDescriptionList) {
        if (Lists.isNullOrEmpty(amenityDescriptionList)) {
            return;
        }

        for (AmenityDescription amenityDescription : amenityDescriptionList) {
            Amenity amenity = new Amenity();

            if (amenityDescription.getDescription() == null) {
                amenity.setDescription(amenityDescription.getAmenity().xmlValue());
            } else {
                amenity.setDescription(amenityDescription.getDescription().getString());
            }

            switch (amenityDescription.getAmenity()) {
                case AIRCONDITION:
                    amenity.setAmenityType(AmenityType.AIR_CONDITIONING);
                    break;
                case EXECUTIVE_LEVEL:
                    amenity.setAmenityType(AmenityType.EXECUTIVE_FLOOR);
                    break;
                case FITNESS_ROOM:
                    amenity.setAmenityType(AmenityType.EXERCISE_GYM);
                    break;
                case AIRPORT_SHUTTLE:
                    amenity.setAmenityType(AmenityType.FREE_AIRPORT_SHUTTLE);
                    break;
                case PARKING_PLACE:
                case FREE_PARKING:
                case CARAVAN_PARKING:
                case FEE_REQUIRED_PARKING:
                case TRUCK_BUS_PARKING:
                    amenity.setAmenityType(AmenityType.PARKING_LOT);
                    break;
                case UNDERGROUND_PARKING:
                case LOCKABLE_GARAGE:
                    amenity.setAmenityType(AmenityType.INDOOR_PARKING);
                    break;
                case INSIDE_POOL:
                    amenity.setAmenityType(AmenityType.INDOOR_POOL);
                    break;
                case OUTSIDE_POOL:
                    amenity.setAmenityType(AmenityType.OUTDOOR_POOL);
                    break;
                case POOL:
                    amenity.setAmenityType(AmenityType.POOL);
                    break;
                case RESTAURANT:
                    amenity.setAmenityType(AmenityType.RESTAURANT);
                    break;
                case SAUNA:
                    amenity.setAmenityType(AmenityType.SAUNA);
                    break;
                case PETS_ALLOWED:
                    amenity.setAmenityType(AmenityType.PETS_ALLOWED);
                    break;
                case BALCONY:
                    amenity.setAmenityType(AmenityType.BALCONY);
                    break;
                case CRIB_AVAILABLE:
                    amenity.setAmenityType(AmenityType.CRIBS);
                    break;
                case INTERNET:
                    amenity.setAmenityType(AmenityType.INTERNET_ACCESS);
                    break;
                case NON_SMOKING_ROOM:
                    amenity.setAmenityType(AmenityType.NON_SMOKING);
                    break;
                case SAFE:
                    amenity.setAmenityType(AmenityType.SAFE);
                    break;
                case WLAN:
                    amenity.setAmenityType(AmenityType.WIFI);
                    break;
                case HANDICAPPED_ROOM:
                    amenity.setAmenityType(AmenityType.HANDICAP_ROOM);
                    break;
                default:
                    amenity.setAmenityType(AmenityType.OTHER);
                    break;
            }

            hotelAmenities.add(amenity);
        }
    }

    private void mapAmenites(List<Amenity> amenities, List<AmenityDescription> amenityDescriptionList) {
        if (Lists.isNullOrEmpty(amenityDescriptionList)) {
            return;
        }

        for (AmenityDescription amenityDescription : amenityDescriptionList) {
            Amenity amenity = new Amenity();
            amenity.setDescription(amenityDescription.getDescription().getString());

            amenities.add(amenity);
        }
    }

    // This method sets the hotel logo as the first picture and removes duplicates of the logo
    private void mapPictures(Hotel hotel, HotelProperties hotelProperties, PropertyDescriptionExtendedResponse response) {
        if (response.getPictureList() == null || response.getPictureList().getPictureReferenceList() == null) {
            return;
        }

        ArrayList<HotelPicture> hotelPictures = new ArrayList<>();

        ArrayOfPictureReference pictureList = response.getPictureList();
        boolean logoAdded = false;
        for (PictureReference picture : pictureList.getPictureReferenceList()) {
            HotelPicture hotelPicture = new HotelPicture();
            hotelPicture.setUrl(picture.getLink());

            mapPictureCategory(picture, hotelPicture);
            mapPictureType(picture, hotelPicture);

            if (hotel.getPicture() != null && hotelPicture.getUrl().equals(hotel.getPicture().toString())) {
                if (!hotelPictures.isEmpty() && hotelPictures.get(0) != null && !hotelPictures.get(0).getUrl().equals(hotelPicture.getUrl())) {
                    hotelPictures.add(0, hotelPicture);
                    logoAdded = true;
                }
            } else {
                hotelPictures.add(hotelPicture);
            }
        }

        if (hotel.getPicture() != null && !logoAdded) {
            HotelPicture logoPicture = new HotelPicture();
            logoPicture.setUrl(hotel.getPicture().toString());
            hotelPictures.add(0, logoPicture);
        }
        hotelProperties.setPictures(hotelPictures);
    }

    private void mapPictureType(PictureReference picture, HotelPicture hotelPicture) {
        switch (picture.getType()) {
            case LOGO:
                hotelPicture.setType(PictureType.LOGO);
                return;
            case MAP:
                hotelPicture.setType(PictureType.MAP);
                return;
            case PROPERTY_THUMBNAIL:
                hotelPicture.setType(PictureType.THUMBNAIL);
                return;
            case CORPORATE_LOGO:
                hotelPicture.setType(PictureType.CORPORATE_LOGO);
                return;
            case STANDARD:
            case OTHER:
            case DEFAULT:
            default:
                hotelPicture.setType(PictureType.DEFAULT);
                return;
        }
    }

    private void mapPictureCategory(PictureReference picture, HotelPicture hotelPicture) {
        switch (picture.getCategory()) {
            case EXTERIOR:
                hotelPicture.setCategory(PictureCategory.EXTERIOR);
                return;
            case SUROUNDING:
                hotelPicture.setCategory(PictureCategory.SURROUNDING);
                return;
            case COMBINED_PICTURE:
                hotelPicture.setCategory(PictureCategory.COMBINED);
                return;
            case WELLNESS:
                hotelPicture.setCategory(PictureCategory.WELLNESS);
                return;
            case POOL:
                hotelPicture.setCategory(PictureCategory.POOL);
                return;
            case GASTRONOMY:
                hotelPicture.setCategory(PictureCategory.GASTRONOMY);
                return;
            case ROOM:
                hotelPicture.setCategory(PictureCategory.ROOM);
                return;
            case LOGO:
                hotelPicture.setCategory(PictureCategory.LOGO);
                return;
            case MAP:
                hotelPicture.setCategory(PictureCategory.MAP);
                return;
            case OTHER:
            case NOT_SPECIFIED:
            default:
                hotelPicture.setCategory(PictureCategory.UNKNOWN);
                return;
        }
    }
}
