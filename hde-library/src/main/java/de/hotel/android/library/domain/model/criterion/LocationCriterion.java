package de.hotel.android.library.domain.model.criterion;

import android.support.annotation.Nullable;

import de.hotel.android.library.domain.model.data.GeoPosition;

/**
 * @author Johannes Mueller
 */
public class LocationCriterion {

    private Integer locationId;
    private GeoPosition geoPosition;
    private int vicinityInKm = 0;

    @Nullable
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(@Nullable Integer locationId) {
        this.locationId = locationId;
    }

    @Nullable
    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(@Nullable GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    public int getVicinityInKm() {
        return vicinityInKm;
    }

    public void setVicinityInKm(int vicinityInKm) {
        this.vicinityInKm = vicinityInKm;
    }
}
