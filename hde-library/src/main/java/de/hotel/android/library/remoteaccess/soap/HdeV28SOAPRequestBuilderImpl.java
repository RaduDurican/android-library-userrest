package de.hotel.android.library.remoteaccess.soap;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;

@SuppressWarnings("magicnumber")
public class HdeV28SOAPRequestBuilderImpl implements SOAPRequestBuilder {

    private static final String UTF_8 = "UTF-8";
    private static final String PREFIX_SOAP = "soap";
    private static final String PREFIX_XSI = "xsi";
    private static final String PREFIX_XSD = "xsd";
    private static final String NAMESPACE_SOAP = "http://schemas.xmlsoap.org/soap/envelope/";
    private static final String NAMESPACE_XSI = "http://www.w3.org/2001/XMLSchema-instance";
    private static final String NAMESPACE_XSD = "http://www.w3.org/2001/XMLSchema";
    private static final String NAMESPACE_TNS = "http://webservices.hotel.de/V2_8/";

    @Override
    public String buildSOAPRequest(String content) throws IOException {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        xmlSerializer.setOutput(writer);

        xmlSerializer.startDocument(UTF_8, false);

        // TODO fix prefixes
        xmlSerializer.setPrefix(PREFIX_SOAP, NAMESPACE_SOAP);
        xmlSerializer.setPrefix(PREFIX_XSI, NAMESPACE_XSI);
        xmlSerializer.setPrefix(PREFIX_XSD, NAMESPACE_XSD);
        xmlSerializer.setPrefix("", NAMESPACE_TNS);

        xmlSerializer.startTag(NAMESPACE_SOAP, "Envelope");
        xmlSerializer.startTag(NAMESPACE_SOAP, "Body");
        xmlSerializer.flush();
        writer.write(content);
        xmlSerializer.endTag(NAMESPACE_SOAP, "Body");

        xmlSerializer.endTag(NAMESPACE_SOAP, "Envelope");
        xmlSerializer.endDocument();

        return writer.toString();


    }
}
