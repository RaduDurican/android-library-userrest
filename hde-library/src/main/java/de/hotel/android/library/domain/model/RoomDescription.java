package de.hotel.android.library.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import de.hotel.android.library.domain.model.enums.RoomDescriptionType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Mueller
 */
public class RoomDescription implements Parcelable {
    public static final Parcelable.Creator<RoomDescription> CREATOR = new Parcelable.Creator<RoomDescription>() {
        @Override
        public RoomDescription createFromParcel(Parcel parcel) {
            return new RoomDescription(parcel);
        }

        @Override
        public RoomDescription[] newArray(int i) {
            return new RoomDescription[i];
        }
    };
    private List<String> roomDescriptions = new ArrayList<>();
    private List<String> imageList = new ArrayList<>();
    private int roomDescriptionType;
    private String descriptionShort;

    public RoomDescription() {
    }

    public RoomDescription(Parcel parcel) {
        parcel.readStringList(roomDescriptions);
        parcel.readStringList(imageList);

        roomDescriptionType = parcel.readInt();
        descriptionShort = parcel.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(roomDescriptions);
        parcel.writeStringList(imageList);

        parcel.writeInt(roomDescriptionType);
        parcel.writeString(descriptionShort);
    }

    @RoomDescriptionType
    public int getRoomDescriptionType() {
        return roomDescriptionType;
    }

    public void setRoomDescriptionType(@RoomDescriptionType int roomEnhancementType) {
        this.roomDescriptionType = roomEnhancementType;
    }

    @Nullable
    public List<String> getRoomDescriptions() {
        return roomDescriptions;
    }

    public void setDescriptionLong(@Nullable List<String> descriptions) {
        this.roomDescriptions = descriptions;
    }

    @Nullable
    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(@Nullable String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }
}
