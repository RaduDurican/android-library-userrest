package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({RoomType.UNKNOWN, RoomType.SINGLE, RoomType.DOUBLE, RoomType.THREE_BED, RoomType.FOUR_BED, RoomType.FAMILY})
public @interface RoomType {

    int UNKNOWN = 0;
    int SINGLE = 1;
    int DOUBLE = 2;
    int THREE_BED = 3;
    int FOUR_BED = 4;
    int FAMILY = 5;

}
