package de.hotel.android.library.domain.service;

import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;
import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;

/**
 * Domain Service handling requests concerning hotel searches.
 */
public interface HotelFacade {
    HotelAvailResult searchAvailableHotels(HotelAvailQuery hotelAvailQuery);
    HotelAvailResult fetchHotelDetailInformation(HotelAvailQuery hotelAvailQuery);
    Hotel fetchHotel(PropertyDescriptionQuery query);
    HotelPropertyReviews fetchHotelReviews(PropertyReviewsQuery query);
}
