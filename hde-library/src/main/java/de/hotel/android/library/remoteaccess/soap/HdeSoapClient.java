package de.hotel.android.library.remoteaccess.soap;

import java.io.IOException;
import java.net.URLConnection;

public interface HdeSoapClient {
    URLConnection performRequest(String soapRequestContent, String soapAction) throws Exception;
}
