package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

/**
 * @author Johannes Mueller
 */
@IntDef({RoomDescriptionType.UNKNOWN, RoomDescriptionType.STANDARD, RoomDescriptionType.ECONOMY, RoomDescriptionType.COMFORT, RoomDescriptionType.JUNIOR_SUITE, RoomDescriptionType.SUITE,
        RoomDescriptionType.APARTMENT, RoomDescriptionType.FAMILY, RoomDescriptionType.BUSINESS, RoomDescriptionType.TERRACE, RoomDescriptionType.BALCONY, RoomDescriptionType.OCEAN,
        RoomDescriptionType.LAKE, RoomDescriptionType.RIVER, RoomDescriptionType.POOL, RoomDescriptionType.MOUNTAIN})
public @interface RoomDescriptionType {

    int UNKNOWN = 0;
    int STANDARD = 1;
    int ECONOMY = 2;
    int COMFORT = 3;
    int JUNIOR_SUITE = 4;
    int SUITE = 5;
    int APARTMENT = 6;
    int FAMILY = 7;
    int BUSINESS = 8;
    int TERRACE = 9;
    int BALCONY = 10;
    int OCEAN = 11;
    int LAKE = 12;
    int RIVER = 13;
    int POOL = 14;
    int MOUNTAIN = 15;

}
