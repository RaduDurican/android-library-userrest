package de.hotel.android.library.domain.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;

/**
 * Adapter handling requests for making, modifying and cancelling reservations.
 * <p/>
 * This class is mapping access layer response/request objects to the hotel
 * service domain model and vice versa.
 *
 * @author Johannes Mueller
 */
public interface ReservationAdapter {
    HotelReservationResponse bookHotel(@NonNull HotelReservationQuery hotelReservationQuery);
}
