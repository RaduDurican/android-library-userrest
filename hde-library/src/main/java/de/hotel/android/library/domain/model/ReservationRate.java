package de.hotel.android.library.domain.model;

import de.hotel.android.library.domain.model.enums.RoomType;

public class ReservationRate {
    private @RoomType int roomtype;
    private String roomDescription;
    private int roomCount;

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public int getRoomtype() {
        return roomtype;
    }

    public void setRoomtype(int roomtype) {
        this.roomtype = roomtype;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }
}
