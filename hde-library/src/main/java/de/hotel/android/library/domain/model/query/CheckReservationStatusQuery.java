package de.hotel.android.library.domain.model.query;

import android.support.annotation.Nullable;

import java.util.Date;

public class CheckReservationStatusQuery extends Query {
    private String email;
    private String password;
    private Date startDate;
    private Date endDate;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(@Nullable Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
