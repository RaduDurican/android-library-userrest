package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;

public interface PropertyDescriptionAdapter {
    Hotel fetchHotelDescription(PropertyDescriptionQuery query);
}
