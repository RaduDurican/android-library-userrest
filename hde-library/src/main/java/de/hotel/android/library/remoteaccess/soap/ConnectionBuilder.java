package de.hotel.android.library.remoteaccess.soap;

import java.io.IOException;
import java.net.URLConnection;

/**
 * Creates an URLConnection for the specified soapAction
 */
public interface ConnectionBuilder {
    URLConnection createUrlConnection(int requestBodyByteCount, String soapAction) throws IOException;
}
