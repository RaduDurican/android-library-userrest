package de.hotel.android.library.remoteaccess.cola;

import java.io.IOException;
import java.net.URLConnection;

public interface WebFormRequestBuilder {
    URLConnection createWebFormUrlConnection(int requestBodyByteCount) throws IOException;
}
