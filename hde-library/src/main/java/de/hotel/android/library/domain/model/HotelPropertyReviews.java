package de.hotel.android.library.domain.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hotel.android.library.domain.model.data.HdeValuationDetail;
import de.hotel.android.library.domain.model.enums.HdeGuestType;

public class HotelPropertyReviews {

    private Map<Integer, HdeValuationDetail> categoryDetailsMap = new HashMap<>();
    private List<HotelCustomerReview> customerReviews = new ArrayList<>();

    public void addValuationDetail(@HdeGuestType int guestType, HdeValuationDetail hdeDetail) {
        this.categoryDetailsMap.put(guestType, hdeDetail);
    }

    public Map<Integer, HdeValuationDetail> getCategoryDetailsMap() {
        return categoryDetailsMap;
    }

    public HdeValuationDetail getValuationDetail(@HdeGuestType int guestType) {
        return this.categoryDetailsMap.get(guestType);
    }

    public List<HotelCustomerReview> getCustomerReviews() {
        return customerReviews;
    }

    public void setCustomerReviews(@NonNull List<HotelCustomerReview> customerReviews) {
        this.customerReviews = customerReviews;
    }
}
