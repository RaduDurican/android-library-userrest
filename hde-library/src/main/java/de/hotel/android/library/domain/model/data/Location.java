package de.hotel.android.library.domain.model.data;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import de.hotel.android.library.domain.model.enums.LocationType;

/**
 * @author Johannes Mueller
 */
public class Location implements Cloneable {

    private @LocationType int locationType;
    private Integer locationId;
    private String locationName;
    private Language locationLanguage;
    private String isoA3Country;
    private GeoPosition geoPosition;
    private String regionName;

    @LocationType
    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(@LocationType int locationType) {
        this.locationType = locationType;
    }

    @Nullable
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(@Nullable Integer locationId) {
        this.locationId = locationId;
    }

    @Nullable
    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(@Nullable String locationName) {
        this.locationName = locationName;
    }

    @Nullable
    public Language getLocationLanguage() {
        return locationLanguage;
    }

    public void setLocationLanguage(@Nullable Language locationLanguage) {
        this.locationLanguage = locationLanguage;
    }

    @Nullable
    public String getIsoA3Country() {
        return isoA3Country;
    }

    public void setIsoA3Country(@Nullable String isoA3Country) {
        this.isoA3Country = isoA3Country;
    }

    @Nullable
    public GeoPosition getGeoPosition() {
        return geoPosition;
    }

    public void setGeoPosition(@Nullable GeoPosition geoPosition) {
        this.geoPosition = geoPosition;
    }

    @Nullable
    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(@Nullable String regionName) {
        this.regionName = regionName;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof Location)) {
            return false;
        }

        Location otherLocation = (Location) other;

        Integer otherLocationId = otherLocation.locationId;
        if (locationId != null && otherLocationId != null) {
            if (locationId.intValue() != otherLocation.locationId.intValue()) {
                return false;
            }
        }

        if (!TextUtils.equals(locationName, otherLocation.locationName)) {
            return false;
        }

        if (locationLanguage != null && !locationLanguage.equals(otherLocation.locationLanguage)) {
            return false;
        }

        if (locationType != otherLocation.locationType) {
            return false;
        }

        if (geoPosition != null && !geoPosition.equals(((Location) other).getGeoPosition())) {
            return false;
        }

        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Location clone = (Location) super.clone();

        if (geoPosition != null) {
            clone.setGeoPosition((GeoPosition) geoPosition.clone());
        }
        if (locationLanguage != null) {
            clone.setLocationLanguage((Language) locationLanguage.clone());
        }

        return clone;
    }
}
