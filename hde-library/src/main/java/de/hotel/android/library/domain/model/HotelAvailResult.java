package de.hotel.android.library.domain.model;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Response for a hotel search considering availability.
 */
public class HotelAvailResult {

    private Long fromDate;
    private Long toDate;
    private List<Hotel> hotelList = new ArrayList<>();
    private int totalAvailableHotels;
    private Date timestamp;

    @Nullable
    public Long getFromDate() {
        return fromDate;
    }

    public void setFromDate(@Nullable Long fromDate) {
        this.fromDate = fromDate;
    }

    @Nullable
    public Long getToDate() {
        return toDate;
    }

    public void setToDate(@Nullable Long toDate) {
        this.toDate = toDate;
    }

    public List<Hotel> getHotelList() {
        return hotelList;
    }

    public void setHotelList(ArrayList<Hotel> hotelList) {
        this.hotelList = hotelList;
    }

    public int getTotalAvailableHotels() {
        return totalAvailableHotels;
    }

    public void setTotalAvailableHotels(int totalAvailableHotels) {
        this.totalAvailableHotels = totalAvailableHotels;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public @Nullable Hotel getHotelById(String hotelId) {
        for (int i = 0; i < hotelList.size(); i++) {
            if (hotelList.get(i). getHotelId().equals(hotelId)) {
                return hotelList.get(i);
            }
        }

        return null;
    }
}
