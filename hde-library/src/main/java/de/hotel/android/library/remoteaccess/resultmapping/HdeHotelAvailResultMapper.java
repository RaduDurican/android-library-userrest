package de.hotel.android.library.remoteaccess.resultmapping;

import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;

public interface HdeHotelAvailResultMapper {
    HotelAvailResult hotelAvailResult(HotelAvailQuery hotelAvailQuery, OTAHotelAvailRS availResponse);
}
