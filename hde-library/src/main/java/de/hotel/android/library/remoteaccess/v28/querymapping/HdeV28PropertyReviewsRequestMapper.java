package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;
import de.hotel.remoteaccess.v28.model.GetPropertyReviews;
import de.hotel.remoteaccess.v28.model.GuestTypeFilter;
import de.hotel.remoteaccess.v28.model.PropertyReviewsRequest;
import de.hotel.remoteaccess.v28.model.ReviewMode;

public class HdeV28PropertyReviewsRequestMapper {

    private HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28PropertyReviewsRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public GetPropertyReviews createPropertyReviewRequest(@NonNull PropertyReviewsQuery query) {
        PropertyReviewsRequest propertyReviewsRequest = new PropertyReviewsRequest();
        propertyReviewsRequest.setPropertyNumber(Integer.valueOf(query.getHotelId()));
        propertyReviewsRequest.setRequestedGuestTypeFilter(GuestTypeFilter.ALL_TYPES_TOGETHER);
        propertyReviewsRequest.setGetTextualReviews(true);
        propertyReviewsRequest.setReviewMode(ReviewMode.LATEST);

        baseRequestMapper.setStandardProperties(propertyReviewsRequest, query.getLanguage().getIso2Language());

        GetPropertyReviews getPropertyReviews = new GetPropertyReviews();
        getPropertyReviews.setObjRequest(propertyReviewsRequest);

        return getPropertyReviews;
    }
}
