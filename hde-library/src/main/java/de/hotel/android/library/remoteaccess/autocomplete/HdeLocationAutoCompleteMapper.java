package de.hotel.android.library.remoteaccess.autocomplete;

import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface HdeLocationAutoCompleteMapper {
    String mapLocationAutoCompleteRequest(LocationAutoCompleteQuery request);

    List<Location> mapLocationAutoCompleteResponse(InputStream inputStream) throws IOException;
}
