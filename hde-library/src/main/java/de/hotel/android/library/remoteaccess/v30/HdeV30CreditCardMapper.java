package de.hotel.android.library.remoteaccess.v30;

import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.enums.CreditCardType;

public interface HdeV30CreditCardMapper {
    String mapToCreditCardCode(CreditCard creditCard);
    @CreditCardType
    int mapFromCreditCardCode(String cardCode);
}
