package de.hotel.android.library.remoteaccess.resultmapping.reservation;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.HotelResResponseType;

public interface HdeHotelResResultMapper {
    HotelReservationResponse getHotelReservationResponse(@NonNull HotelReservationQuery hotelReservationQuery, @NonNull HotelResResponseType hotelResResponseType);
}
