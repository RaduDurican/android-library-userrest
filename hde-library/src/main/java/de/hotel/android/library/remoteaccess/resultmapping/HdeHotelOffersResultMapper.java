package de.hotel.android.library.remoteaccess.resultmapping;

import de.hotel.android.library.domain.model.Hotel;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.OTAHotelAvailRS;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.RoomStayType;

import java.util.List;

public interface HdeHotelOffersResultMapper {
    List<Hotel> createHotels(List<? extends RoomStayType> roomStays, OTAHotelAvailRS.CurrencyConversions currencyConversions, String iso3CurrencyCustomer);
}
