package de.hotel.android.library.domain.model.enums;

import android.support.annotation.IntDef;

@IntDef({HdeGuaranteeType.GUARANTEE_REQUIRED, HdeGuaranteeType.NONE, HdeGuaranteeType.CCDC_VOUCHER,
        HdeGuaranteeType.PROFILE, HdeGuaranteeType.DEPOSIT, HdeGuaranteeType.PRE_PAY,
        HdeGuaranteeType.DEPOSIT_REQUIRED})

public @interface HdeGuaranteeType {
    int NONE = 0;
    int GUARANTEE_REQUIRED = 1;
    int DEPOSIT = 2;

    //TODO Check if these values are really required
    int DEPOSIT_REQUIRED = 3;
    int PRE_PAY = 4;
    int CCDC_VOUCHER = 5;
    int PROFILE = 6;
}
