package de.hotel.android.library.remoteaccess.adapter;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.enums.RegistrationResultCode;
import de.hotel.android.library.domain.model.query.CustomerRegistrationQuery;
import de.hotel.android.library.domain.model.response.RegistrationResponse;

public interface HdeCustomerRegistrationAdapter {
    @RegistrationResultCode int registerCustomer(@NonNull CustomerRegistrationQuery query);
}
