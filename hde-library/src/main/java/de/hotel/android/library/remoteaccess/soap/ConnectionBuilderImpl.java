package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class ConnectionBuilderImpl implements ConnectionBuilder {

    // Trying to connect for ten seconds should be more than enough
    private static final int SOCKET_CONNECT_TIMEOUT = 10000;

    // Depending on how long webservices responses may take, we need a conservative timeout.
    // CheckReservationStatus takes over 30 seconds when loading about 160 reservations.
    // TODO: We definitely need this configurable based on the devices network type (WiFi, Edge, etc.)
    private static final int SOCKET_READ_TIMEOUT = 60000;

    private final String endpointUrl;

    public ConnectionBuilderImpl(@NonNull String endpointUrl) {
        this.endpointUrl = endpointUrl;
    }

    public URLConnection createUrlConnection(int requestBodyByteCount, String soapAction) throws IOException {
        URL url = new URL(endpointUrl);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        urlConnection.setFixedLengthStreamingMode(requestBodyByteCount);
        urlConnection.setReadTimeout(SOCKET_READ_TIMEOUT);
        urlConnection.setConnectTimeout(SOCKET_CONNECT_TIMEOUT);

        return urlConnection;
    }
}
