package de.hotel.android.library.remoteaccess.resultmapping.reservation;

import android.support.annotation.NonNull;

import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import com.hrsgroup.remoteaccess.hde.v30.model.ota.ErrorType;

import java.util.List;

public interface HdeHotelResErrorMapper {
    HotelReservationResponse handleHotelResErrors(@NonNull HotelReservationQuery hotelReservationQuery, @NonNull List<ErrorType> errors);
}
