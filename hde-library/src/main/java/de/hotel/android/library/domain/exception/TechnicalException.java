package de.hotel.android.library.domain.exception;

/**
 * @author Johannes Mueller
 */
//TODO: Kill this crap really fast. Broken, under-engineered and just wrong..
public class TechnicalException extends RuntimeException {

    private int exceptionCode;

    public TechnicalException(@TechnicalExceptionCode int exceptionCode, String detailMessage, Throwable t) {
        super(detailMessage, t);
        this.exceptionCode = exceptionCode;
    }
}
