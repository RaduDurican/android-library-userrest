package de.hotel.android.library.remoteaccess.v28.querymapping;

import android.support.annotation.NonNull;

import java.util.Calendar;

import de.hotel.android.library.domain.model.query.CheckReservationStatusQuery;
import de.hotel.android.library.util.Dates;
import de.hotel.remoteaccess.v28.model.CheckReservationStatus;
import de.hotel.remoteaccess.v28.model.CheckReservationStatusRequest;
import de.hotel.remoteaccess.v28.model.Date;
import de.hotel.remoteaccess.v28.model.SearchType;
import de.hotel.remoteaccess.v28.model.SimpleDateTime;
import de.hotel.remoteaccess.v28.model.SimpleTime;

public class HdeV28CheckReservationStatusRequestMapper {

    private static final int START_DATE_HOURS = 0;
    private static final int START_DAYS_MINUTES = 0;
    private static final int END_DATE_HOURS = 23;
    private static final int END_DATE_MINUTES = 59;

    private HdeV28BaseRequestMapper baseRequestMapper;

    public HdeV28CheckReservationStatusRequestMapper(@NonNull HdeV28BaseRequestMapper baseRequestMapper) {
        this.baseRequestMapper = baseRequestMapper;
    }

    public CheckReservationStatus createReservationStatusRequest(@NonNull CheckReservationStatusQuery query) {
        CheckReservationStatus checkReservationStatus = new CheckReservationStatus();
        CheckReservationStatusRequest request = new CheckReservationStatusRequest();

        baseRequestMapper.setStandardProperties(request, query.getLocale().getLanguage().getIso2Language());

        request.setCustomerPassword(query.getPassword());
        request.setHoteldeCustomerEmail(query.getEmail());
        request.setReservationSearchMethod(SearchType.BY_DEPARTURE_DATE);
        setDateRange(query, request);

        checkReservationStatus.setObjRequest(request);

        return checkReservationStatus;
    }

    private void setDateRange(CheckReservationStatusQuery query, CheckReservationStatusRequest request) {
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(query.getStartDate());

        SimpleDateTime startDateTime = new SimpleDateTime();
        Date startDate = new Date();
        startDate.setDay(startCalendar.get(Calendar.DAY_OF_MONTH));
        startDate.setMonth(startCalendar.get(Calendar.MONTH) + 1);
        startDate.setYear(startCalendar.get(Calendar.YEAR));

        startDateTime.setDate(startDate);

        SimpleTime startTime = new SimpleTime();
        startTime.setHours(START_DATE_HOURS);
        startTime.setMinutes(START_DAYS_MINUTES);
        startDateTime.setTime(startTime);


        Calendar endCalendar = (Calendar) startCalendar.clone();
        endCalendar.setTime(query.getEndDate());

        SimpleDateTime endDateTime = new SimpleDateTime();
        Date endDate = new Date();
        endDate.setDay(endCalendar.get(Calendar.DAY_OF_MONTH));
        endDate.setMonth(endCalendar.get(Calendar.MONTH) + 1);
        endDate.setYear(endCalendar.get(Calendar.YEAR));
        endDateTime.setDate(endDate);

        SimpleTime endTime = new SimpleTime();
        endTime.setHours(END_DATE_HOURS);
        endTime.setMinutes(END_DATE_MINUTES);
        endDateTime.setTime(endTime);

        request.setStartDate(startDateTime);
        request.setEndDate(endDateTime);
    }
}
