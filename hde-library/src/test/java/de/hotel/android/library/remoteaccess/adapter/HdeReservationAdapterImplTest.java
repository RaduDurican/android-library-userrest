package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.adapter.ReservationAdapter;
import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilderImpl;
import de.hotel.android.library.remoteaccess.soap.ConnectionBuilderV30;
import de.hotel.android.library.remoteaccess.soap.HdeSoapClientImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeCurrencyConversionResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResErrorMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResResultMapper;
import de.hotel.android.library.remoteaccess.resultmapping.reservation.HdeHotelResResultMapperImpl;
import de.hotel.android.library.remoteaccess.v30.CreditCardTypeMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapperImpl;
import de.hotel.android.library.remoteaccess.soap.HdeV30SOAPRequestBuilderImpl;
import de.hotel.android.library.remoteaccess.soap.SoapServiceImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqDefaultValuesMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelResRqMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaMapperImpl;
import com.hrsgroup.remoteaccess.hde.v30.v30SoapEnvelope;
import de.hotel.android.library.util.Dates;

import org.jibx.runtime.JiBXException;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.*;

/**
 * See http://joel-costigliola.github.io/assertj/ for assertj's fluent API
 */
@SuppressWarnings({"ConstantConditions", "magicnumber"}) // Not relevant for unit tests
public class HdeReservationAdapterImplTest {
    private static final String OTA_HOTEL_AVAIL_RS_GEO_SEARCH_XML = "OTA_HotelResRS.xml";

    @Test
    public void bookShouldCorrectValues() throws IOException, JiBXException {
        // Arrange
        FileInputStream inputStream = loadHotelResRSXmlFileWithSuccessfulReservation();

        HdeJiBXMarshaller jiBXMarshaller = new HdeJiBXMarshallerImpl();
        HdeHotelResResultMapper hdeHotelResResultMapper = createHdeHotelResResultMapper();

        Language language = new Language();
        language.setIso2Language("en");
        language.setVariantIso2Country("us");

        Locale locale = new Locale();
        locale.setIso3Currency("EUR");
        locale.setLanguage(language);

        HotelReservationQuery hotelReservationQuery = createHotelReservationQuery(locale);

        // Act
        v30SoapEnvelope otaAvailRSEnvelope = jiBXMarshaller.unmarshalResponse(inputStream, v30SoapEnvelope.class);
        HotelReservationResponse hotelReservationResponse = hdeHotelResResultMapper.getHotelReservationResponse(
                hotelReservationQuery,
                otaAvailRSEnvelope.getBody().getOtaHotelResRS());


        // Assert
        assertThat(hotelReservationResponse).isNotNull();

        // Reservation
        assertThat(hotelReservationResponse.getReservationId()).isEqualTo("5E2B041A64754BDF854DBAA2B88E708B");
        assertThat(hotelReservationResponse.getReservationNumber()).isEqualTo("66496781");
        assertThat(hotelReservationResponse.getCancellationCode()).isEqualTo("5E2B041A");
        assertThat(hotelReservationResponse.getUniqueId()).isEqualTo("My_GUID");

        // Booking custoemr
        assertThat(hotelReservationResponse.getBookingCustomer().getEmail()).isEqualTo("hank.scorpio@globex.com");
        assertThat(hotelReservationResponse.getBookingCustomer().getFirstname()).isEqualTo("Hank");
        assertThat(hotelReservationResponse.getBookingCustomer().getLastname()).isEqualTo("Scorpio");

        // Hotel offer
        assertThat(hotelReservationResponse.getHotels()).hasSize(1);
        Hotel hotel = hotelReservationResponse.getHotels().get(0);

        // Hotel
        assertThat(hotel.getName()).isEqualTo("Hotel Saint Quentin");
        assertThat(hotel.getPicture().toString()).isEqualTo("https://www.hotel.de/media/hotel/logo/148043.jpg");
        assertThat(hotel.getAddress().getGeoPosition().getLatitude()).isEqualTo(48.87862);
        assertThat(hotel.getAddress().getGeoPosition().getLongitude()).isEqualTo(2.35549);
        assertThat(hotel.getAddress().getStreet()).isEqualTo("27 Rue St Quentin");
        assertThat(hotel.getAddress().getCity()).isEqualTo("Paris");
        assertThat(hotel.getAddress().getPostalCode()).isEqualTo("75010");
        assertThat(hotel.getAddress().getCountryIsoA3()).isEqualTo("FRA");
        assertThat(hotel.getStarsRating()).isEqualTo(2f);
        assertThat(hotel.getHotelId()).isEqualTo("148043");

        // Credit card
        assertThat(hotelReservationResponse.getCreditCard().getCreditCardType()).isEqualTo(CreditCardType.VISA);
        assertThat(hotelReservationResponse.getCreditCard().getMonth()).isEqualTo(1);
        assertThat(hotelReservationResponse.getCreditCard().getName()).isEqualTo("Hans Maulwurf");
        assertThat(hotelReservationResponse.getCreditCard().getNumber()).isNull(); // Currently the format the HSBW returns is not like it is defined in the XSD
        assertThat(hotelReservationResponse.getCreditCard().getYear()).isEqualTo(2016);

        // Travelers
        assertThat(hotelReservationResponse.getTravelers()).hasSize(2);
        assertThat(hotelReservationResponse.getTravelers().get(0).getFirstname()).isEqualTo("Hank");
        assertThat(hotelReservationResponse.getTravelers().get(0).getLastname()).isEqualTo("Scorpio");
        assertThat(hotelReservationResponse.getTravelers().get(1).getFirstname()).isEqualTo("Test2GivenName");
        assertThat(hotelReservationResponse.getTravelers().get(1).getLastname()).isEqualTo("Test2Surname");
    }

    private HotelReservationQuery createHotelReservationQuery(Locale locale) {
        HotelReservationQuery hotelReservationQuery = new HotelReservationQuery();
        hotelReservationQuery.setLocale(locale);

        hotelReservationQuery.setHotelCode("262079");
        hotelReservationQuery.setInvBlockCode("77BF9E6EF3F942B290679F1A9B1B68D6");
        hotelReservationQuery.setRatePlanCode("nor316629");
        hotelReservationQuery.setRoomTypeCode("124064");

        long today = System.currentTimeMillis();
        long tomorrow = today + Dates.ONE_DAY_IN_MILLISECONDS;

        hotelReservationQuery.setFrom(today);
        hotelReservationQuery.setTo(tomorrow);

        ArrayList<Customer> travelingPersons = new ArrayList<>();
        Customer customer = new Customer();
        customer.setFirstname("Traveller first name 1");
        customer.setLastname("Traveller last name 1");
        customer.setEmail("traveller1@hotel.de");
        travelingPersons.add(customer);

        Customer customer2 = new Customer();
        customer2.setFirstname("Traveller first name 2");
        customer2.setLastname("Traveller last name 2");
        customer2.setEmail("traveller2@hotel.de");
        travelingPersons.add(customer2);

        hotelReservationQuery.setTravelingPersons(travelingPersons);

        Customer bookingPerson = new Customer();
        bookingPerson.setFirstname("Booker first name");
        bookingPerson.setLastname("Booker last name");
        bookingPerson.setEmail("ad@hotel.de");
        bookingPerson.setPhone("+499111234567");

        CreditCard creditCard = new CreditCard();
        creditCard.setName("Credit card holder name");
        creditCard.setNumber("4111111111111111");
        creditCard.setCreditCardType(CreditCardType.VISA);
        creditCard.setMonth(12);
        creditCard.setYear(2017);
        hotelReservationQuery.setCreditCard(creditCard);

        hotelReservationQuery.setBookingPerson(bookingPerson);
        return hotelReservationQuery;
    }

    private ReservationAdapter createReservationAdapter() {
        // TODO: Use mock once it becomes available...
        return new HdeReservationAdapterImpl(
                RemoteAccessTargetEnvironmentType.BETA,
                new SoapServiceImpl(new HdeJiBXMarshallerImpl(),
                        new HdeV30SOAPRequestBuilderImpl("180", "An324Krd911d"),
                        new HdeSoapClientImpl(
                                new ConnectionBuilderV30(
                                        new ConnectionBuilderImpl("https://betaappservices.hotel.de/V3_0/OTA2012A/HTNG_SeamlessShopAndBookService.svc")))),
                new HdeV30OtaHotelResRqDefaultValuesMapperImpl(
                        new HdeV30OtaHotelResRqMapperImpl(
                                new HdeV30OtaMapperImpl("180", RemoteAccessTargetEnvironmentType.BETA),
                                new HdeV30CreditCardMapperImpl(new CreditCardTypeMapper()))),
                new HdeHotelResErrorMapperImpl(),
                createHdeHotelResResultMapper());
    }

    private HdeHotelResResultMapper createHdeHotelResResultMapper() {
        return new HdeHotelResResultMapperImpl(
                new HdeHotelOffersResultMapperImpl(
                        new HdeCurrencyConversionResultMapperImpl(),
                        new HdeV30CreditCardMapperImpl(new CreditCardTypeMapper())),
                new HdeV30CreditCardMapperImpl(new CreditCardTypeMapper()));
    }

    private FileInputStream loadHotelResRSXmlFileWithSuccessfulReservation() throws FileNotFoundException {
        URL otaHotelAvailRSUrl = this.getClass().getClassLoader().getResource(OTA_HOTEL_AVAIL_RS_GEO_SEARCH_XML);
        File otaHotelAvailRSFile = new File(otaHotelAvailRSUrl.getFile());
        return new FileInputStream(otaHotelAvailRSFile);
    }
}
