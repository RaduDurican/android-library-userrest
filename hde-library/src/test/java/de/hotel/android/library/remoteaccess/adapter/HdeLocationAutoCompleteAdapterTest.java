package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.domain.model.query.LocationAutoCompleteQuery;
import de.hotel.android.library.domain.service.LocationAutoCompleteFacade;
import de.hotel.android.library.domain.service.impl.LocationAutoCompleteFacadeImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeLocationAutoCompleteAdapter;
import de.hotel.android.library.remoteaccess.autocomplete.HdeLocationAutoCompleteMapperImpl;
import de.hotel.android.library.remoteaccess.http.HdeLocationAutoCompleteHttpClient;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * See http://joel-costigliola.github.io/assertj/ for assertj's fluent API
 */
@SuppressWarnings({"ConstantConditions", "magicnumber"}) // Not relevant for unit tests
public class HdeLocationAutoCompleteAdapterTest {
    @Test
    public void parsingAutoCompleteResultShouldReturnCorrectAmountOfLocations() throws IOException {
        // Arrange
        HdeLocationAutoCompleteHttpClient httpClientMock = Mockito.mock(HdeLocationAutoCompleteHttpClient.class);
        InputStream autoCompleteResponse = new ByteArrayInputStream(getMarkHotelInGerman().getBytes());
        when(httpClientMock.performRequest(anyString())).thenReturn(autoCompleteResponse);

        LocationAutoCompleteFacade locationAutoCompleteFacade = new LocationAutoCompleteFacadeImpl(new HdeLocationAutoCompleteAdapter(httpClientMock, new HdeLocationAutoCompleteMapperImpl()));
        LocationAutoCompleteQuery locationAutoCompleteQuery = new LocationAutoCompleteQuery("mark hotel", "en");

        // Act
        List<Location> locations = null;
        try {
            locations = locationAutoCompleteFacade.searchLocations(locationAutoCompleteQuery);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Assert
        assertThat(locations).hasSize(60);

        assertThat(locations.get(0).getLocationName()).isEqualTo("Choice Hotels International");
        assertThat(locations.get(0).getRegionName()).isEqualTo("San Marcos, Texas");
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("USA");
        assertThat(locations.get(0).getLocationId()).isEqualTo(23807);
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.CITY);

        // Debug
        dumpLocations(locations);
    }

    @Test
    public void parsingAutoCompleteResultShouldReturnCorrectAmountOfLocationsInChinese() throws IOException {
        // Arrange
        HdeLocationAutoCompleteHttpClient httpClientMock = Mockito.mock(HdeLocationAutoCompleteHttpClient.class);
        InputStream autoCompleteResponse = new ByteArrayInputStream(getNurembergInChinese().getBytes());
        when(httpClientMock.performRequest(anyString())).thenReturn(autoCompleteResponse);

        LocationAutoCompleteFacade locationAutoCompleteFacade = new LocationAutoCompleteFacadeImpl(new HdeLocationAutoCompleteAdapter(httpClientMock, new HdeLocationAutoCompleteMapperImpl()));
        LocationAutoCompleteQuery locationAutoCompleteQuery = new LocationAutoCompleteQuery("nurem", "zh");

        // Act
        List<Location> locations = null;
        try {
            locations = locationAutoCompleteFacade.searchLocations(locationAutoCompleteQuery);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Assert
        assertThat(locations).hasSize(60);

        assertThat(locations.get(0).getLocationName()).isEqualTo("纽伦堡 (Nürnberg, Nuremberg)");
        assertThat(locations.get(0).getRegionName()).isEqualTo("巴伐利亚 (Bayern)");
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("DEU");
        assertThat(locations.get(0).getLocationId()).isEqualTo(39020);
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.CITY);

        assertThat(locations.get(1).getLocationName()).isEqualTo("Nuremberg");
        assertThat(locations.get(1).getRegionName()).isEqualTo("Pennsylvania");
        assertThat(locations.get(1).getIsoA3Country()).isEqualTo("USA");
        assertThat(locations.get(1).getLocationId()).isEqualTo(19724);
        assertThat(locations.get(1).getLocationType()).isEqualTo(LocationType.CITY);

        // Debug
        dumpLocations(locations);
    }

    @Test
    public void parsingAutoCompleteResultShouldReturnCorrectAmountOfLocationsInEnglish() throws IOException {
        // Arrange
        HdeLocationAutoCompleteHttpClient httpClientMock = Mockito.mock(HdeLocationAutoCompleteHttpClient.class);
        InputStream autoCompleteResponse = new ByteArrayInputStream(getNurembergInEnglish().getBytes());
        when(httpClientMock.performRequest(anyString())).thenReturn(autoCompleteResponse);

        LocationAutoCompleteFacade locationAutoCompleteFacade = new LocationAutoCompleteFacadeImpl(new HdeLocationAutoCompleteAdapter(httpClientMock, new HdeLocationAutoCompleteMapperImpl()));
        LocationAutoCompleteQuery locationAutoCompleteQuery = new LocationAutoCompleteQuery("nurem", "en");

        // Act
        List<Location> locations = null;
        try {
            locations = locationAutoCompleteFacade.searchLocations(locationAutoCompleteQuery);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Assert
        assertThat(locations).hasSize(60);

        assertThat(locations.get(0).getLocationName()).isEqualTo("Nuremberg (Nürnberg)");
        assertThat(locations.get(0).getRegionName()).isEqualTo("Bavaria (Bayern)");
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("DEU");
        assertThat(locations.get(0).getLocationId()).isEqualTo(39020);
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.CITY);

        // Debug
        dumpLocations(locations);
    }

    @Test
    public void parsingAutoCompleteResultShouldReturnCorrectAmountOfHotelsInInChinese() throws IOException {
        // Arrange
        HdeLocationAutoCompleteHttpClient httpClientMock = Mockito.mock(HdeLocationAutoCompleteHttpClient.class);
        InputStream autoCompleteResponse = new ByteArrayInputStream(getSmartCityHotelInChinese().getBytes());
        when(httpClientMock.performRequest(anyString())).thenReturn(autoCompleteResponse);

        LocationAutoCompleteFacade locationAutoCompleteFacade = new LocationAutoCompleteFacadeImpl(new HdeLocationAutoCompleteAdapter(httpClientMock, new HdeLocationAutoCompleteMapperImpl()));
        LocationAutoCompleteQuery locationAutoCompleteQuery = new LocationAutoCompleteQuery("smartc", "zh");

        // Act
        List<Location> locations = null;
        try {
            locations = locationAutoCompleteFacade.searchLocations(locationAutoCompleteQuery);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Assert
        assertThat(locations).hasSize(2);

        assertThat(locations.get(0).getLocationName()).isEqualTo("Smartcity Designhotel");
        assertThat(locations.get(0).getRegionName()).isEqualTo("汉诺威 (Hannover, Hanover), 下萨克森 (Niedersachsen)");
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("DEU");
        assertThat(locations.get(0).getLocationId()).isEqualTo(325737);
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.HOTEL);

        assertThat(locations.get(1).getLocationName()).isEqualTo("Hotel SmartCue52 (昭盛52行館)");
        assertThat(locations.get(1).getRegionName()).isEqualTo("台中市");
        assertThat(locations.get(1).getIsoA3Country()).isEqualTo("TWN");
        assertThat(locations.get(1).getLocationId()).isEqualTo(341583);
        assertThat(locations.get(1).getLocationType()).isEqualTo(LocationType.HOTEL);

        // Debug
        dumpLocations(locations);
    }

    private void dumpLocations(List<Location> locations) {
        for (Location location : locations) {
            System.out.print(location.getLocationId());
            System.out.print(", " + location.getLocationName());
            System.out.print(", " + location.getRegionName());
            System.out.print(", " + location.getIsoA3Country());
            System.out.print(", " + location.getLocationType());
            System.out.print(", " + location.getGeoPosition());
            System.out.println(", " + location.getLocationLanguage());
        }
    }

    private String getMarkHotelInGerman() {
        return "mark hotel§Choice Hotels International, San Marcos, Texas, USA$LC23807|58~Comfort Hotels, San Marcos, Texas, USA$LC23807|76~Marriott Hotels & Resorts, Marco, Florida, USA$LC16291|696~bon marche hotel Bochum, Bochum, Nordrhein-Westfalen, DEU$H283281~Winters Hotel Berlin Mitte Am Gendarmenmarkt, Berlin, DEU$H239047~Dorint Hotel am Heumarkt Köln, Köln, Nordrhein-Westfalen, DEU$H101246~BEST WESTERN Hotel am Spittelmarkt, Berlin, DEU$H229735~Hotel Marktwirtschaft, Burg auf Fehmarn, Schleswig-Holstein, DEU$H265307~Hotel Am Hopfenmarkt, Rostock, Mecklenburg-Vorpommern, DEU$H116628~Hotel Märkischer Hof, Luckenwalde, Brandenburg, DEU$H170126~Hotel am Markt, Karlsruhe, Baden-Württemberg, DEU$H137927~Hotel Markgraf Leipzig, Leipzig, Sachsen, DEU$H139372~Hotel Hackescher Markt, Berlin, DEU$H117990~Atlanta Hotel International Leipzig, Markkleeberg, Sachsen, DEU$H52472~Hotel Alter Markt, Berlin, DEU$H139382~Hotel am Pferdemarkt, Rotenburg (Wümme), Niedersachsen, DEU$H172911~Adina Apartment Hotel Berlin Hackescher Markt, Berlin, DEU$H276749~Hotel Neuer Markt, Vechta, Niedersachsen, DEU$H383282~Hotel Seegarten, Grünheide (Mark), Brandenburg, DEU$H189841~City Partner Hotel Am Jakobsmarkt, Nürnberg, Bayern, DEU$H84299~Hotel Marco Polo, Münster, Nordrhein-Westfalen, DEU$H335499~Hotel Markkleeberger Hof, Markkleeberg, Sachsen, DEU$H52336~Angel's - das hotel am fruchtmarkt, Sankt Wendel, Saarland, DEU$H99014~City Hotel Mark Michelstadt, Michelstadt, Hessen, DEU$H201744~Hotel Alte Mark, Hamm, Nordrhein-Westfalen, DEU$H121422~Hotel Herrenküferei, Markgröningen, Baden-Württemberg, DEU$H203193~Hotel am Garnmarkt, Götzis, Vorarlberg, AUT$H401742~Hotel Rößle, Stetten am kalten Markt, Baden-Württemberg, DEU$H335238~Hotel am Obermarkt, Freiberg, Sachsen, DEU$H215369~Hotel Zum Goldenen Hahnen, Markgröningen, Baden-Württemberg, DEU$H230962~Autobahn-Rasthaus Hünxe Ost Marché Hotel Hünxe, Hünxe, Nordrhein-Westfalen, DEU$H335234~Smeraldo Suite Hotel, San Benedetto del Tronto, Marken (Marche), ITA$H317694~eee Hotel, Marchtrenk, Oberösterreich, AUT$H407838~Hotel Anker, Marktheidenfeld, Bayern, DEU$H142328~Hotel Markgräfler Hof, Freiburg im Breisgau, Baden-Württemberg, DEU$H186360~Hotel Schönblick, Neumarkt i.d. OPf., Bayern, DEU$H223625~Mindness Hotel Bischofschloss, Markdorf, Baden-Württemberg, DEU$H83333~Hotel am Wollmarkt, Braunschweig, Niedersachsen, DEU$H183193~Hotel Marconi, Mailand (Milano), Lombardei (Lombardia), ITA$H181899~Hotel Marktterrassen, Hennef (Sieg), Nordrhein-Westfalen, DEU$H127538~Hotel Spessarttor - Villa Italia, Marktheidenfeld, Bayern, DEU$H436648~Hotel Wandinger Hof, Markt Schwaben, Bayern, DEU$H98917~Hotel Zum Märchenwald, Lingen (Ems), Niedersachsen, DEU$H121564~Hotel Galerie Markgraf, Emmendingen, Baden-Württemberg, DEU$H196189~Hotel zum Löwen, Marktheidenfeld, Bayern, DEU$H198203~Stadt-gut-Hotel Altstadt-Hotel Stern, Neumarkt i.d. OPf., Bayern, DEU$H163123~Hotel Am Markt, München, Bayern, DEU$H190917~Hotel am Markt, Werder (Havel), Brandenburg, DEU$H359409~Hotel an der Marktkirche, Hannover, Niedersachsen, DEU$H51564~Hotel Markgraf, Lehnin, Brandenburg, DEU$H51294~Hotel Markgraf, Rostock, Mecklenburg-Vorpommern, DEU$H99330~Hotel am Markt, Ennigerloh, Nordrhein-Westfalen, DEU$H373860~Hotel Greinwald, Marktoberdorf, Bayern, DEU$H379422~Hotel Markgräfler Hof garni, Karlsruhe, Baden-Württemberg, DEU$H217472~Meister BÄR HOTEL Fichtelgebirge, Marktredwitz, Bayern, DEU$H142640~Metzgerei-Hotel-Gasthof Wittmann, Neumarkt i.d. OPf., Bayern, DEU$H265199~Ramada Hotel and Conference Centre Marcoola Beach, Marcoola Beach, Queensland, AUS$H322504~Hotel Georgenhof, Markt Schwaben, Bayern, DEU$H306169~Hotel Marco Polo, Hamburg, DEU$H276319~Hotel Rubens - Grote Markt, Antwerpen, Flämische Region, BEL$H183726\n"
                + "\n" + "<!-- (c) hotel.de AG (30.03.2015 14:27:04) -->\n" + "\n" + "<!-- Compression: GZip; Uncompressed Content-Length: 3835 -->";
    }

    private String getNurembergInChinese() {
        return "nurem§纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$L39020~Nuremberg, Pennsylvania, USA$L19724~纽伦堡机场 (NUE) (Flughafen Nürnberg, Nuremberg Airport), DEU$L125916~纽伦堡 火车总站 (Hauptbahnhof Nürnberg, Nuremberg Central Station), DEU$L134316~Congress Center Nuremberg (Congress Center Nürnberg, Congress Center Nuremberg), 纽伦堡, DEU$L137749~纽伦堡 展览会 (Nürnberg Messe, Nuremberg Fair), DEU$L129467~Accor Hotels, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$LC39020|31~InterContinental Hotels Group, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$LC39020|26~Novina Hotel Tillypark, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H251919~NH Nürnberg City Center, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H156713~Best Western Hotel Nürnberg City West, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H180207~Acom, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H255583~Azimut Hotel Nuremberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H93356~Novina Hotel Südwestpark Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84294~TOP Dürer Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H51841~MARITIM Hotel Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H99093~Holiday Inn Nürnberg City Centre, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H141259~Noris Hotel Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H116532~Derag Livinghotel Maximilian, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52036~bfwhotel und Tagungszentrum, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H172950~Mövenpick Hotel Nürnberg-Airport, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H129592~Holiday Inn Express Nürnberg City - Hauptbahnhof, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H403777~Hotel Agneshof - Partner of SORAT Hotels, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84023~Burghotel Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84093~Hotel Erlenstegen, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H135073~Appart Hotel Tassilo, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H85648~Motel One Nürnberg City, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H224954~Arvena Messe Hotel an der NürnbergMesse, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52345~myHOME by Lehrieder, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H345380~Hotel Gerhard, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H123857~Motel One Nürnberg-Plärrer, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H180268~Sorat Hotel Saxx, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52418~InterCityHotel Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H102151~Hotel Alpha, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H50900~Hotel Burgschmiet, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H51381~Ramada Nürnberg Parkhotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H51990~Garden Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84091~Ringhotel Loew´s Merkur, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52642~Ibis Nürnberg City am Plaerrer, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H127620~Novotel Nuernberg Centre Ville, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H429961~Burghotel Stammhaus, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52143~Arvena Park Hotel - Das Hotel am Franken-Center, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H116448~INVITE Hotel Nürnberg City, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H51020~Schindlerhof, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H197247~Advantage Appartement Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H102123~Hotel Avenue, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H131277~Hotel Marienbad, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H127684~Ibis Nürnberg Hauptbahnhof, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H127572~Numberone Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H406279~Hotel am Färbertor, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H281008~art & business Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H52417~Best Living Hotel AROTEL, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84094~Hotel Victoria Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H127587~Novotel Nürnberg Am Messezentrum, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H84298~Hilton Nuremberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H102291~Best Western Hotel Nürnberg am Hauptbahnhof, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H93359~Metropol, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H93339~Leonardo Hotel Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H144871~Park Inn by Radisson Nürnberg, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H110583~Behringers City Hotel, 纽伦堡 (Nürnberg, Nuremberg), 巴伐利亚 (Bayern), DEU$H98883";
    }

    private String getNurembergInEnglish() {
        return "nurem§Nuremberg (Nürnberg), Bavaria (Bayern), DEU$L39020~Nuremberg, Pennsylvania, USA$L19724~Nuremberg Airport (NUE) (Flughafen Nürnberg), DEU$L125916~Nuremberg Central Station (Hauptbahnhof Nürnberg), DEU$L134316~Congress Center Nuremberg (Congress Center Nürnberg), DEU$L137749~Nuremberg Fair (Nürnberg Messe), DEU$L129467~Accor Hotels, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$LC39020|31~InterContinental Hotels Group, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$LC39020|26~Novina Hotel Tillypark, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H251919~NH Nürnberg City Center, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H156713~Best Western Hotel Nürnberg City West, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H180207~Acom, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H255583~Azimut Hotel Nuremberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H93356~Novina Hotel Südwestpark Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84294~TOP Dürer Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H51841~MARITIM Hotel Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H99093~Holiday Inn Nürnberg City Centre, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H141259~Noris Hotel Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H116532~Derag Livinghotel Maximilian, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52036~bfwhotel und Tagungszentrum, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H172950~Mövenpick Hotel Nürnberg-Airport, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H129592~Holiday Inn Express Nürnberg City - Hauptbahnhof, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H403777~Hotel Agneshof - Partner of SORAT Hotels, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84023~Burghotel Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84093~Hotel Erlenstegen, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H135073~Appart Hotel Tassilo, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H85648~Motel One Nürnberg City, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H224954~Arvena Messe Hotel an der NürnbergMesse, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52345~myHOME by Lehrieder, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H345380~Hotel Gerhard, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H123857~Motel One Nürnberg-Plärrer, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H180268~Sorat Hotel Saxx, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52418~InterCityHotel Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H102151~Hotel Alpha, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H50900~Hotel Burgschmiet, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H51381~Ramada Nürnberg Parkhotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H51990~Garden Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84091~Ringhotel Loew´s Merkur, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52642~Ibis Nürnberg City am Plaerrer, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H127620~Novotel Nuernberg Centre Ville, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H429961~Burghotel Stammhaus, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52143~Arvena Park Hotel - Das Hotel am Franken-Center, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H116448~INVITE Hotel Nürnberg City, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H51020~Schindlerhof, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H197247~Advantage Appartement Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H102123~Hotel Avenue, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H131277~Hotel Marienbad, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H127684~Ibis Nürnberg Hauptbahnhof, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H127572~Numberone Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H406279~Hotel am Färbertor, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H281008~art & business Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H52417~Best Living Hotel AROTEL, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84094~Hotel Victoria Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H127587~Novotel Nürnberg Am Messezentrum, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H84298~Hilton Nuremberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H102291~Best Western Hotel Nürnberg am Hauptbahnhof, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H93359~Metropol, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H93339~Leonardo Hotel Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H144871~Park Inn by Radisson Nürnberg, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H110583~Behringers City Hotel, Nuremberg (Nürnberg), Bavaria (Bayern), DEU$H98883";
    }

    private String getSmartCityHotelInChinese() {
        return "smartc§Smartcity Designhotel, 汉诺威 (Hannover, Hanover), 下萨克森 (Niedersachsen), DEU$H325737~Hotel SmartCue52 (昭盛52行館), 台中市, TWN$H341583";
    }
}

