package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.model.HdeRatePlan;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.criterion.HotelSearchCriterion;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.criterion.LocationCriterion;
import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeCurrencyConversionResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapper;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapperImpl;
import de.hotel.android.library.remoteaccess.v30.CreditCardTypeMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapperImpl;
import com.hrsgroup.remoteaccess.hde.v30.v30SoapEnvelope;

import org.jibx.runtime.JiBXException;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * See http://joel-costigliola.github.io/assertj/ for assertj's fluent API
 */
// CS.SUPPRESS ClassFanOutComplexity, Reason: This is a Test.
@SuppressWarnings({"ConstantConditions", "magicnumber"})
public class HdeHotelAdapterImplTest {

    private static final String OTA_HOTEL_AVAIL_RS_GEO_SEARCH_XML = "02_OTA_HotelAvailRS_GeoSearch.xml";
    private static final String OTA_HOTEL_AVAIL_RS_NO_AVAILABILITY_XML = "03_OTA_HotelAvailRS_No_Availability.xml";

    private static final int THIRTEEN = 13;
    private static final int LOCATION_ID_FUERTH = 33743;

    @Test
    public void successfulSearchShouldReturnAvailableHotelsWithRates() throws IOException, JiBXException, ParseException {
        // Arrange
        FileInputStream inputStream = loadXMLFile(OTA_HOTEL_AVAIL_RS_GEO_SEARCH_XML);

        HdeJiBXMarshaller jiBXMarshaller = new HdeJiBXMarshallerImpl();
        HdeHotelAvailResultMapper hdeHotelAvailResultMapper = createHdeHotelAvailResultMapper();

        HotelAvailQuery hotelAvailQuery = createAvailQuery();

        // Act
        v30SoapEnvelope otaAvailRSEnvelope = jiBXMarshaller.unmarshalResponse(inputStream, v30SoapEnvelope.class);
        HotelAvailResult hotelAvailResult = hdeHotelAvailResultMapper.hotelAvailResult(hotelAvailQuery, otaAvailRSEnvelope.getBody().getOtaHotelAvailRS());

        // 13 Hotels
        assertThat(hotelAvailResult.getHotelList()).hasSize(THIRTEEN);

        Hotel firstHotel = hotelAvailResult.getHotelList().get(0);

        // Check first hotel
        assertThat("Hotel Quentin Berlin am Kurfürstendamm").isEqualTo(firstHotel.getName());

        // Check cheapest rate matches expectations
        HdeRatePlan cheapestRatePlan = firstHotel.getHotelOffer().getCheapestRatePlan();
        assertThat(cheapestRatePlan.getTotalAmountAfterTax().equals(new BigDecimal(59.0)));
        assertThat(cheapestRatePlan.getTotalAmountBeforeTax().equals(new BigDecimal(55.14)));
        assertThat(cheapestRatePlan.getNumberOfRequestedRooms() == 1);
        assertThat(cheapestRatePlan.getBookingCode().equals("11|my|nor|125026"));
        assertThat(cheapestRatePlan.getRatePlanDescription().equals("Best Price"));

        // Check cheapest cancelable rate matches expectations
        HdeRatePlan cheapestCancelableRatePlan = firstHotel.getHotelOffer().getCheapestCancelableRatePlan();
        assertThat(cheapestCancelableRatePlan.getTotalAmountAfterTax().equals(new BigDecimal(59.0)));
        assertThat(cheapestCancelableRatePlan.getTotalAmountBeforeTax().equals(new BigDecimal(55.14)));
        assertThat(cheapestCancelableRatePlan.getPaymentInfo().isCancelable());
    }

    @Test
    public void successfulSearchShouldReturnCorrectStartAndFromDates() throws IOException, JiBXException, ParseException {
        // Arrange
        FileInputStream inputStream = loadXMLFile(OTA_HOTEL_AVAIL_RS_GEO_SEARCH_XML);

        HdeJiBXMarshaller jiBXMarshaller = new HdeJiBXMarshallerImpl();
        HdeHotelAvailResultMapper hdeHotelAvailResultMapper = createHdeHotelAvailResultMapper();

        HotelAvailQuery hotelAvailQuery = createAvailQuery();

        // Act
        v30SoapEnvelope otaAvailRSEnvelope = jiBXMarshaller.unmarshalResponse(inputStream, v30SoapEnvelope.class);
        HotelAvailResult hotelAvailResult = hdeHotelAvailResultMapper.hotelAvailResult(hotelAvailQuery, otaAvailRSEnvelope.getBody().getOtaHotelAvailRS());

        // Assert
        assertThat(hotelAvailResult.getFromDate()).isEqualTo(hotelAvailQuery.getHotelAvailCriterion().getFrom());
        assertThat(hotelAvailResult.getToDate()).isEqualTo(hotelAvailQuery.getHotelAvailCriterion().getTo());
    }

    /*
     * Currently does not work. The AvailabilityService needs a mock object of
     * the HttpClient that returns the faked XML stream.
     */
    @Test
    @Ignore
    public void searchShouldReturnErrorsWhenNoHotelsAreAvailable() throws IOException, JiBXException, ParseException {
        // Arrange
        FileInputStream inputStream = loadXMLFile(OTA_HOTEL_AVAIL_RS_NO_AVAILABILITY_XML);

        HdeJiBXMarshaller jiBXMarshaller = new HdeJiBXMarshallerImpl();
        HdeHotelAvailResultMapper hdeHotelAvailResultMapper = createHdeHotelAvailResultMapper();

        HotelAvailQuery hotelAvailQuery = createAvailQuery();

        // Act
        v30SoapEnvelope otaAvailRSEnvelope = jiBXMarshaller.unmarshalResponse(inputStream, v30SoapEnvelope.class);
        HotelAvailResult hotelAvailResult = hdeHotelAvailResultMapper.hotelAvailResult(hotelAvailQuery, otaAvailRSEnvelope.getBody().getOtaHotelAvailRS());

        // Assert
        assertThat(hotelAvailResult.getHotelList()).isEmpty();
    }

    private FileInputStream loadXMLFile(String resource) throws FileNotFoundException {
        URL otaHotelAvailRSUrl = this.getClass().getClassLoader().getResource(resource);
        File otaHotelAvailRSFile = new File(otaHotelAvailRSUrl.getFile());
        return new FileInputStream(otaHotelAvailRSFile);
    }

    private HotelAvailQuery createAvailQuery() throws ParseException {
        HotelAvailQuery hotelAvailQuery = new HotelAvailQuery();
        HotelAvailCriterion hotelAvailCriterion = new HotelAvailCriterion();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date dateFrom = simpleDateFormat.parse("2015-05-03");
        long from = dateFrom.getTime();

        Date dateTo = simpleDateFormat.parse("2015-05-04");
        long to = dateTo.getTime();

        hotelAvailCriterion.setFrom(from);
        hotelAvailCriterion.setTo(to);
        ArrayList<RoomCriterion> roomCriterions = new ArrayList<>();
        RoomCriterion roomCriterion = new RoomCriterion();
        roomCriterion.setAdultCount(1);
        roomCriterion.setRoomType(RoomType.DOUBLE);
        roomCriterions.add(roomCriterion);
        hotelAvailCriterion.setRoomCriterionList(roomCriterions);
        hotelAvailQuery.setHotelAvailCriterion(hotelAvailCriterion);

        HotelSearchCriterion hotelSearchCriterion = new HotelSearchCriterion();
        LocationCriterion locationCriterion = new LocationCriterion();
        locationCriterion.setLocationId(LOCATION_ID_FUERTH);
        hotelSearchCriterion.setLocationCriterion(locationCriterion);
        hotelAvailQuery.setHotelSearchCriterion(hotelSearchCriterion);

        Locale locale = new Locale();
        locale.setIso3Currency("EUR");
        Language language = new Language();
        language.setIso2Language("EN");
        locale.setLanguage(language);
        hotelAvailQuery.setLocale(locale);

        return hotelAvailQuery;
    }

    private HdeHotelAvailResultMapperImpl createHdeHotelAvailResultMapper() {
        return new HdeHotelAvailResultMapperImpl(
                new HdeHotelOffersResultMapperImpl(
                        new HdeCurrencyConversionResultMapperImpl(),
                        new HdeV30CreditCardMapperImpl(new CreditCardTypeMapper())));
    }
}