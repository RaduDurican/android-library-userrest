package com.hrsgroup.util;

import de.hotel.android.library.util.Dates;

import org.junit.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class DatesTest {
    @Test
    public void utcCombinedDateTimeFormatParsing() {
        final String[] unsupportedDateStrings = {
            "2015-11-31",
            "2015-01-02 12:01:02",
            "2015-01-02T12:01"
        };

        final String[] supportedDateStrings = {
            "2015-10-30T12:01:02"
        };
        final Date[] expectedDates = {
            new GregorianCalendar(2015, Calendar.OCTOBER, 30, 12, 1, 2).getTime()
        };

        // Negative cases
        for (String unexpectedDateString : unsupportedDateStrings) {
            try {
                Dates.UTC_COMBINED_DATE_TIME_FORMAT.parse(unexpectedDateString);
                fail("Should not parse " + unexpectedDateString);
            } catch (ParseException e) {
                assertTrue(true);
            }
        }

        // Positive cases
        for (int index = 0; index < supportedDateStrings.length; index++) {
            try {
                final Date parsedDate = Dates.UTC_COMBINED_DATE_TIME_FORMAT.parse(supportedDateStrings[index]);
                assertTrue(parsedDate.equals(expectedDates[index])); // No fluent assertions for dates available
            } catch (ParseException e) {
                fail(e.getMessage());
            }
        }

    }
}
