package de.hotel.android.library.remoteaccess.adapter;

import android.test.InstrumentationTestCase;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hotel.android.library.domain.model.Reservation;
import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.enums.CancelResultCode;
import de.hotel.android.library.domain.model.enums.CancellationType;
import de.hotel.android.library.domain.model.enums.ReservationStatus;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.RequestUnawareSoapService;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CancelReservationRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28CheckReservationStatusRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CancelReservationResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28CheckReservationStatusResultMapper;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HdeReservationManagementAdapterTest extends InstrumentationTestCase {

    // This is needed because dexmaker has some problems without this, so mockito crashes
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    public void testThatReservationsAreReturnedProperly() throws ParseException {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CHECK_RESERVATION_STATUS_RESPONSE);

        // Act
        List<Reservation> reservations = checkReservationAdapter.getReservationList(null);

        // Assert
        assertThat(reservations).hasSize(6);

        assertThat(reservations.get(0).getTotalPrice()).isEqualByComparingTo(new BigDecimal(96.00));
        assertThat(reservations.get(0).getRates().get(0).getRoomtype()).isEqualTo(RoomType.SINGLE);
        assertThat(reservations.get(0).getRates().get(0).getRoomDescription()).contains("Standard single room");
        assertThat(reservations.get(0).getCancellationType()).isEqualTo(CancellationType.WITHOUT_CHARGE_UNTIL_DATE);
        assertThat(reservations.get(0).getHotelName()).isEqualTo("Testhotel.de SUPPORT 02");
        assertThat(reservations.get(0).getHotelPhoneNumber()).isEqualTo("+49/911598320");
        assertThat(reservations.get(0).getReservationNr()).isEqualTo("66508218");
        assertThat(reservations.get(0).getReservationId()).isEqualTo("3C0BD6A6BD9140BFA64AD9BDC10641D4");
        assertThat(reservations.get(0).getCancellationCode()).isEqualTo("3C0BD6A6");
        assertThat(reservations.get(0).getCurrency()).isEqualTo("EUR");
        assertThat(reservations.get(0).getMayTechnicallyBeCancelled()).isFalse();
        assertThat(reservations.get(0).getCancellationPolicy()).isEqualTo("Free cancellation available up to 20/11/2015 at 18:00 (hotel's local time).");
        assertThat(reservations.get(0).getRates().get(0).getRoomCount()).isEqualTo(1);
        assertThat(reservations.get(0).getReservationStatus()).isEqualTo(ReservationStatus.RESERVED);
        assertThat(reservations.get(0).getStarsRating()).isEqualTo(0f);
        assertThat(reservations.get(0).getHotelAddress().getStreet()).isEqualTo("Teststraße 11");
        assertThat(reservations.get(0).getHotelAddress().getPostalCode()).isEqualTo("1111");
        assertThat(reservations.get(0).getHotelAddress().getCity()).isEqualTo("Musterstadt");
        assertThat(reservations.get(0).getHotelAddress().getCountryIsoA3()).isEqualTo("DEU");

        assertThat(reservations.get(0).getGuests().size()).isEqualTo(1);
        assertThat(reservations.get(0).getGuests().get(0).getFirstName()).isEqualTo("Android");
        assertThat(reservations.get(0).getGuests().get(0).getLastName()).isEqualTo("Booker");
        assertThat(reservations.get(0).getGuests().get(0).getEMail()).isEqualTo("ad@hotel.de");


        Date expectedArrival = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("2015-11-20");
        Date expectedDeparture = new SimpleDateFormat("yyyy-MM-dd", Locale.US).parse("2015-11-21");
        assertThat(reservations.get(0).getArrival()).isEqualTo(expectedArrival);
        assertThat(reservations.get(0).getDeparture()).isEqualTo(expectedDeparture);

        assertThat(reservations.get(1).getTotalPrice()).isEqualByComparingTo(new BigDecimal(100.00));
        assertThat(reservations.get(1).getRates().get(0).getRoomtype()).isEqualTo(RoomType.SINGLE);
        assertThat(reservations.get(1).getCancellationType()).isEqualTo(CancellationType.WITHOUT_CHARGE_UNTIL_DATE);

        assertThat(reservations.get(1).getGuests().size()).isEqualTo(3);
        assertThat(reservations.get(1).getGuests().get(0).getFirstName()).isEqualTo("Android_1");
        assertThat(reservations.get(1).getGuests().get(0).getLastName()).isEqualTo("Booker_1");
        assertThat(reservations.get(1).getGuests().get(0).getEMail()).isEqualTo("ad_1@hotel.de");

        assertThat(reservations.get(1).getGuests().get(1).getFirstName()).isEqualTo("Android_2");
        assertThat(reservations.get(1).getGuests().get(1).getLastName()).isEqualTo("Booker_2");
        assertThat(reservations.get(1).getGuests().get(1).getEMail()).isEqualTo("ad_2@hotel.de");

        assertThat(reservations.get(1).getGuests().get(2).getFirstName()).isEqualTo("Android_3");
        assertThat(reservations.get(1).getGuests().get(2).getLastName()).isEqualTo("Booker_3");
        assertThat(reservations.get(1).getGuests().get(2).getEMail()).isEqualTo("ad_3@hotel.de");

        assertThat(reservations.get(2).getTotalPrice()).isEqualByComparingTo(new BigDecimal(100.00));
        assertThat(reservations.get(2).getRates().get(0).getRoomtype()).isEqualTo(RoomType.SINGLE);

        assertThat(reservations.get(3).getTotalPrice()).isEqualByComparingTo(new BigDecimal(70.00));
        assertThat(reservations.get(3).getRates().get(0).getRoomtype()).isEqualTo(RoomType.DOUBLE);
        assertThat(reservations.get(3).getRates().get(0).getRoomDescription()).contains("Standard Doppelzimmer");

        assertThat(reservations.get(4).getTotalPrice()).isEqualByComparingTo(new BigDecimal(850.00));
        assertThat(reservations.get(4).getRates().get(0).getRoomtype()).isEqualTo(RoomType.SINGLE);
        assertThat(reservations.get(4).getMayTechnicallyBeCancelled()).isTrue();

        assertThat(reservations.get(5).getTotalPrice()).isEqualByComparingTo(new BigDecimal(100.00));
        assertThat(reservations.get(5).getRates().get(0).getRoomCount()).isEqualTo(2);
        assertThat(reservations.get(5).getReservationStatus()).isEqualTo(ReservationStatus.CANCELLED);
        assertThat(reservations.get(5).getStarsRating()).isEqualTo(5f);
    }

    public void testThatMultipleRatesAreMappedCorrectly() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CHECK_RESERVATION_STATUS_RESPONSE_MULTIPLE_RATES);

        // Act
        List<Reservation> reservations = checkReservationAdapter.getReservationList(null);

        // Assert
        assertThat(reservations.size()).isEqualTo(1);
        assertThat(reservations.get(0).getRates()).hasSize(2);

        assertThat(reservations.get(0).getRates().get(0).getRoomCount()).isEqualTo(1);
        assertThat(reservations.get(0).getRates().get(0).getRoomtype()).isEqualTo(RoomType.SINGLE);
        assertThat(reservations.get(0).getRates().get(0).getRoomDescription()).isEqualTo("Standard Einzelzimmer");

        assertThat(reservations.get(0).getRates().get(1).getRoomCount()).isEqualTo(2);
        assertThat(reservations.get(0).getRates().get(1).getRoomtype()).isEqualTo(RoomType.SINGLE);
        assertThat(reservations.get(0).getRates().get(1).getRoomDescription()).isEqualTo("Super Mega Überzimmer");

        assertThat(reservations.get(0).getTotalPrice()).isEqualByComparingTo(new BigDecimal(175.00));
    }

    public void testThatAnEmtpyListIsReturned() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CHECK_RESERVATION_STATUS_RESPONSE_NO_RESERVATIONS);

        // Act
        List<Reservation> reservations = checkReservationAdapter.getReservationList(null);

        // Assert
        assertThat(reservations).isEmpty();
    }

    public void testThatACancelableReservationIsHandledProperly() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CANCEL_RESERVATION_RESPONSE);

        // Act
        @CancelResultCode int cancelResultCode = checkReservationAdapter.cancelReservation(null);

        // Assert
        assertThat(cancelResultCode).isEqualTo(CancelResultCode.CANCEL_OK);
    }

    public void testThatAnAlreadyCancelledReservationIsHandledProperly() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CANCEL_RESERVATION_RESPONSE_ALREADY_CANCELLED);

        // Act
        @CancelResultCode int cancelResultCode = checkReservationAdapter.cancelReservation(null);

        // Assert
        assertThat(cancelResultCode).isEqualTo(CancelResultCode.ALREADY_CANCELLED);
    }
    
    public void testThatNotCancelableReservationsAreHandledProperly() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = createReservationManagementAdapter(Paths.CANCEL_RESERVATION_RESPONSE_NOT_CANCELABLE_RESERVATION);

        // Act
        @CancelResultCode int cancelResultCode = checkReservationAdapter.cancelReservation(null);

        // Assert
        assertThat(cancelResultCode).isEqualTo(CancelResultCode.RATE_NOT_CANCELABLE);
    }

    private HdeReservationManagementAdapterImpl createReservationManagementAdapter(String file) {
        return new HdeReservationManagementAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV28CheckReservationStatusRequestMapper.class),
                new HdeV28CheckReservationStatusResultMapper(),
                Mockito.mock(HdeV28CancelReservationRequestMapper.class),
                new HdeV28CancelReservationResultMapper());
    }
}
