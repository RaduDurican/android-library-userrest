package de.hotel.android.library.util;

import junit.framework.TestCase;

public class DatesTest extends TestCase {
    public void testDatesDays() {
        long now = System.currentTimeMillis();
        long nowInOneDay = now + Dates.ONE_DAY_IN_MILLISECONDS; // 86400000
        long nowInOneDayAndAMinute = nowInOneDay + 60000;
        long nowSixHoursLater = now + 21600000;

        assertEquals(0, Dates.days(now, now));
        assertEquals(1, Dates.days(now, nowInOneDay));
        assertEquals(0, Dates.days(nowInOneDay, nowInOneDayAndAMinute));
        assertEquals(1, Dates.days(now, nowInOneDayAndAMinute));
        assertEquals(-1, Dates.days(nowInOneDay, now));
        assertEquals(0, Dates.days(now, nowSixHoursLater));
        assertEquals(0, Dates.days(nowSixHoursLater, now));
    }
}
