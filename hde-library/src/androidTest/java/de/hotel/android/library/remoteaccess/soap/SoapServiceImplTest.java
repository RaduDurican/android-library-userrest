package de.hotel.android.library.remoteaccess.soap;

import android.support.annotation.NonNull;
import android.test.InstrumentationTestCase;

import com.hrsgroup.remoteaccess.hde.v30.v30SoapEnvelope;

import org.jibx.runtime.JiBXException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.TwoTimesFailingSoapClient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"magicnumber", "javadoctype"})
public class SoapServiceImplTest extends InstrumentationTestCase {

    // This is needed because dexmaker has some problems without this, so mockito crashes
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    public void testThatSoapServiceRetriesRequests() throws IOException, JiBXException {
        // Arrange
        final int expectedCalls = 2;

        HdeJiBXMarshaller jiBXMarshallerMock = createHdeJiBXMarshallerMock();
        TwoTimesFailingSoapClient twoTimesFailingSoapClient = new TwoTimesFailingSoapClient(Paths.HOTEL_AVAIL_RESPONSE_WITHOUT_PAGING, expectedCalls);
        SoapServiceImpl soapService = new SoapServiceImpl(jiBXMarshallerMock, Mockito.mock(HdeV30SOAPRequestBuilderImpl.class), twoTimesFailingSoapClient);

        // Act
        v30SoapEnvelope soapEnvelope = soapService.performRequest(null, v30SoapEnvelope.class, null, true);

        // Assert
        assertThat(twoTimesFailingSoapClient.getActualCalls()).isEqualTo(expectedCalls);
        assertThat(soapEnvelope).isNotNull();
    }

    public void testThatSoapServiceDoesNotRetryRequests() throws IOException, JiBXException {
        // Arrange
        final int maximumCalls = 2; // Maximum the soap client should retry
        final int expectedCalls = 1; // Actually should not retry because we pass false into performRequest

        HdeJiBXMarshaller jiBXMarshallerMock = createHdeJiBXMarshallerMock();
        TwoTimesFailingSoapClient twoTimesFailingSoapClient = new TwoTimesFailingSoapClient(Paths.HOTEL_AVAIL_RESPONSE_WITHOUT_PAGING, maximumCalls);
        SoapServiceImpl soapService = new SoapServiceImpl(jiBXMarshallerMock, Mockito.mock(HdeV30SOAPRequestBuilderImpl.class), twoTimesFailingSoapClient);

        // Act
        try {
            soapService.performRequest(null, v30SoapEnvelope.class, null, false);
        } catch (Exception e) {
            // Blank
        }

        // Assert
        assertThat(twoTimesFailingSoapClient.getActualCalls()).isEqualTo(expectedCalls);
    }


    private HdeJiBXMarshaller createHdeJiBXMarshallerMock() throws JiBXException, java.io.UnsupportedEncodingException {
        HdeJiBXMarshaller jiBXMarshallerMock = Mockito.mock(HdeJiBXMarshaller.class);
        when(jiBXMarshallerMock.marshalRequest(Mockito.any())).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return new OutputStream() {
                    @Override
                    public void write(int i) throws IOException {

                    }
                };
            }
        });
        when(jiBXMarshallerMock.unmarshalResponse(Mockito.any(InputStream.class), Mockito.any(Class.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                HdeJiBXMarshaller hdeJiBXMarshaller = new HdeJiBXMarshallerImpl();
                return hdeJiBXMarshaller.unmarshalResponse((InputStream) invocationOnMock.getArguments()[0], (Class<? extends Object>) invocationOnMock.getArguments()[1]);
            }
        });
        return jiBXMarshallerMock;
    }
}
