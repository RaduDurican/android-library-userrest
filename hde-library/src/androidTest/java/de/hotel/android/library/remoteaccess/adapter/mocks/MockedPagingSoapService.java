package de.hotel.android.library.remoteaccess.adapter.mocks;

import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.soap.SoapService;

import junit.framework.Assert;

import org.jibx.runtime.JiBXException;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class MockedPagingSoapService implements SoapService {
    private static final String PARAM_MAX_RESPONSES = "MaxResponses=\"%d\"";
    private static final String PARAM_SEQUENCE_NUMBER = "SequenceNmbr=\"%d\"";

    private HdeJiBXMarshaller hdeJiBXMarshaller = new HdeJiBXMarshallerImpl();
    private InputStream response0;
    private InputStream response2;
    private int pageSize;

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public MockedPagingSoapService() {
        try {
            response0 = loadXMLFile(Paths.HOTEL_AVAIL_RESPONSE_PAGED_0);
            response2 = loadXMLFile(Paths.HOTEL_AVAIL_RESPONSE_PAGED_2);
        } catch (FileNotFoundException e) {
            Assert.fail("Could not load XML response file");
        }
    }

    @Override
    public <T, S> S performRequest(T request, Class<S> type, String soapAction, boolean retryOnFailure) {
        try {
            String marshalledRequest = hdeJiBXMarshaller.marshalRequest(request).toString();

            if (!marshalledRequest.contains(String.format(PARAM_MAX_RESPONSES, pageSize))) {
                throw new IllegalArgumentException("Missing MaxResponses attribute");
            }
            if (marshalledRequest.contains(String.format(PARAM_SEQUENCE_NUMBER, 0))) {
                return hdeJiBXMarshaller.unmarshalResponse(response0, type);
            }
            if (marshalledRequest.contains(String.format(PARAM_SEQUENCE_NUMBER, 2))) {
                return hdeJiBXMarshaller.unmarshalResponse(response2, type);
            }
        } catch (JiBXException | UnsupportedEncodingException e) {
            Assert.fail("JiBX exception. Exception + " + e);
        }

        Assert.fail("Request does not contain sequence number");
        return null;
    }

    private InputStream loadXMLFile(String resource) throws FileNotFoundException {
        return this.getClass().getClassLoader().getResourceAsStream(resource);
    }
}
