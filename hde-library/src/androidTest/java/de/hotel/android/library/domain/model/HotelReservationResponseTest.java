package de.hotel.android.library.domain.model;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import de.hotel.android.library.domain.model.data.Address;
import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.data.Distance;
import de.hotel.android.library.domain.model.data.GeoPosition;
import de.hotel.android.library.domain.model.enums.AdditionalDetailsType;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.enums.HdeGuaranteeType;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.domain.model.enums.RoomDescriptionType;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class HotelReservationResponseTest extends TestCase {

    public static final String RESERVATION_ID = "ResId123";
    public static final String RESERVATION_NR = "ResNr321";
    public static final String CANCELLATION_CODE = "CancelCode132";
    public static final String UNIQUE_ID = "UniqueId123";
    public static final String BOOKING_CUSTOMER_FIRST_NAME = "test first name";
    public static final String BOOKING_CUSTOMER_LAST_NAME = "test last name";
    public static final String BOOKING_CUSTOMER_EMAIL = "test email";
    public static final String BOOKING_CUSTOMER_PHONE = "test phone";
    public static final String BOOKING_CUSTOMER_COUNTRY = "test country iso a3";
    public static final String BOOKING_CUSTOMER_STREET = "test street";
    public static final String BOOKING_CUSTOMER_POSTAL_CODE = "test postal code";
    public static final String BOOKING_CUSTOMER_CITY = "test city";
    public static final double LATITUDE = 49.4321;
    public static final double LONGITUDE = 11.1234;
    public static final String HOTEL_NAME = "test hotel";
    public static final String HOTEL_ID = "hotel id";
    public static final String HOTEL_PICTURE_URI = "http://www.test.de";
    public static final float USER_RATING = 2.6f;
    public static final String HOTEL_CITY = "hotel test city";
    public static final String HOTEL_COUNTRY_ISO_A3 = "hotel test country iso a3";
    public static final String HOTEL_STREET = "hotel test street";
    public static final String HOTEL_POSTAL_CODE = "hotel test postal code";
    public static final String TRAVELER_FIRST_NAME = "test traveler first name";
    public static final String TRAVELER_LAST_NAME = "test traveler last name";
    public static final String TRAVELER_EMAIL = "test traveler email";
    public static final String TRAVELER_PHONE = "test traveler phone";
    public static final int CREDIT_CARD_MONTH = 07;
    public static final int CREDIT_CARD_YEAR = 2022;
    public static final String CREDIT_CARD_NAME = "Credit card name";
    public static final String CREDIT_CARD_NUMBER = "4111111111111111";
    public static final int CREDIT_CARD_CVC = 456;
    public static final float DISTANCE_IN_KM = 3.5f;
    public static final int LOCATION_TYPE = LocationType.CITY;
    public static final int CREDIT_CARD_TYPE = CreditCardType.DINERS_CLUB;
    public static final double HOTEL_LATITUDE = 48.12321;
    public static final double HOTEL_LONGITUDE = 11.55446;
    public static final double AVERAGE_BREAKFAST_PRICE_HOTEL = 11.1;
    public static final double TOTAL_ROOM_PRICE_GROSS_CUSTOMER = 12.2;
    public static final double TOTAL_ROOM_PRICE_GROSS_HOTEL = 13.3;
    public static final double TOTAL_ROOM_PRICE_NET_CUSTOMER = 14.4;
    public static final double TOTAL_ROOM_PRICE_NET_HOTEL = 15.5;
    public static final String DETAILS_DESCRIPTION = "Breakfast";
    public static final @AdditionalDetailsType int DETAILS_TYPE = AdditionalDetailsType.BREAKFAST;
    public static final String DETAILS_CURRENCY_CODE = "EUR";
    public static final BigDecimal DETAILS_AMMOUNT = new BigDecimal(15.0);
    public static final boolean BREAKFAST_INCLUSIVE = true;
    public static final boolean CANCELABLE = true;
    public static final Date CANCELLATION_DEADLINE = new GregorianCalendar(2010, 05, 19).getTime();
    public static final int GUARANTEE_TYPE = HdeGuaranteeType.GUARANTEE_REQUIRED;
    public static final String ISO_3_CURRENCY_CUSTOMER = "EUR";
    public static final String ISO_3_CURRENCY_HOTEL = "USD";
    public static final boolean RESTRICTED = true;
    public static final String CURRENCY_CODE_DATE_OFFER = "EUR";
    public static final String EFFECTIVE_DATE = "2020-09-18";
    public static final String EXPIRE_DATE = "20120-12-30";
    public static final String RATE_PLAN_DESCRIPTION = "RatePlanDescription";
    public static final String ROOM_DESCRIPTION = "Room Description 1";
    public static final String ROOM_DESCRIPTION_SHORT = "Room Description Short";
    public static final String IMAGE = "Image";
    public static final int ROOM_ENHANCEMENT_TYPE = RoomDescriptionType.BALCONY;
    public static final double AMOUNT_AFTER_TAX = 77.7;
    public static final double AMOUNT_BEFORE_TAX = 99.9;

    public void testParcelShouldReturnAllValuesCorrectly() {
        // Arrange
        HotelReservationResponse response = createFakeHotelReservationResponse();
        Parcel parcel = Parcel.obtain();

        // Act
        parcel.writeParcelable(response, Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
        parcel.setDataPosition(0);
        HotelReservationResponse parcelResponse = parcel.readParcelable(HotelReservationResponse.class.getClassLoader());

        // Assert
        assertThat(parcelResponse.getReservationId()).isEqualTo(RESERVATION_ID);
        assertThat(parcelResponse.getReservationNumber()).isEqualTo(RESERVATION_NR);
        assertThat(parcelResponse.getCancellationCode()).isEqualTo(CANCELLATION_CODE);
        assertThat(parcelResponse.getUniqueId()).isEqualTo(UNIQUE_ID);

        Customer bookingCustomer = parcelResponse.getBookingCustomer();
        assertThat(bookingCustomer.getFirstname()).isEqualTo(BOOKING_CUSTOMER_FIRST_NAME);
        assertThat(bookingCustomer.getLastname()).isEqualTo(BOOKING_CUSTOMER_LAST_NAME);
        assertThat(bookingCustomer.getEmail()).isEqualTo(BOOKING_CUSTOMER_EMAIL);
        assertThat(bookingCustomer.getPhone()).isEqualTo(BOOKING_CUSTOMER_PHONE);

        Address address = bookingCustomer.getAddress();
        assertThat(address.getStreet()).isEqualTo(BOOKING_CUSTOMER_STREET);
        assertThat(address.getPostalCode()).isEqualTo(BOOKING_CUSTOMER_POSTAL_CODE);
        assertThat(address.getCity()).isEqualTo(BOOKING_CUSTOMER_CITY);
        assertThat(address.getCountryIsoA3()).isEqualTo(BOOKING_CUSTOMER_COUNTRY);
        assertThat(address.getGeoPosition().getLatitude()).isEqualTo(LATITUDE);
        assertThat(address.getGeoPosition().getLongitude()).isEqualTo(LONGITUDE);

        Hotel hotel = parcelResponse.getHotels().get(0);
        HotelOffer hotelOffer = hotel.getHotelOffer();

        assertThat(hotel.getName()).isEqualTo(HOTEL_NAME);
        assertThat(hotel.getHotelId()).isEqualTo(HOTEL_ID);
        assertThat(hotel.getStarsRating()).isEqualTo(-1f);
        assertThat(hotel.getUserRating()).isEqualTo(USER_RATING);
        assertThat(hotel.getPicture().toString()).isEqualTo(HOTEL_PICTURE_URI);

        Address hotelAddress = hotel.getAddress();
        assertThat(hotelAddress.getStreet()).isEqualTo(HOTEL_STREET);
        assertThat(hotelAddress.getPostalCode()).isEqualTo(HOTEL_POSTAL_CODE);
        assertThat(hotelAddress.getCity()).isEqualTo(HOTEL_CITY);
        assertThat(hotelAddress.getCountryIsoA3()).isEqualTo(HOTEL_COUNTRY_ISO_A3);
        assertThat(hotelAddress.getGeoPosition().getLatitude()).isEqualTo(HOTEL_LATITUDE);
        assertThat(hotelAddress.getGeoPosition().getLongitude()).isEqualTo(HOTEL_LONGITUDE);

        Distance distance = hotel.getDistances().get(0);
        assertThat(distance.getDistanceInKm()).isEqualTo(DISTANCE_IN_KM);
        assertThat(distance.getLocationType()).isEqualTo(LOCATION_TYPE);

        CreditCard creditCard = parcelResponse.getCreditCard();
        assertThat(creditCard.getCreditCardType()).isEqualTo(CREDIT_CARD_TYPE);
        assertThat(creditCard.getMonth()).isEqualTo(CREDIT_CARD_MONTH);
        assertThat(creditCard.getYear()).isEqualTo(CREDIT_CARD_YEAR);
        assertThat(creditCard.getNumber()).isEqualTo(CREDIT_CARD_NUMBER);
        assertThat(creditCard.getName()).isEqualTo(CREDIT_CARD_NAME);
        assertThat(creditCard.getCvc()).isEqualTo(CREDIT_CARD_CVC);

        Customer customer = parcelResponse.getTravelers().get(0);
        assertThat(customer.getFirstname()).isEqualTo(TRAVELER_FIRST_NAME);
        assertThat(customer.getLastname()).isEqualTo(TRAVELER_LAST_NAME);
        assertThat(customer.getEmail()).isEqualTo(TRAVELER_EMAIL);
        assertThat(customer.getPhone()).isEqualTo(TRAVELER_PHONE);

        List<HdeRatePlan> ratePlans = hotelOffer.getRatePlans();
        Assert.assertNotNull(ratePlans);
        Assert.assertFalse(ratePlans.isEmpty());

        HdeRatePlan ratePlan = ratePlans.get(0);
        Assert.assertNotNull(ratePlan);

        List<String> ratePlanDescriptions = ratePlan.getRatePlanDescriptions();
        Assert.assertNotNull(ratePlans);
        Assert.assertFalse(ratePlans.isEmpty());
        assertThat(ratePlanDescriptions.get(0)).isEqualTo(RATE_PLAN_DESCRIPTION);

        //check payment info
        HdePaymentInfo paymentInfo = ratePlan.getPaymentInfo();
        Assert.assertNotNull(paymentInfo);
        assertThat(paymentInfo.isBreakFastIncluded()).isEqualTo(BREAKFAST_INCLUSIVE);
        assertThat(paymentInfo.isCancelable()).isEqualTo(CANCELABLE);
        assertThat(paymentInfo.getCancellationDeadline()).isEqualTo(CANCELLATION_DEADLINE);

        List<HdeAdditionalDetails> additionalDetailsList = paymentInfo.getAdditionalDetailList();
        Assert.assertNotNull(additionalDetailsList);
        Assert.assertFalse(additionalDetailsList.isEmpty());

        HdeAdditionalDetails additionalDetails = additionalDetailsList.get(0);
        Assert.assertNotNull(additionalDetails);

        List<String> descriptions = additionalDetails.getDescriptions();
        Assert.assertNotNull(descriptions);
        Assert.assertFalse(descriptions.isEmpty());
        assertThat(descriptions.get(0)).isEqualTo(DETAILS_DESCRIPTION);

        assertThat(additionalDetails.getDetailsType()).isEqualTo(DETAILS_TYPE);
        assertThat(additionalDetails.getCurrencyCode()).isEqualTo(DETAILS_CURRENCY_CODE);
        assertThat(additionalDetails.getAmount().compareTo(DETAILS_AMMOUNT) == 0).isTrue();

        //check single roomRate
        HdeRoomRate roomRate = ratePlan.getRoomRate();
        Assert.assertNotNull(roomRate);
        assertThat(roomRate.getIso3CurrencyCustomer()).isEqualTo(ISO_3_CURRENCY_CUSTOMER);
        assertThat(roomRate.getIso3CurrencyHotel()).isEqualTo(ISO_3_CURRENCY_HOTEL);
        assertThat(roomRate.getTotalRoomPriceGrossCustomer().compareTo((new BigDecimal(TOTAL_ROOM_PRICE_GROSS_CUSTOMER))) == 0).isTrue();
        assertThat(roomRate.getTotalRoomPriceGrossHotel().compareTo((new BigDecimal(TOTAL_ROOM_PRICE_GROSS_HOTEL))) == 0).isTrue();
        assertThat(roomRate.getTotalRoomPriceNetCustomer().compareTo((new BigDecimal(TOTAL_ROOM_PRICE_NET_CUSTOMER))) == 0).isTrue();
        assertThat(roomRate.getTotalRoomPriceNetHotel().compareTo((new BigDecimal(TOTAL_ROOM_PRICE_NET_HOTEL))) == 0).isTrue();

        //check room descriptions
        RoomDescription roomDescription = roomRate.getRoomDescription();
        assertThat(roomDescription).isNotNull();
        assertThat(roomDescription.getDescriptionShort()).isEqualTo(ROOM_DESCRIPTION_SHORT);
        assertThat(roomDescription.getImageList().get(0)).isEqualTo(IMAGE);
        assertThat(roomDescription.getRoomDescriptions().get(0)).isEqualTo(ROOM_DESCRIPTION);
        assertThat(roomDescription.getRoomDescriptionType()).isEqualTo(ROOM_ENHANCEMENT_TYPE);


        //check single rate
        List<HdeRate> rates = roomRate.getHdeRates();
        Assert.assertNotNull(rates);
        Assert.assertFalse(rates.isEmpty());
        HdeRate rate = rates.get(0);
        Assert.assertNotNull(rate);
        assertThat(rate.getAmountAfterTax().compareTo((new BigDecimal(AMOUNT_AFTER_TAX))) == 0).isTrue();
        assertThat(rate.getAmountBeforeTax().compareTo((new BigDecimal(AMOUNT_BEFORE_TAX))) == 0).isTrue();
        assertThat(rate.getCurrencyCode()).isEqualTo(CURRENCY_CODE_DATE_OFFER);
        assertThat(rate.getEffectiveDate()).isEqualTo(EFFECTIVE_DATE);
        assertThat(rate.getExpireDate()).isEqualTo(EXPIRE_DATE);
    }

    private HotelReservationResponse createFakeHotelReservationResponse() {
        HotelReservationResponse response = new HotelReservationResponse();

        response.setReservationId(RESERVATION_ID);
        response.setReservationNumber(RESERVATION_NR);
        response.setCancellationCode(CANCELLATION_CODE);
        response.setUniqueId(UNIQUE_ID);

        Customer bookingCustomer = new Customer();
        bookingCustomer.setFirstname(BOOKING_CUSTOMER_FIRST_NAME);
        bookingCustomer.setLastname(BOOKING_CUSTOMER_LAST_NAME);
        bookingCustomer.setEmail(BOOKING_CUSTOMER_EMAIL);
        bookingCustomer.setPhone(BOOKING_CUSTOMER_PHONE);

        Address customerAddress = new Address();
        customerAddress.setStreet(BOOKING_CUSTOMER_STREET);
        customerAddress.setPostalCode(BOOKING_CUSTOMER_POSTAL_CODE);
        customerAddress.setCity(BOOKING_CUSTOMER_CITY);
        customerAddress.setCountryIsoA3(BOOKING_CUSTOMER_COUNTRY);

        GeoPosition geoPosition = new GeoPosition();
        geoPosition.setLatitude(LATITUDE);
        geoPosition.setLongitude(LONGITUDE);
        customerAddress.setGeoPosition(geoPosition);

        bookingCustomer.setAddress(customerAddress);
        response.setBookingCustomer(bookingCustomer);

        ArrayList<Hotel> hotels = new ArrayList<>();
        Hotel hotel = new Hotel();
        HotelOffer hotelOffer = new HotelOffer();
        hotel.setName(HOTEL_NAME);
        hotel.setHotelId(HOTEL_ID);
        hotel.setPicture(Uri.parse(HOTEL_PICTURE_URI));
        hotel.setStarsRating(-1f);
        hotel.setUserRating(USER_RATING);

        Address hotelAddress = new Address();
        hotelAddress.setCity(HOTEL_CITY);
        hotelAddress.setCountryIsoA3(HOTEL_COUNTRY_ISO_A3);
        hotelAddress.setStreet(HOTEL_STREET);
        hotelAddress.setPostalCode(HOTEL_POSTAL_CODE);

        GeoPosition hotelGeoPosition = new GeoPosition();
        hotelGeoPosition.setLatitude(HOTEL_LATITUDE);
        hotelGeoPosition.setLongitude(HOTEL_LONGITUDE);
        hotelAddress.setGeoPosition(hotelGeoPosition);
        hotel.setAddress(hotelAddress);

        ArrayList<Distance> distances = new ArrayList<>();
        Distance distance = new Distance();
        distance.setDistanceInKm(DISTANCE_IN_KM);
        distance.setLocationType(LOCATION_TYPE);
        distances.add(distance);
        hotel.setDistances(distances);

        hotel.setHotelOffer(hotelOffer);
        hotels.add(hotel);
        response.setHotels(hotels);

        ArrayList<Customer> travelers = new ArrayList<>();
        Customer traveler = new Customer();
        traveler.setFirstname(TRAVELER_FIRST_NAME);
        traveler.setLastname(TRAVELER_LAST_NAME);
        traveler.setEmail(TRAVELER_EMAIL);
        traveler.setPhone(TRAVELER_PHONE);
        travelers.add(traveler);
        response.setTravelers(travelers);

        CreditCard creditCard = new CreditCard();
        creditCard.setCreditCardType(CREDIT_CARD_TYPE);
        creditCard.setMonth(CREDIT_CARD_MONTH);
        creditCard.setYear(CREDIT_CARD_YEAR);
        creditCard.setName(CREDIT_CARD_NAME);
        creditCard.setNumber(CREDIT_CARD_NUMBER);
        creditCard.setCvc(CREDIT_CARD_CVC);
        response.setCreditCard(creditCard);


        List<HdeRatePlan> ratePlans = new ArrayList<>();
        HdeRatePlan ratePlan = new HdeRatePlan();

        ArrayList<String> ratePlanDescriptions = new ArrayList<>();
        ratePlanDescriptions.add(RATE_PLAN_DESCRIPTION);
        ratePlan.setRatePlanDescriptions(ratePlanDescriptions);

        HdePaymentInfo paymentInfo = new HdePaymentInfo();
        paymentInfo.setCancelable(CANCELABLE);
        paymentInfo.setCancellationDeadline(CANCELLATION_DEADLINE);
        paymentInfo.setBreakFastIncluded(BREAKFAST_INCLUSIVE);
        paymentInfo.setGuaranteeType(GUARANTEE_TYPE);

        List<HdeAdditionalDetails> additionalDetailsList = new ArrayList<>();
        HdeAdditionalDetails hdeAdditionalDetails = new HdeAdditionalDetails();

        hdeAdditionalDetails.setDetailsType(AdditionalDetailsType.BREAKFAST);
        hdeAdditionalDetails.setAmount(DETAILS_AMMOUNT);
        hdeAdditionalDetails.setCurrencyCode(DETAILS_CURRENCY_CODE);

        List<String> descriptions = new ArrayList<>();
        descriptions.add(DETAILS_DESCRIPTION);
        hdeAdditionalDetails.setDescriptions(descriptions);

        additionalDetailsList.add(hdeAdditionalDetails);

        paymentInfo.setAdditionalDetailList(additionalDetailsList);

        ratePlan.setPaymentInfo(paymentInfo);

        ArrayList<HdeRate> rates = new ArrayList<>();
        HdeRate rate = new HdeRate();
        rate.setAmountAfterTax(new BigDecimal(AMOUNT_AFTER_TAX));
        rate.setAmountBeforeTax(new BigDecimal(AMOUNT_BEFORE_TAX));
        rate.setCurrencyCode(CURRENCY_CODE_DATE_OFFER);
        rate.setEffectiveDate(EFFECTIVE_DATE);
        rate.setExpireDate(EXPIRE_DATE);
        rates.add(rate);

        HdeRoomRate roomRate = new HdeRoomRate();
        roomRate.setHdeRates(rates);

        roomRate.setIso3CurrencyCustomer(ISO_3_CURRENCY_CUSTOMER);
        roomRate.setIso3CurrencyHotel(ISO_3_CURRENCY_HOTEL);

        RoomDescription roomDescription = new RoomDescription();
        ArrayList<String> roomDescriptions = new ArrayList<>();
        roomDescriptions.add(ROOM_DESCRIPTION);
        roomDescription.setDescriptionLong(roomDescriptions);
        roomDescription.setDescriptionShort(ROOM_DESCRIPTION_SHORT);
        ArrayList<String> imageList = new ArrayList<>();
        imageList.add(IMAGE);
        roomDescription.setImageList(imageList);
        roomDescription.setRoomDescriptionType(ROOM_ENHANCEMENT_TYPE);
        roomRate.setRoomDescription(roomDescription);
        roomRate.setTotalRoomPriceGrossCustomer(new BigDecimal(TOTAL_ROOM_PRICE_GROSS_CUSTOMER));
        roomRate.setTotalRoomPriceGrossHotel(new BigDecimal(TOTAL_ROOM_PRICE_GROSS_HOTEL));
        roomRate.setTotalRoomPriceNetCustomer(new BigDecimal(TOTAL_ROOM_PRICE_NET_CUSTOMER));
        roomRate.setTotalRoomPriceNetHotel(new BigDecimal(TOTAL_ROOM_PRICE_NET_HOTEL));

        ratePlan.setRoomRate(roomRate);
        ratePlans.add(ratePlan);
        hotelOffer.setRatePlans(ratePlans);

        return response;
    }
}
