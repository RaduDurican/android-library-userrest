package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import junit.framework.TestCase;

import java.util.List;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Location;
import de.hotel.android.library.domain.model.enums.LocationType;
import de.hotel.android.library.domain.model.query.GetLocationsQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
@SuppressWarnings({"magicnumber", "javadoctype"})
public class HdeLocationAdapterIntegrationTest extends TestCase {

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe(); // Pass your authentication here in order to run this test successfully
    }

    public void testThatCorrectPropertiesAreReturned() {
        // Arrange
        HdeLocationAdapter getLocationsAdapter = hde.createGetLocationsAdapter(RemoteAccessTargetEnvironmentType.BETA);

        Language language = new Language();
        language.setIso2Language("en");

        // Act
        Location location = getLocationsAdapter.getLocationById(39020, language);

        // Assert
        assertThat(location).isNotNull();
        assertThat(location.getLocationId().intValue()).isEqualTo(39020);
        assertThat(location.getLocationName()).isEqualTo("Nuremberg");
        assertThat(location.getLocationType()).isEqualTo(LocationType.CITY);
        assertThat(location.getGeoPosition().getLatitude()).isEqualTo(49.4540471761304);
        assertThat(location.getGeoPosition().getLongitude()).isEqualTo(11.077154159284);
        assertThat(location.getIsoA3Country()).isEqualTo("DEU");
        assertThat(location.getRegionName()).isEqualTo("Bavaria");
    }

    public void testThatTheCorrectCityByAirportCodeIsReturned() {
        // Arrange
        HdeLocationAdapter getLocationsAdapter = hde.createGetLocationsAdapter(RemoteAccessTargetEnvironmentType.BETA);

        GetLocationsQuery request = new GetLocationsQuery();
        request.setAirportCode("VIE");
        Language language = new Language();
        language.setIso2Language("de");
        request.setLanguage(language);

        // Act
        List<Location> locations = getLocationsAdapter.getLocations(request);

        // Assert
        assertThat(locations).isNotNull();
        assertThat(locations).isNotEmpty();
        assertThat(locations.get(0).getLocationId().intValue()).isEqualTo(46355);
        assertThat(locations.get(0).getLocationName()).isEqualTo("Wien");
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.CITY);
        assertThat(locations.get(0).getGeoPosition().getLatitude()).isEqualTo(48.20924803066188);
        assertThat(locations.get(0).getGeoPosition().getLongitude()).isEqualTo(16.36966609963565);
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("AUT");
        assertThat(locations.get(0).getRegionName()).isNull();
    }

    public void testThatTheCorrectCityByNameIsReturned() {
        // Arrange
        HdeLocationAdapter getLocationsAdapter = hde.createGetLocationsAdapter(RemoteAccessTargetEnvironmentType.BETA);

        GetLocationsQuery request = new GetLocationsQuery();
        request.setDestination("Wien");
        Language language = new Language();
        language.setIso2Language("de");
        request.setLanguage(language);

        // Act
        List<Location> locations = getLocationsAdapter.getLocations(request);

        // Assert
        assertThat(locations).isNotNull();
        assertThat(locations).isNotEmpty();
        assertThat(locations.get(0).getLocationId().intValue()).isEqualTo(46355);
        assertThat(locations.get(0).getLocationName()).isEqualTo("Wien");
        assertThat(locations.get(0).getLocationType()).isEqualTo(LocationType.CITY);
        assertThat(locations.get(0).getGeoPosition().getLatitude()).isEqualTo(48.20924803066188);
        assertThat(locations.get(0).getGeoPosition().getLongitude()).isEqualTo(16.36966609963565);
        assertThat(locations.get(0).getIsoA3Country()).isEqualTo("AUT");
        assertThat(locations.get(0).getRegionName()).isNull();
    }

    public void testThatTheCorrectCityByCountryCodeIsReturned() {
        // Arrange
        HdeLocationAdapter getLocationsAdapter = hde.createGetLocationsAdapter(RemoteAccessTargetEnvironmentType.BETA);

        Language language = new Language();
        language.setIso2Language("en");

        GetLocationsQuery requestUSA = new GetLocationsQuery();
        requestUSA.setDestination("Vienna");
        requestUSA.setCountryIsoA3("USA");
        requestUSA.setLanguage(language);

        GetLocationsQuery requestAUT = new GetLocationsQuery();
        requestAUT.setDestination("Vienna");
        requestAUT.setCountryIsoA3("AUT");
        requestAUT.setLanguage(language);

        // Act
        List<Location> locationsUSA = getLocationsAdapter.getLocations(requestUSA);
        List<Location> locationsAUT = getLocationsAdapter.getLocations(requestAUT);

        // Assert
        assertThat(locationsUSA).isNotNull();
        assertThat(locationsUSA).isNotEmpty();
        assertThat(locationsUSA.get(0).getLocationId().intValue()).isEqualTo(27732);
        assertThat(locationsUSA.get(0).getLocationName()).isEqualTo("Vienna");
        assertThat(locationsUSA.get(0).getLocationType()).isEqualTo(LocationType.CITY);
        assertThat(locationsUSA.get(0).getGeoPosition().getLatitude()).isEqualTo(38.89950549564985);
        assertThat(locationsUSA.get(0).getGeoPosition().getLongitude()).isEqualTo(-77.26774406502955);
        assertThat(locationsUSA.get(0).getIsoA3Country()).isEqualTo("USA");
        assertThat(locationsUSA.get(0).getRegionName()).isEqualTo("Virginia");

        assertThat(locationsAUT).isNotNull();
        assertThat(locationsAUT).isNotEmpty();
        assertThat(locationsAUT.get(0).getLocationId().intValue()).isEqualTo(46355);
        assertThat(locationsAUT.get(0).getLocationName()).isEqualTo("Vienna");
        assertThat(locationsAUT.get(0).getLocationType()).isEqualTo(LocationType.CITY);
        assertThat(locationsAUT.get(0).getGeoPosition().getLatitude()).isEqualTo(48.20924803066188);
        assertThat(locationsAUT.get(0).getGeoPosition().getLongitude()).isEqualTo(16.36966609963565);
        assertThat(locationsAUT.get(0).getIsoA3Country()).isEqualTo("AUT");
        assertThat(locationsAUT.get(0).getRegionName()).isNull();
    }
}