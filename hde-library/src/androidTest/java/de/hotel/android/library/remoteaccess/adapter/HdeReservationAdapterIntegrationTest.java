package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import junit.framework.TestCase;

import java.util.ArrayList;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.adapter.ReservationAdapter;
import de.hotel.android.library.domain.model.HotelReservationResponse;
import de.hotel.android.library.domain.model.data.CreditCard;
import de.hotel.android.library.domain.model.data.Customer;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.enums.CreditCardType;
import de.hotel.android.library.domain.model.query.HotelReservationQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.util.Dates;

import static org.assertj.core.api.Assertions.*;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
public class HdeReservationAdapterIntegrationTest extends TestCase {
    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe(); // Insert a real username and a real password for debugging purposes
    }

    public void testBookShouldReturnSuccess() {
        // Arrange
        ReservationAdapter reservationAdapter = hde.createReservationAdapter(RemoteAccessTargetEnvironmentType.BETA);

        Language language = new Language();
        language.setIso2Language("en");
        language.setVariantIso2Country("us");

        Locale locale = new Locale();
        locale.setIso3Currency("EUR");
        locale.setLanguage(language);

        HotelReservationQuery hotelReservationQuery = createHotelReservationQuery(locale);

        // Act
        HotelReservationResponse hotelReservationResponse = reservationAdapter.bookHotel(hotelReservationQuery);

        // Assert
        assertThat(hotelReservationResponse).isNotNull();
        assertThat(hotelReservationResponse.getCancellationCode()).isNotNull().isNotEmpty();
    }


    private HotelReservationQuery createHotelReservationQueryWithProfile(Locale locale) {
        HotelReservationQuery hotelReservationQuery = createHotelReservationQuery(locale);
        hotelReservationQuery.getBookingPerson().setPassword("123456");
        return hotelReservationQuery;
    }

    private HotelReservationQuery createHotelReservationQuery(Locale locale) {
        HotelReservationQuery hotelReservationQuery = new HotelReservationQuery();
        hotelReservationQuery.setLocale(locale);

        hotelReservationQuery.setHotelCode("444084");
        hotelReservationQuery.setInvBlockCode("FAB41BCCFE844050A1EBDF082FAFC49E");
        hotelReservationQuery.setRatePlanCode("45FDC0A2");
        hotelReservationQuery.setRoomTypeCode("d^ds-0");

        long today = System.currentTimeMillis();
        long tomorrow = today + Dates.ONE_DAY_IN_MILLISECONDS;

        hotelReservationQuery.setFrom(today);
        hotelReservationQuery.setTo(tomorrow);

        ArrayList<Customer> travelingPersons = new ArrayList<>();
        Customer customer = new Customer();
        customer.setFirstname("Traveller first name 1");
        customer.setLastname("Traveller last name 1");
        customer.setEmail("traveller1@hotel.de");
        travelingPersons.add(customer);

        Customer customer2 = new Customer();
        customer2.setFirstname("Traveller first name 2");
        customer2.setLastname("Traveller last name 2");
        customer2.setEmail("traveller2@hotel.de");
        travelingPersons.add(customer2);

        hotelReservationQuery.setTravelingPersons(travelingPersons);

        Customer bookingPerson = new Customer();
        bookingPerson.setFirstname("Booker first name");
        bookingPerson.setLastname("Booker last name");
        bookingPerson.setEmail("ad@hotel.de");
        bookingPerson.setPhone("+499111234567");

        CreditCard creditCard = new CreditCard();
        creditCard.setName("Credit card holder name");
        creditCard.setNumber("4222222222222");
        creditCard.setCreditCardType(CreditCardType.VISA);
        creditCard.setMonth(12);
        creditCard.setYear(2017);
        hotelReservationQuery.setCreditCard(creditCard);

        hotelReservationQuery.setBookingPerson(bookingPerson);
        return hotelReservationQuery;
    }
}
