package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.HotelProperties;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.query.PropertyDescriptionQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;

import junit.framework.TestCase;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
@SuppressWarnings({"magicnumber", "javadoctype"})
public class HdePropertyDescriptionAdapterIntegrationTest extends TestCase {

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe();
    }

    public void testThatCorrectPropertiesAreReturned() {
        // Arrange
        HdePropertyDescriptionAdapterImpl propertyDescriptionAdapter = hde.createPropertyDescriptionAdapter(RemoteAccessTargetEnvironmentType.BETA);

        PropertyDescriptionQuery query = new PropertyDescriptionQuery();
        query.setHotelId("296998");
        Language language = new Language();
        language.setIso2Language("EN");
        query.setLanguage(language);

        // Act
        Hotel hotel = propertyDescriptionAdapter.fetchHotelDescription(query);

        // Assert
        assertThat(hotel.getHotelProperties()).isNotNull();
        assertThat(hotel.getHotelProperties().getPropertyDescription()).isNotNull();
    }
}
