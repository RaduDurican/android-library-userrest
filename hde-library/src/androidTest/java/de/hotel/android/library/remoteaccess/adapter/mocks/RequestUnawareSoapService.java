package de.hotel.android.library.remoteaccess.adapter.mocks;

import de.hotel.android.library.remoteaccess.HdeJiBXMarshaller;
import de.hotel.android.library.remoteaccess.HdeJiBXMarshallerImpl;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.util.FileReader;

import junit.framework.Assert;

import org.jibx.runtime.JiBXException;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class RequestUnawareSoapService implements SoapService {
    private HdeJiBXMarshaller hdeJiBXMarshaller = new HdeJiBXMarshallerImpl();
    private InputStream response;

    public RequestUnawareSoapService(String file) {
        try {
            response = FileReader.loadXMLFile(file);
        } catch (FileNotFoundException e) {
            Assert.fail("Could not load XML response file");
        }
    }

    @Override
    public <T, S> S performRequest(T request, Class<S> type, String soapAction, boolean retryOnFailure) {
        try {
            return hdeJiBXMarshaller.unmarshalResponse(response, type);
        } catch (JiBXException e) {
            Assert.fail(e.toString());
        }

        throw new IllegalArgumentException();
    }
}
