package de.hotel.android.library.remoteaccess.adapter.mocks;

import junit.framework.Assert;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import de.hotel.android.library.remoteaccess.resultmapping.HdeJsonMapper;
import de.hotel.android.library.remoteaccess.soap.WebFormService;
import de.hotel.android.library.util.FileReader;

public class RequestUnawareWebFormService implements WebFormService {

    private final HdeJsonMapper jsonResponseMapper;
    private InputStream inputStream;

    public RequestUnawareWebFormService(String file, HdeJsonMapper responseMapper) {
        jsonResponseMapper = responseMapper;

        try {
            inputStream = FileReader.loadXMLFile(file);
        } catch (FileNotFoundException e) {
            Assert.fail("Could not load file: " + file);
        }
    }

    @Override
    public <S> S performRequest(Map<String, String> parameters, Class<S> type) {
        return jsonResponseMapper.mapFromJson(inputStream, type);
    }
}
