package de.hotel.android.library.domain.model

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable
import de.hotel.android.library.domain.model.data.Address
import de.hotel.android.library.domain.model.data.Distance
import de.hotel.android.library.domain.model.data.GeoPosition
import de.hotel.android.library.domain.model.enums.LocationType
import junit.framework.TestCase
import java.util.*

import org.assertj.core.api.Assertions.assertThat

const val HOTEL_ID: String = "1337"
const val HOTEL_NAME: String = "1337 Hotel"
const val HOTEL_PICTURE_URI: String = "http://www.test.de"
const val USER_RATING: Float = 2.6f
const val STARS_RATING: Float = -1f
const val HOTEL_CITY: String = "hotel test city"
const val HOTEL_COUNTRY_ISO_A3: String = "DEU"
const val HOTEL_STREET: String = "hotel test street"
const val HOTEL_POSTAL_CODE: String = "hotel test postal code"
const val HOTEL_LATITUDE: Double = 49.4321
const val HOTEL_LONGITUDE: Double = 11.1234

const val DISTANCE_IN_KM = 3.5f
const val LOCATION_TYPE = LocationType.CITY


class HotelParcelTest : TestCase() {

    fun testParcelShouldReturnAllValuesCorrectlyWithAllValuesSet() {
        // Arrange
        val parcel = Parcel.obtain()
        val hotel = createHotelWithAllValuesSet()

        // Act
        parcel.writeParcelable(hotel, Parcelable.PARCELABLE_WRITE_RETURN_VALUE)
        parcel.setDataPosition(0)
        val parcelHotel: Hotel = parcel.readParcelable(Hotel::class.java.classLoader)

        // Assert
        assertThat(parcelHotel.hotelId).isEqualTo(HOTEL_ID)
        assertThat(parcelHotel.name).isEqualTo(HOTEL_NAME)
        assertThat(parcelHotel.picture.toString()).isEqualTo(HOTEL_PICTURE_URI)
        assertThat(parcelHotel.userRating).isEqualTo(USER_RATING)
        assertThat(parcelHotel.starsRating).isEqualTo(STARS_RATING)
        assertThat(parcelHotel.address?.city).isEqualTo(HOTEL_CITY)
        assertThat(parcelHotel.address?.countryIsoA3).isEqualTo(HOTEL_COUNTRY_ISO_A3)
        assertThat(parcelHotel.address?.street).isEqualTo(HOTEL_STREET)
        assertThat(parcelHotel.address?.postalCode).isEqualTo(HOTEL_POSTAL_CODE)
        assertThat(parcelHotel.address?.geoPosition?.latitude).isEqualTo(HOTEL_LATITUDE)
        assertThat(parcelHotel.address?.geoPosition?.longitude).isEqualTo(HOTEL_LONGITUDE)
        assertThat(parcelHotel.distances[0].distanceInKm).isEqualTo(DISTANCE_IN_KM)
        assertThat(parcelHotel.distances[0].locationType).isEqualTo(LOCATION_TYPE)
    }

    fun testParcelShouldReturnAllValuesCorrectlyWithNullableValuesNull() {
        // Arrange
        val parcel = Parcel.obtain()
        val hotel = Hotel()

        // Act
        parcel.writeParcelable(hotel, Parcelable.PARCELABLE_WRITE_RETURN_VALUE)
        parcel.setDataPosition(0)
        val parcelHotel: Hotel = parcel.readParcelable(Hotel::class.java.classLoader)

        // Assert
        assertThat(parcelHotel.hotelId).isNull()
        assertThat(parcelHotel.name).isNull()
        assertThat(parcelHotel.picture).isNull()
        assertThat(parcelHotel.userRating).isNull()
        assertThat(parcelHotel.starsRating).isEqualTo(0.0f)
        assertThat(parcelHotel.address).isNull()
        assertThat(parcelHotel.distances).isEmpty()
    }

    private fun createHotelWithAllValuesSet(): Hotel {
        val hotel = Hotel()
        hotel.hotelId = HOTEL_ID
        hotel.name = HOTEL_NAME
        hotel.picture = Uri.parse(HOTEL_PICTURE_URI)
        hotel.starsRating = STARS_RATING
        hotel.userRating = USER_RATING


        val hotelAddress = Address()
        hotelAddress.city = HOTEL_CITY
        hotelAddress.countryIsoA3 = HOTEL_COUNTRY_ISO_A3
        hotelAddress.street = HOTEL_STREET
        hotelAddress.postalCode = HOTEL_POSTAL_CODE

        val hotelGeoPosition = GeoPosition()
        hotelGeoPosition.latitude = HOTEL_LATITUDE
        hotelGeoPosition.longitude = HOTEL_LONGITUDE
        hotelAddress.geoPosition = hotelGeoPosition
        hotel.address = hotelAddress

        val distances = ArrayList<Distance>()
        val distance = Distance()
        distance.distanceInKm = DISTANCE_IN_KM
        distance.locationType = LOCATION_TYPE
        distances.add(distance)
        hotel.distances = distances

        return hotel
    }
}
