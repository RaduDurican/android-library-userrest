package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import junit.framework.TestCase;

import java.util.List;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.Reservation;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.enums.CancelResultCode;
import de.hotel.android.library.domain.model.query.CancelReservationQuery;
import de.hotel.android.library.domain.model.query.CheckReservationStatusQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
public class HdeReservationManagementAdapterIntegrationTest extends TestCase {

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe(); // Insert a real username and a real password for debugging purposes
    }

    public void testThatReservationsAreReturnedProperly() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = hde.createReservationManagementAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CheckReservationStatusQuery checkReservationStatusQuery = new CheckReservationStatusQuery();
        checkReservationStatusQuery.setEmail("ad@hotel.de");
        checkReservationStatusQuery.setPassword("123456");
        checkReservationStatusQuery.setLocale(createLocale());

        // Act
        List<Reservation> reservations = checkReservationAdapter.getReservationList(checkReservationStatusQuery);

        // Assert
        assertThat(reservations).isNotEmpty();
    }

    public void testThatReservationsCanBeCancelled() {
        // Arrange
        HdeReservationManagementAdapterImpl checkReservationAdapter = hde.createReservationManagementAdapter(RemoteAccessTargetEnvironmentType.BETA);

        CancelReservationQuery cancelReservationQuery = new CancelReservationQuery();
        cancelReservationQuery.setEmail("ad@hotel.de");
        cancelReservationQuery.setPassword("123456");
        cancelReservationQuery.setReservationId("D8DF667B57214534B8912BC05A50CD90");
        cancelReservationQuery.setLocale(createLocale());

        // Act
        @CancelResultCode int cancelResultCode = checkReservationAdapter.cancelReservation(cancelReservationQuery);

        // Assert
        assertThat(cancelResultCode).isEqualTo(CancelResultCode.CANCEL_OK);
    }

    private Locale createLocale() {
        Locale locale = new Locale();
        Language language = new Language();
        language.setIso2Language("EN");
        locale.setLanguage(language);
        return locale;
    }
}
