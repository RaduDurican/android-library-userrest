package de.hotel.android.library.domain.service;

import android.test.InstrumentationTestCase;

import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.criterion.HotelRateCriterion;
import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.domain.service.impl.HotelFacadeImpl;
import de.hotel.android.library.remoteaccess.adapter.HdeHotelAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdePropertyDescriptionAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.HdePropertyReviewsAdapterImpl;
import de.hotel.android.library.remoteaccess.adapter.PropertyDescriptionAdapter;
import de.hotel.android.library.remoteaccess.adapter.PropertyReviewsAdapter;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.adapter.mocks.RequestUnawareSoapService;
import de.hotel.android.library.remoteaccess.resultmapping.HdeCurrencyConversionResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailErrorMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapperImpl;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyDescriptionRequestMapper;
import de.hotel.android.library.remoteaccess.v28.querymapping.HdeV28PropertyReviewsRequestMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyDescriptionResultMapper;
import de.hotel.android.library.remoteaccess.v28.resultmapping.HdeV28PropertyReviewsResultMapper;
import de.hotel.android.library.remoteaccess.v30.CreditCardTypeMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelAvailRqMapperImpl;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({"magicnumber", "javadoctype"})
public class HotelFacadeTest extends InstrumentationTestCase {

    private static final long FROM_DATE = 1337l;
    private static final long TO_DATE = 7331l;

    // This is needed because dexmaker has some problems without this
    // See http://stackoverflow.com/questions/12267572/mockito-dexmaker-on-android
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        System.setProperty(
                "dexmaker.dexcache",
                getInstrumentation().getTargetContext().getCacheDir().getPath());
    }

    public void testHotelDetailInformation() {
        // Arrange
        HotelFacade hotelFacade = createHotelFacade(
                Paths.RATE_DETAIL_RESPONSE,
                Paths.PROPERTY_DESCRIPTION_RESPONSE,
                Paths.PROPERTY_REVIEWS_RESPONSE);

        HotelAvailQuery hotelAvailQuery = createHotelAvailQuery();

        // Act
        HotelAvailResult hotelAvailResult = hotelFacade.fetchHotelDetailInformation(hotelAvailQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList().get(0).getHotelOffer().getRatePlans().get(0).getInvBlockCode()).isEqualTo("9ADF886A7C2747C88B8553551ECFB61D");
        assertThat(hotelAvailResult.getHotelList().get(0).getHotelOffer().getRatePlans().get(0).getBookingCode()).isEqualTo("16|d2a2655ba3ed7103b723e2ecec5ee83f^81.00^standard^standard^doubleroom^|d^ds-0||66259|2|1|standard|");
        assertThat(hotelAvailResult.getHotelList().get(0).getHotelProperties()).isNotNull();
    }

    public void testHotelSearchNoAvailabilityReturnsTheHotelAtLeast() {
        // Arrange
        HotelFacade hotelFacade = createHotelFacade(
                Paths.HOTEL_AVAIL_RESPONSE_NO_AVAILABILITY,
                Paths.PROPERTY_DESCRIPTION_RESPONSE,
                Paths.PROPERTY_REVIEWS_RESPONSE);

        HotelAvailQuery hotelAvailQuery = createHotelAvailQuery();

        // Act
        HotelAvailResult hotelAvailResult = hotelFacade.fetchHotelDetailInformation(hotelAvailQuery);

        // Assert
        assertThat(hotelAvailResult).isNotNull();
        assertThat(hotelAvailResult.getHotelList()).isNotEmpty();
        assertThat(hotelAvailResult.getFromDate()).isEqualTo(FROM_DATE);
        assertThat(hotelAvailResult.getToDate()).isEqualTo(TO_DATE);
    }

    private HotelAvailQuery createHotelAvailQuery() {
        HotelAvailQuery hotelAvailQuery = new HotelAvailQuery();
        HotelRateCriterion hotelRateCtriterion = new HotelRateCriterion();
        hotelRateCtriterion.setHotelId("296998");
        hotelAvailQuery.setHotelRateCriterion(hotelRateCtriterion);

        HotelAvailCriterion hotelAvailCriterion = new HotelAvailCriterion();
        RoomCriterion roomCriterion = new RoomCriterion();
        roomCriterion.setAdultCount(2);
        roomCriterion.setRoomType(RoomType.DOUBLE);
        roomCriterion.setQuantity(1);
        hotelAvailCriterion.getRoomCriterionList().add(roomCriterion);

        hotelAvailCriterion.setFrom(FROM_DATE);
        hotelAvailCriterion.setTo(TO_DATE);
        hotelAvailQuery.setHotelAvailCriterion(hotelAvailCriterion);

        Locale locale = new Locale();
        Language language = new Language();
        language.setIso2Language("EN");
        locale.setLanguage(language);
        locale.setIso3Currency("EUR");
        hotelAvailQuery.setLocale(locale);
        return hotelAvailQuery;
    }

    private HotelFacade createHotelFacade(String hotelAvailFile, String propertyDescriptionFile, String propertyReviewsFile) {
        return new HotelFacadeImpl(
                createHotelAdapter(hotelAvailFile),
                createPropertyDescriptionAdapter(propertyDescriptionFile),
                createPropertyReviewsAdapter(propertyReviewsFile));
    }

    private static HdeHotelAdapterImpl createHotelAdapter(String file) {
        return new HdeHotelAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV30OtaHotelAvailRqMapperImpl.class),
                new HdeHotelAvailResultMapperImpl(
                        new HdeHotelOffersResultMapperImpl(
                                new HdeCurrencyConversionResultMapperImpl(),
                                new HdeV30CreditCardMapperImpl(
                                        new CreditCardTypeMapper())
                        )),
                new HdeHotelAvailErrorMapperImpl());
    }

    private static PropertyDescriptionAdapter createPropertyDescriptionAdapter(String file) {
        return new HdePropertyDescriptionAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV28PropertyDescriptionRequestMapper.class),
                new HdeV28PropertyDescriptionResultMapper());
    }

    private static PropertyReviewsAdapter createPropertyReviewsAdapter(String file) {
        return new HdePropertyReviewsAdapterImpl(
                new RequestUnawareSoapService(file),
                Mockito.mock(HdeV28PropertyReviewsRequestMapper.class),
                new HdeV28PropertyReviewsResultMapper());
    }
}

