package de.hotel.android.library.remoteaccess.adapter;

import android.test.suitebuilder.annotation.Suppress;

import de.hotel.android.library.HotelDe;
import de.hotel.android.library.domain.model.HotelPropertyReviews;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.query.PropertyReviewsQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.util.Constants;

import junit.framework.TestCase;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Runs against the real HSBW. Use for developing/debugging purposes.
 */
@Suppress
public class HdePropertyReviewsAdapterIntegrationTest extends TestCase {

    private HotelDe hde;

    public void setUp() {
        hde = new HotelDe();
    }

    /**
     * Test if the request is done properly and the response is not null
     */
    public void testIfResponseIsOk() {
        //Arrange
        HdePropertyReviewsAdapterImpl reviewsAdapter = hde.createPropertyReviewsAdapter(RemoteAccessTargetEnvironmentType.BETA);

        //create a query
        PropertyReviewsQuery query = new PropertyReviewsQuery();
        query.setHotelId("127495");
        Language language = new Language();
        language.setIso2Language("EN");
        query.setLanguage(language);

        //act
        HotelPropertyReviews hotelPropertyReviews = reviewsAdapter.fetchPropertyReviews(query);

        //assert
        assertThat(hotelPropertyReviews).isNotNull();
        assertThat(hotelPropertyReviews.getCustomerReviews()).isNotNull();
        assertThat(hotelPropertyReviews.getCustomerReviews()).isNotEmpty();
        assertThat(hotelPropertyReviews.getCustomerReviews().get(0).getCommentNegative()).isNotEmpty();
    }
}
