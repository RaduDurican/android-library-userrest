package de.hotel.android.library.remoteaccess.adapter;

import de.hotel.android.library.domain.model.HdeAdditionalDetails;
import de.hotel.android.library.domain.model.HotelAvailResult;
import de.hotel.android.library.domain.model.Hotel;
import de.hotel.android.library.domain.model.criterion.HotelAvailCriterion;
import de.hotel.android.library.domain.model.criterion.HotelSearchCriterion;
import de.hotel.android.library.domain.model.criterion.LocationCriterion;
import de.hotel.android.library.domain.model.criterion.RoomCriterion;
import de.hotel.android.library.domain.model.data.Language;
import de.hotel.android.library.domain.model.data.Locale;
import de.hotel.android.library.domain.model.enums.AdditionalDetailsType;
import de.hotel.android.library.domain.model.enums.RoomType;
import de.hotel.android.library.domain.model.query.HotelAvailQuery;
import de.hotel.android.library.remoteaccess.RemoteAccessTargetEnvironmentType;
import de.hotel.android.library.remoteaccess.adapter.mocks.MockedPagingSoapService;
import de.hotel.android.library.remoteaccess.adapter.mocks.MockedSoapService;
import de.hotel.android.library.remoteaccess.adapter.mocks.Paths;
import de.hotel.android.library.remoteaccess.resultmapping.HdeCurrencyConversionResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailErrorMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelAvailResultMapperImpl;
import de.hotel.android.library.remoteaccess.resultmapping.HdeHotelOffersResultMapperImpl;
import de.hotel.android.library.remoteaccess.soap.SoapService;
import de.hotel.android.library.remoteaccess.v30.CreditCardTypeMapper;
import de.hotel.android.library.remoteaccess.v30.HdeV30CreditCardMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaHotelAvailRqMapperImpl;
import de.hotel.android.library.remoteaccess.v30.querymapping.HdeV30OtaMapperImpl;

import junit.framework.TestCase;

import org.jibx.runtime.JiBXException;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings("magicnumber")
public class HdeHotelAdapterTest extends TestCase {
    public static final int LOCATION_ID_FUERTH = 33743;
    public static final int PAGE_SIZE = 7;
    public static final int TOTAL_AVAILABLE_HOTELS_MULTI_AVAIL = 15;
    public static final int TOTAL_AVAILABLE_HOTELS_SINGLE_AVAIL = 1;

    public void testHotelSearchWithPagingShouldReturnCorrectPageSize() throws IOException, JiBXException, ParseException {
        // Arrange
        MockedPagingSoapService mockedPagingSoapService = new MockedPagingSoapService();
        mockedPagingSoapService.setPageSize(PAGE_SIZE);
        HdeHotelAdapterImpl hotelAdapter = createHotelAdapter(mockedPagingSoapService);

        HotelAvailQuery availQuery = createAvailQuery();
        availQuery.setPageSize(PAGE_SIZE);
        availQuery.setPageNumber(0);

        // Act
        HotelAvailResult hotelAvailResult = hotelAdapter.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).hasSize(PAGE_SIZE);
        assertThat(hotelAvailResult.getTotalAvailableHotels()).isEqualTo(TOTAL_AVAILABLE_HOTELS_MULTI_AVAIL);
    }

    public void testHotelSearchWithPagingShouldReturnCorrectPageSizeForLastCall() throws IOException, JiBXException, ParseException {
        // Arrang
        MockedPagingSoapService mockedPagingSoapService = new MockedPagingSoapService();
        mockedPagingSoapService.setPageSize(PAGE_SIZE);
        HdeHotelAdapterImpl hotelAdapter = createHotelAdapter(mockedPagingSoapService);

        HotelAvailQuery availQuery = createAvailQuery();
        availQuery.setPageSize(PAGE_SIZE);
        availQuery.setPageNumber(2);

        // Act
        HotelAvailResult hotelAvailResult = hotelAdapter.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).hasSize(1);
        assertThat(hotelAvailResult.getTotalAvailableHotels()).isEqualTo(TOTAL_AVAILABLE_HOTELS_MULTI_AVAIL);
    }

    public void testHotelSearchWithoutPagingShouldReturnCorrectValues() throws IOException, JiBXException, ParseException {
        // Arrange
        HotelAvailQuery availQuery = createAvailQuery();
        MockedSoapService mockedSoapService = new MockedSoapService(Paths.HOTEL_AVAIL_RESPONSE_WITHOUT_PAGING);
        HdeHotelAdapterImpl hotelAdapter = createHotelAdapter(mockedSoapService);

        // Act
        HotelAvailResult hotelAvailResult = hotelAdapter.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).hasSize(15);
        assertThat(hotelAvailResult.getTotalAvailableHotels()).isEqualTo(TOTAL_AVAILABLE_HOTELS_MULTI_AVAIL);
        assertThat(hotelAvailResult.getHotelList().get(2).getReceptionTimes()).isNull();

        List<HdeAdditionalDetails> additionalDetailList = hotelAvailResult.getHotelList().get(2).getHotelOffer().getRatePlans().get(0).getPaymentInfo().getAdditionalDetailList();
        assertThat(additionalDetailList.size()).isEqualTo(2);

        HdeAdditionalDetails breakfastDetail = additionalDetailList.get(0);
        assertThat(breakfastDetail.getDetailsType()).isEqualTo(AdditionalDetailsType.BREAKFAST);
        assertThat(breakfastDetail.getCurrencyCode()).isEqualTo("EUR");
        assertThat(breakfastDetail.getAmount()).isEqualByComparingTo(new BigDecimal(9.00));

        HdeAdditionalDetails amenitiesDetail = additionalDetailList.get(1);
        assertThat(amenitiesDetail.getDetailsType()).isEqualTo(AdditionalDetailsType.AMENITIES);

        List<String> descriptions = amenitiesDetail.getDescriptions();
        assertThat(descriptions.get(0)).isEqualTo("Free internet terminal");
        assertThat(descriptions.get(1)).isEqualTo("Complimentary daily newspaper");
        assertThat(descriptions.get(2)).isEqualTo("Parking spaces at the hotel");
        assertThat(descriptions.get(3)).isEqualTo("Garage");
        assertThat(descriptions.get(4)).isEqualTo("Access to sauna");
        assertThat(descriptions.get(5)).isEqualTo("Free WI-FI");
    }

    public void testHotelSearchSingleAvailShouldReturnCorrectValues() throws IOException, JiBXException, ParseException {
        // Arrange
        HotelAvailQuery availQuery = createAvailQuery();
        MockedSoapService mockedSoapService = new MockedSoapService(Paths.RATE_DETAIL_RESPONSE);
        HdeHotelAdapterImpl hotelAdapter = createHotelAdapter(mockedSoapService);

        // Act
        HotelAvailResult hotelAvailResult = hotelAdapter.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).hasSize(1);
        assertThat(hotelAvailResult.getTotalAvailableHotels()).isEqualTo(TOTAL_AVAILABLE_HOTELS_SINGLE_AVAIL);

        Hotel hotel = hotelAvailResult.getHotelList().get(0);
        assertThat(hotel.getReceptionTimes()).isEqualTo("von 00:00 bis 00:00 Uhr");

        List<HdeAdditionalDetails> additionalDetailList = hotel.getHotelOffer().getRatePlans().get(0).getPaymentInfo().getAdditionalDetailList();
        assertThat(additionalDetailList.size()).isEqualTo(2);

        HdeAdditionalDetails breakfastDetail = additionalDetailList.get(0);
        assertThat(breakfastDetail.getDetailsType()).isEqualTo(AdditionalDetailsType.BREAKFAST);
        assertThat(breakfastDetail.getCurrencyCode()).isEqualTo("EUR");
        assertThat(breakfastDetail.getAmount()).isEqualByComparingTo(new BigDecimal(4.50));

        HdeAdditionalDetails amenitiesDetail = additionalDetailList.get(1);
        assertThat(amenitiesDetail.getDetailsType()).isEqualTo(AdditionalDetailsType.AMENITIES);

        List<String> descriptions = amenitiesDetail.getDescriptions();
        assertThat(descriptions.get(0)).isEqualTo("Kostenlose Tageszeitung");
        assertThat(descriptions.get(1)).isEqualTo("Parkplätze direkt am Hotel");
    }

    public void testThatNotAvailableCorporateRatesAreFiltered() throws ParseException {
        // Arrange
        HotelAvailQuery availQuery = createAvailQuery();
        MockedSoapService mockedSoapService = new MockedSoapService(Paths.HOTEL_AVAIL_RESPONSE_CORPORATE);
        HdeHotelAdapterImpl hotelAdapter = createHotelAdapter(mockedSoapService);

        // Act
        HotelAvailResult hotelAvailResult = hotelAdapter.searchAvailableHotels(availQuery);

        // Assert
        assertThat(hotelAvailResult.getHotelList()).hasSize(18); // 2 of 20 RoomStays are not available
    }

    public static HotelAvailQuery createAvailQuery() throws ParseException {
        HotelAvailQuery hotelAvailQuery = new HotelAvailQuery();
        HotelAvailCriterion hotelAvailCriterion = new HotelAvailCriterion();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.US);
        Date dateFrom = simpleDateFormat.parse("2016-12-13");
        long from = dateFrom.getTime();

        Date dateTo = simpleDateFormat.parse("2016-12-14");
        long to = dateTo.getTime();

        hotelAvailCriterion.setFrom(from);
        hotelAvailCriterion.setTo(to);
        ArrayList<RoomCriterion> roomCriterions = new ArrayList<>();
        RoomCriterion roomCriterion = new RoomCriterion();
        roomCriterion.setAdultCount(1);
        roomCriterion.setRoomType(RoomType.DOUBLE);
        roomCriterion.setQuantity(1);
        roomCriterions.add(roomCriterion);
        hotelAvailCriterion.setRoomCriterionList(roomCriterions);
        hotelAvailQuery.setHotelAvailCriterion(hotelAvailCriterion);

        HotelSearchCriterion hotelSearchCriterion = new HotelSearchCriterion();
        LocationCriterion locationCriterion = new LocationCriterion();
        locationCriterion.setLocationId(LOCATION_ID_FUERTH);
        hotelSearchCriterion.setLocationCriterion(locationCriterion);
        hotelAvailQuery.setHotelSearchCriterion(hotelSearchCriterion);

        Locale locale = new Locale();
        locale.setIso3Currency("EUR");
        Language language = new Language();
        language.setIso2Language("EN");
        locale.setLanguage(language);
        hotelAvailQuery.setLocale(locale);

        return hotelAvailQuery;
    }

    public static HdeHotelAdapterImpl createHotelAdapter(SoapService soapService) {
        return new HdeHotelAdapterImpl(soapService,
                // TODO: Use mock once it becomes available...
                new HdeV30OtaHotelAvailRqMapperImpl(
                        new HdeV30OtaMapperImpl("dummy", RemoteAccessTargetEnvironmentType.BETA)
                ),
                new HdeHotelAvailResultMapperImpl(
                        new HdeHotelOffersResultMapperImpl(
                                new HdeCurrencyConversionResultMapperImpl(),
                                new HdeV30CreditCardMapperImpl(
                                        new CreditCardTypeMapper())
                        )),
                new HdeHotelAvailErrorMapperImpl());
    }
}
